
  <html>
  <head>

<?php
header('Content-type: text/html;charset=utf-8');
  require  "menu.php";
  if (isset($Titolo)) {
     print("<title>".$Titolo."</title>");
  }
?>


  <link rel="stylesheet" type="text/css" href="../../Html/am_style.css" >
  <link rel="stylesheet" href="../../Html/jquery-ui-1.8.16.custom.css" type="text/css" >
  <link rel="stylesheet" href="../../Html/demo_page.css" type="text/css" >
  <link rel="stylesheet" href="../../Html/demo_table_jui.css" type="text/css" >
  <link rel="stylesheet" href="../../Html/jquery.ui.timepicker.css" type="text/css" >

  <script type="text/javascript" src="../../Html/jquery-1.7.2.min.js" /></script>

  <script type="text/javascript" src="../../Html/jquery-ui-1.8.16.custom.min.js" ></script>
  <script type="text/javascript" src="../../Html/jquery.droppy.js"></script>
  <script type="text/javascript" src="../../Html/tooltip.js"></script>
  <script type="text/javascript" src="../../Html/jqeury-context-menu.js"></script>
  <script type="text/javascript" src="../../Html/utility.js"></script>
  <script type="text/javascript" src="../../Html/ajax.js"></script>
  <script type="text/javascript" src="../../Html/jquery.dataTables.js"></script>
  <script type="text/javascript" src="../../Html/dataTables.personal.js"></script>
  <script type="text/javascript" src="../../Html/jquery.ui.timepicker.js"></script>


  <script type="text/javascript" src="../../Html/jquery.validate.min.js" /></script>

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >

  </head>
  <body>
  <script type="text/javascript">
      jQuery.extend(jQuery.validator.messages, {
        required: "Campo Obbligatorio",
        email: "Inserire una mail valida",
        digits: "Inserire solo numeri",
        importoeuro:"Inserire un importo corretto",
        percentuale:"Inserire una percentuale tra 1 e 99" 
    });
  $.validator.methods.importoeuro = function(value, element, param) {
       if (value == null || value == "" ) return true;
       value =  value.replace(".","");
       var myre= /^\d+([\,]{1}\d{2})?$/;
      	if (!(myre.test(value))) {
               return false;
        }
        var imp = value.replace(".","");
        imp = imp.replace(",",".");
    		var impN = parseFloat(imp);

        if (impN < 0) return false;


        return true;

  };

  $.validator.methods.percentuale = function(value, element, param) {
       if (value == null || value == "" ) return true;
       value =  value.replace(".","");
       var myre= /^\d+([\,]{1}\d{2})?$/;
      	if (!(myre.test(value))) {
               return false;
        }
        var imp = value.replace(".","");
        imp = imp.replace(",",".");
    		var impN = parseFloat(imp);

        if (impN < 0) return false;

        if (impN > 100) return false;


        return true;

  };

  Number.prototype.formatMoney = function(c, d, t){
  var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
     return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
   };

$(document).keydown(function(e) {


       var nodeName = e.target.nodeName.toLowerCase();
     if( e.which === 13)
         if ( nodeName === 'textarea' ||
              nodeName === 'button') {
            return true;
          }
         else {
          return false;
          }
     else
          return true;


});


  </script>
  <table width="820px" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
  <td align="center">
  <img src="../../Icons/yperesia.jpg" width="410px" />
  </td></tr>
  </table>

  <table width="900px" border="0" align="center" cellpadding="0" cellspacing="0">

  <tr  class="grigio"> <td class="px" height="10"></td></tr>
  <tr  class="grigio"> <td>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" >
  <tr>
  <td width="2%"></td>
  <td width="68%px">
  <?php
     crea_menu();
  ?>
  </td>
  <td width="30%" class="label" align="right">
  <?php
  if (is_logged() && !isset($_GET['rub']) )  {
        echo "<a href=\"../Base/login.php?logout=1\">Disconnetti ".$_SESSION['user']."</a>";
    }
   ?>
  </td></tr>
  </table>
  </td></tr>
  <tr  class="grigio"> <td class="px" height="10"></td></tr>
  <tr  class="grigioc"> <td class="px" height="15"></td></tr>


  <tr><td>
  <table width="95%" border="0" cellspacing="0" cellpadding="0">
  <tr><td width="10%" valign="top">
     <!--?php require '../../Librerie/ges_html_left.php'; ?-->
  </td>
  <td width="80%" valign="top">

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td class="TitoloPRG" align="center">
    <?php if (isset($Titolo)) echo $Titolo;
           /*
          $pos = strrpos($_SERVER['PHP_SELF'],'/');
          $programma = substr($_SERVER['PHP_SELF'],$pos +1);
          if (db_StartsWith($programma,'vis_') && isset($Tavola) ) {
             $ritorno = substr(strstr($_SERVER['PHP_SELF'],"Programmi"),9);
             echo "&nbsp;&nbsp;&nbsp;";
             echo "<a href=\"../Admin/report.php?p_tavola=".$Tavola."&p_ritorno=".$ritorno."\" ><img class=\"link\" src=\"../../Icons/stampante.gif\"/></a>";
          }  */


    ?> </td></tr>
    </table>
