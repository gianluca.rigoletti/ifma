<?php

class ges_errori
{

     var $Errori= array();
     var $Errore_campo = array();
     var $Testo_errore_campo = array();
     var $pop_errori = true;
     var  $altra_funzione = "";

     function add($testo,$campo = null) {
         $this->Errori[] = $testo;
         if ($campo != null && $campo != " " && $campo != "") {
             if (is_array($campo)) { 
               for ($i=0;$i<count($campo);$i++) {
                  $this->Errore_campo[] = $campo[$i];
                  $this->Testo_errore_campo[] = $testo;
               }
             } else { 
               $this->Errore_campo[] = $campo;
               $this->Testo_errore_campo[] = $testo;
             }
         }
     }
     
     function set_pop_errori ($var) {
        if (!$var) $this->pop_errori == false;
     }

     function set_altra_funzione ($var) {
        $this->altra_funzione == $var;
     }     
     
     function is_errore() {
         return count($this->Errori) >0;
     }
     
     function mostra() {
       if (isset($this->Errori) && count($this->Errori) > 0) {       
          if (!isset($this->pop_errori) || $this->pop_errori ) {

            echo "
             <div aria-hidden=\"true\" role=\"dialog\" tabindex=\"-1\" class=\"modal fade bs-example-modal-sm \" >
                    <div class=\"modal-dialog modal-sm\">
                      <div class=\"modal-content\">

                        <div class=\"modal-header\">
                          <button aria-label=\"Close\" data-dismiss=\"modal\" class=\"close\" type=\"button\"><span aria-hidden=\"true\">X</span>
                          </button>
                          <h4 id=\"myModalLabel2\" class=\"modal-title\">Attenzione: Rilevate le seguenti segnalazioni</h4>
                        </div>
                        <div class=\"modal-body\">";

                        for ($a = 0; $a< count($this->Errori); $a++) { 
                             echo "<span class=\"errore\" >".$this->Errori[$a]."</span><br>";
                        }                        
                        
                        echo "</div>
                        <div class=\"modal-footer\">
                          <button data-dismiss=\"modal\" class=\"btn btn-default\" type=\"button\">Chiudi</button>
                        </div>

                      </div>
                    </div>
                  </div>
                  
                <script>
                  $('.bs-example-modal-sm ').modal('show');
                </script>
                ";
                            
          
          } else {
              if (isset($this->Errori) && count($this->Errori) > 0) {                
                  $numero_Errori = count($this->Errori); 
                  echo "<table> <tr><td class=\"px\" height=\"30\"></td></tr><tr>";
                    for ($a = 0; $a< $numero_Errori; $a++) { 
                         echo "<td class=\"errore\">".$this->Errori[$a]."</td></tr>";
                    }
                  echo "<tr><td class=\"px\" height=\"10\"></td></tr> </table>";
              
              } 
          }
        }
      
      }
      
      
      function tooltip($campo,$echo = true) {
      
          
          $testo = "<ul class=err >";
          $trovato = false;
          for ($i=0;$i<count($this->Errore_campo);$i++) {
              if ($this->Errore_campo[$i] == $campo) {
                 $trovato = true;
                 $testo .= "<li>".$this->Testo_errore_campo[$i]."</li>";
              }
          }
          
          $testo .= "</ul>";
          
          if ($trovato) {
             $stringa =  " onMouseOver=\"toolTip('".$testo."', 300)\" onMouseOut=\"toolTip()\" data-error=\"true\" ";
             if ($echo) echo $stringa;
             else  return $stringa;
          }
      }
}
?>   