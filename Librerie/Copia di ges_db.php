<?php

function db_is_null ($valore) {
   return  ($valore == "" || $valore == " " || $valore == null );
}

// visualizzazione

function db_query_vis ($p_table_name, $p_order_by)
{

    $p_where = " 1 = 1 ";
    return db_query_generale($p_table_name,$p_where,$p_order_by);
}

// modifica
function db_query_mod($p_table_name, $p_id)
{
   $p_where = "id =".$p_id;
   return db_query_generale($p_table_name,$p_where,null);
}

// modifica
function db_query_fk($p_table_name, $p_id)
{
   return db_query_fk_cod($p_table_name, $p_id,"ID");
}

function db_query_fk_cod($p_table_name, $p_id, $p_campo_key)
{
   $p_where = " ".$p_campo_key." ='".$p_id."'";
   return db_query_generale($p_table_name,$p_where,null);
}

//generale
function db_query_generale ($p_table_name,$p_where, $p_order_by,$p_limit_ini = -1,$p_limit_quanti =-1) 
{

    $sql = "select * from ".$p_table_name." where ".$p_where;
    if ($p_order_by != null) {
        $sql = $sql." order by ".$p_order_by;
    }
    
    if ($p_limit_ini >=0 && $p_limit_quanti >=0) {
        $sql .= " limit ".$p_limit_ini.",".$p_limit_quanti;
    }
    $result = mysql_query($sql);

    if (! $result ) die ("Error in db_query_generale:<b> $sql </b> ".mysql_error());
    return $result;

}

function db_query_count ($p_table_name,$p_where, $p_order_by = null,$p_limit_ini = -1,$p_limit_quanti =-1) 
{

    $sql = "select count(*) nrec from ".$p_table_name." where ".$p_where;
    if ($p_order_by != null) {
        $sql = $sql." order by ".$p_order_by;
    }
    
    if ($p_limit_ini >=0 && $p_limit_quanti >=0) {
        $sql .= " limit ".$p_limit_ini.",".$p_limit_quanti;
    }
    
    $result = mysql_query($sql);
    if (! $result ) die ("Error in db_query_count:<b> $sql </b> ".mysql_error());
    $cur_rec = mysql_fetch_assoc($result);
    return $cur_rec['nrec'];

}

function db_query_personalizzata ($p_table_name,$p_where, $p_order_by,$altro) 
{

    $sql = "select *,".$altro." from ".$p_table_name." where ".$p_where;
    if ($p_order_by != null) {
        $sql = $sql." order by ".$p_order_by;
    }
    
    $result = mysql_query($sql);
    if (! $result ) die ("Error in db_query_generale:<b> $sql </b> ".mysql_error());
    return $result;

}


function db_numero ($p_codice) 
{

    $sql = "select numero from progressivi  where codice =".$p_codice;
    
    
    $result = mysql_query($sql);
    if (! $result ) die ("Error in db_numero:<b> $sql </b> ".mysql_error());
    return $result;

}

function db_get_id () 
{

    $sql = "SELECT LAST_INSERT_ID() ID";
    
    $result = mysql_query($sql);
    if (! $result ) die ("Error in recupero id:<b> $sql </b> ".mysql_error());
    $cur_rec = mysql_fetch_assoc($result);
    return $cur_rec['ID'];    

}

//delete
function db_delete ($p_table_name, $p_id)
{
      db_delete_cod($p_table_name, $p_id,'ID');
}

function db_delete_cod ($p_table_name, $p_val, $p_campo)
{
        $delete = "delete from ".$p_table_name." where ".$p_campo."='".$p_val."'";
        $result = mysql_query($delete);
        if (! $result ) die ("Error in db_query_generale:<b> $result </b> ".mysql_error());
}


//update
function db_update ($p_table_name,$p_id,$p_post) {
	$update = "update  ".$p_table_name;
	
	$colonne = db_get_struttura($p_table_name);
	$i = 0;
	while ($cur_rec = mysql_fetch_assoc($colonne)) {
	
	   if (isset($p_post[$cur_rec['Field']])) {
	       if ($i== 0) {
	          $update = $update." set ";
	          $i=1;
	       } else {
	          $update = $update.", ";
	       }
	       
	       if ($cur_rec['Field'] == 'DATA_UPD') {
	          $update=$update.$cur_rec['Field']." = now('')";
	       } else {
	         if ( $p_post[$cur_rec['Field']] != "") {
              $elem = addslashes($p_post[$cur_rec['Field']]); // risolve il classico problema dei caratteri speciali per inserimenti in SQL
              //$elem = ($p_post[$cur_rec['Field']]); // altervista fa gi� la addslashes
                $elem = preg_replace("/\\\'/","''",$elem); //cambia lo backslash+singlequote con 2 singlequote come fa phpmyadmin.
                if (colonna_numerica($cur_rec['Type'])) $elem = db_convnum($elem);
  	            $update=$update.$cur_rec['Field']." = '".$elem."'";
	         } else {
               $update=$update.$cur_rec['Field']." = null";
           }
	       }
	   }
	}
	
	
	if ($i== 0) die ("Error in aggiornamento dati, nessuna colonna in update");
	
	$update=$update." where id =".$p_id;

	$result = mysql_query($update);
        if (! $result ) die ("Error in aggiornamento:<b> $update </b> ".mysql_error());
}


//update
function db_insert ($p_table_name,$p_post) {

	$insert = "insert into  ".$p_table_name;
	
	$colonne = db_get_struttura($p_table_name);
	$i = 0;
	$elenco_colonne = "(id, ";
	$elenco_valori  = "values (null, ";
	while ($cur_rec = mysql_fetch_assoc($colonne)) {
	
	   if (isset($p_post[$cur_rec['Field']]) && $cur_rec['Field'] != "ID") {
	       if ($i== 0) {
	          $i=1;
	       } else {
	          $elenco_colonne = $elenco_colonne.", ";
	          $elenco_valori  = $elenco_valori.", ";
	       }
         
	       
         $elenco_colonne=$elenco_colonne.$cur_rec['Field'];
	       
         // non entrava con un campo = 0....so strage!
	       if ( $p_post[$cur_rec['Field']] != "" || $p_post[$cur_rec['Field']] == "0") {
              
              $elem = addslashes($p_post[$cur_rec['Field']]); // risolve il classico problema dei caratteri speciali per inserimenti in SQL
              //$elem = ($p_post[$cur_rec['Field']]); // altervista fa gi� la addslashes
              $elem = preg_replace("/\\\'/","''",$elem); //cambia lo backslash+singlequote con 2 singlequote come fa phpmyadmin.
              if (colonna_numerica($cur_rec['Type'])) $elem = db_convnum($elem);
  	          $elenco_valori=$elenco_valori."'".$elem."'";
              
	       } else  {
	          $elenco_valori=$elenco_valori."null";
	       }
	   }
	}
	
	if ($i== 0) die ("Error in inserimento dati, nessuna colonna in insert");
	
        $insert = $insert." ".$elenco_colonne.") ".$elenco_valori.")";
        //echo $insert;
	$result = mysql_query($insert);
        if (! $result ) die ("Error in aggiornamento:<b> $insert </b> ".mysql_error());
        
}

//controllo duplicati.
function db_dup_key ($p_table_name,$p_post) {

        $colonne = db_get_struttura_uk($p_table_name);
        
        $sql = "SELECT ID FROM ".$p_table_name." where ";
        
        // se id valorizzato controllo che il record non sia lo stesso che sto modificando
        if (isset($p_post['ID']) && $p_post['ID'] != 0 ) {
           $sql .= "id != " .$p_post['ID']." and ";
        }
        
        $i = 0;
	while ($cur_rec = mysql_fetch_assoc($colonne)) {
    	  if ($i== 0) {
    	      $i=1;
    	      } else {
    	      $sql = $sql." and ";
    	     }
    
    	     // una colonna di UK non � gestita dal form in insert!
    	     if (!isset($p_post[$cur_rec['Column_name']])) {
    	        die ("Error in controllo dati, colonna ".$cur_rec['Column_name']." non gestita in insert");
    	     }
    	     
    	     if ($p_post[$cur_rec['Column_name']] != " " && $p_post[$cur_rec['Column_name']] != null && 
               $p_post[$cur_rec['Column_name']] != "" 
              )
                 $sql = $sql.$cur_rec['Column_name']." = '".$p_post[$cur_rec['Column_name']]."'";
           else
                 $sql = $sql.$cur_rec['Column_name']." is null";
	}
   // se i = 0 allora la tabella non ha UK!
	if ($i== 0) return 0; 
        $result = mysql_query($sql);
        if (! $result ) die ("Error in controllo duplicati:<b> $sql </b> ".mysql_error());           
     
        return mysql_num_rows($result);
}

// controllo update
function db_ctr_update ($p_table_name,$p_post) {
     
     if ($p_post['ID'] == "") return null;
      
     $ris = db_query_mod($p_table_name,$p_post['ID']);
     if (! $ris ) return null;
     $cur_rec = mysql_fetch_assoc($ris);
    
     if ($cur_rec['DATA_UPD'] != $p_post['DATA_UPD']) {
        return 'record modificato da altro utente';
     } 
     return null;
}

//generale
function db_get_struttura($p_table_name) {

   $where = "and 1 = 1";
   return db_get_struttura_completa($p_table_name,$where);
}

function db_get_struttura_uk($p_table_name) {

   //global $database_schema, $database;
   
   //mysql_select_db($database_schema);
   $sql = "SHOW INDEX FROM ".$p_table_name." WHERE non_unique =0 AND key_name != 'PRIMARY'";
   
   /*
   $sql = "SELECT COLUMN_NAME FROM TABLE_CONSTRAINTS a,KEY_COLUMN_USAGE b
            WHERE a.TABLE_SCHEMA = '".$database."' 
              and a.table_name = '".$p_table_name."' 
              and a.CONSTRAINT_TYPE = 'UNIQUE'
              and b.table_schema = a.table_schema
              and b.table_name = a.table_name
              and b.constraint_name = a.constraint_name";
   */
   $result = mysql_query($sql);
   if (!$result ) die ("Error in db_get_struttura:<b> $sql </b> ".mysql_error());

   //mysql_select_db($database);
   return $result;
}

function db_get_struttura_completa ($p_table_name,$p_where) {

   //global $database_schema, $database;
   
   //mysql_select_db($database_schema);
   $sql = "SHOW COLUMNS FROM ".$p_table_name."";     
   //$sql = "SELECT COLUMN_NAME FROM COLUMNS WHERE TABLE_SCHEMA = '".$database."' and table_name = '".$p_table_name."'"." ".$p_where;
   $result = mysql_query($sql);
   if (!$result ) die ("Error in db_get_struttura:<b> $sql </b> ".mysql_error());

   //mysql_select_db($database);
   return $result;
        
}

function db_get_fk ($p_table_name) {
 $sql = "SHOW CREATE TABLE ".$p_table_name."";
 $result = mysql_query($sql);
 $row = mysql_fetch_assoc($result);
 $current_tab = str_replace("`", "", stripslashes($row['Create Table']));
 $lines = explode("\n", $current_tab);
 
 $costranint = array();
 for ($i=0;$i<count($lines);$i++) {
    //echo "***".substr($lines[$i],2,10)."***<br>";
    if ( substr($lines[$i],2,10) == "CONSTRAINT" ) {
       //echo $lines[$i]."<br>";
       $colonna = strstr($lines[$i],'(');
       
       $colonna = substr(strstr($colonna,')',true),1);
       
       $tavola = strstr($lines[$i],"REFERENCES ");
       $tavola = substr(strstr($tavola," "),1);
       
       $chiave = strstr($tavola,'(');
       $chiave = substr(strstr($chiave,')',true),1);
       
       $tavola = strstr($tavola,' ',true);
       
       //echo $colonna." ".$tavola." ".$chiave;
       
       $costranint[] = array($colonna,$tavola,$chiave);  
    }
 
 }
 
 return $costranint;
}

//html
function db_html_select($p_table_name, $p_sel, $non_nullo = true,$where =null)
{
    db_html_select_cod($p_table_name, $p_sel,'ID','DESCRIZIONE',$non_nullo,$where);
}

function db_html_select_cod($p_table_name, $p_sel,$p_campo_key,$p_campo_desc, $non_nullo,$where=null)
{
    $esiste = false;
    $selected = '';
    if (!isset($non_nullo) || $non_nullo) {
    
        if ($p_sel == null || $p_sel == "") {
          $selected = 'selected';
          $esiste = true;  
        }    
        $stringa = '<option value = "" '.$selected.'>';
        echo $stringa;
    }
    
    if ($where == null ) $risultato = db_query_vis($p_table_name,$p_campo_desc);
    else $risultato = db_query_generale($p_table_name,$where,$p_campo_desc);
    while ($cur_rec = mysql_fetch_assoc($risultato))       
    {
    
        $selected = '';
        if (isset($p_sel) && ($p_sel==$cur_rec[$p_campo_key]) ) {
          $selected = 'selected';
          $esiste = true;
        } 
    
        $stringa = '<option value = "'.$cur_rec[$p_campo_key].'" '.$selected.' >'.$cur_rec[$p_campo_desc];
        echo $stringa;
    } 
    
    if (!$esiste) {
       $stringa = '<option value = "?" selected > Non Codificato';
       echo $stringa;
    }
}

function db_elenco_progressivo($p_sel)
{
    $esiste = false;
    $selected = '';
    
    if ($p_sel == null || $p_sel == "") {
        $selected = 'selected';
        $esiste = true;  
    }    
    $stringa = '<option value = "" '.$selected.'>';
    echo $stringa;
 
    $sql = "select CODICE,max(descrizione) DES from progressivi group by codice order by codice";
    
    $result = mysql_query($sql);
    if (! $result ) die ("Errore in elenco_progressivo:<b> $sql </b> ".mysql_error());
    
    while ($cur_rec = mysql_fetch_assoc($result))       
    {
    
        $selected = '';
        if (isset($p_sel) && ($p_sel==$cur_rec['CODICE']) ) {
          $selected = 'selected';
          $esiste = true;
        } 
    
       $stringa = '<option value = "'.$cur_rec['CODICE'].'" '.$selected.' >'.$cur_rec['DES'];
         
        echo $stringa;
    } 
    
    if (!$esiste) {
       $stringa = '<option value = " " selected > Non Codificato';
       echo $stringa;
    }
}

// controlli uguale, contiene, inizia con
function db_controlla_valori($cntr,$valore,$test) {
    if ($test == null || $test == "") return true;

    if ($valore == null || $valore == "") return false;

    $valore = strtoupper($valore);
    $test = strtoupper($test);
    
    if ( $cntr == "U" && $valore != $test ) return  false;
    if ( $cntr == "C" && strpos($valore, $test) === false ) return false;
    if ( $cntr == "I" && (strpos($valore, $test) != 0 || strpos($valore, $test) === false) ) return false;    
    
    return true;
}


// utilit� csv
function make_csv_line($values) {

    // If a value contains a comma, a quote, a space, a 

    // tab (\t), a newline (\n), or a linefeed (\r),

    // then surround it with quotes and replace any quotes inside

    // it with two quotes

    foreach($values as $i => $value) {

        if ((strpos($value, ',')  !== false) ||

            (strpos($value, '"')  !== false) ||

            (strpos($value, ' ')  !== false) ||

            (strpos($value, "\t") !== false) ||

            (strpos($value, "\n") !== false) ||

            (strpos($value, "\r") !== false)) {

            $values[$i] = '"' . str_replace('"', '""', $value) . '"';

        }

    }

    // Join together each value with a comma and tack on a newline

    return implode(';', $values) . "\n";

}

//login.
function db_log_ope ($p_post) {

        $sql = "SELECT ID,DESCRIZIONE FROM operatori where CODICE = '".$p_post['CODICE']."' AND PASSWORD ='".$p_post['PASSWORD']."'";

        $result = mysql_query($sql);
        if (!$result ) die ("Error in controllo OPERATORE:<b> $sql </b> ".mysql_error());           
     
        return $result;
}

function db_log_utenti ($p_post) {

        $sql = "SELECT * FROM amministratori where user = '".$p_post['CODICE']."' AND pword ='".$p_post['PASSWORD']."'";

        $result = mysql_query($sql);
        if (!$result ) die ("Error in controllo UTENTE:<b> $sql </b> ".mysql_error());           
     
        return $result;
}

function db_log_utenti_pubblici ($p_post) {

        $sql = "SELECT * FROM utenti where upper(email) = upper('".$p_post['CODICE']."') AND password ='".$p_post['PASSWORD']."' and idsito = ".$p_post['SITO'];

        $result = mysql_query($sql);
        if (!$result ) die ("Error in controllo UTENTE:<b> $sql </b> ".mysql_error());           
     
        return $result;
}

function is_logged () {
  
  return (isset($_SESSION['ID_OPE']) && $_SESSION['ID_OPE'] != NULL);
   
}

function carica_label () {
       if (isset($_SESSION['LABEL']) && sizeof($_SESSION['LABEL']) > "0" )  return true;

       $sql = "SELECT ID,upper(CODICE) CODICE,DESCRIZIONE FROM label";
       $result = mysql_query($sql);
       $testi = array();
       if (!$result ) die ("Error in caricamento  Label:<b> $sql </b> ".mysql_error());           
       while ($cur_rec = mysql_fetch_assoc($result)) {
          $testi[$cur_rec['CODICE']] = $cur_rec['DESCRIZIONE'];
       }
       
       $_SESSION['LABEL'] = $testi;
       
       
       return true;
}

function cerca_label ($chiave) {
      
      if (!isset($_SESSION['LABEL']) || sizeof($_SESSION['LABEL']) < "1" ) { 
        return $chiave;
      }

      if (!array_key_exists(strtoupper($chiave),$_SESSION['LABEL'])) return $chiave; 
      $valore = $_SESSION['LABEL'][strtoupper($chiave)];
      if ($valore == null || $valore==" ") return $chiave;
      return $valore;
}

function get_config($chiave) {
     $risultato = db_query_fk_cod('configurazioni',$chiave,'CODICE');
     $cur_rec = mysql_fetch_assoc($risultato);
     return $cur_rec['VALORE'];
     
}

function select_news($p_where = "N") {
    $Tavola = "catalogo";
    $where = "tipo = '".$p_where."'";
    $data = date("y-m-d");
    $where .= " and (da_data is null or da_data = '0000-00-00' or da_data <= '".$data."')";
    $where .= " and (a_data is null or  a_data = '0000-00-00' or  a_data >= '".$data."')";
    $risultato = db_query_generale($Tavola,$where,"da_data desc");
    return $risultato;
}

function db_StartsWith($stringa, $valore){
    // Recommended version, using strpos
    return strpos($stringa, $valore) === 0;
}

function db_converti_data($p_data) {
    $app = "";
    if ( $p_data != null && $p_data != '0000-00-00') {
        $array = explode("-",  substr($p_data,0,10));
        $app = $array[2]."/".$array[1]."/".$array[0];
    }
    return $app;
}

function ControlloData($data){
	if(!preg_match ("/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/", $data)){
		return false;
	}else{
		$arrayData = explode("/", $data);
		$Giorno = $arrayData[0];
		$Mese = $arrayData[1];
		$Anno = $arrayData[2];
		if(!checkdate($Mese, $Giorno, $Anno)){
			return false;
		}else{
			return true;
		}
	}
}

function ControlloRangeData($data1,$data2,$ugualeammesso = true){

     if (ControlloData($data1) && ControlloData($data2)) {
      		$arrayData1 = explode("/", $data1);
      		$Giorno1 = $arrayData1[0];
      		$Mese1 = $arrayData1[1];
      		$Anno1 = $arrayData1[2];
          $d1 = mktime(0,0,0,$Mese1,$Giorno1,$Anno1);

          $arrayData2 = explode("/", $data2);
      		$Giorno2 = $arrayData2[0];
      		$Mese2 = $arrayData2[1];
      		$Anno2 = $arrayData2[2];
          $d2 = mktime(0,0,0,$Mese2,$Giorno2,$Anno2);
          
          if ($ugualeammesso) return ($d1 <= $d2);
          else return ($d1 < $d2);
          
     } 
     return true;
    
}

function converti_data_per_db($data) {
    if ($data == null && $data == " ") return "";
    
		$arrayData = explode("/", $data);
		$Giorno = $arrayData[0];
		$Mese = $arrayData[1];
		$Anno = $arrayData[2];

     return $Anno."-".$Mese."-".$Giorno;
}

function db_esplodi_data($p_cur_rec,$p_nome_campo) {

  if ( $p_cur_rec[$p_nome_campo] != null && $p_cur_rec[$p_nome_campo] != '0000-00-00') {
      $array = explode("-",  $p_cur_rec[$p_nome_campo]);
      $p_cur_rec[$p_nome_campo.'_D']  = $array[2];
      $p_cur_rec[$p_nome_campo.'_M']  = $array[1];
      $p_cur_rec[$p_nome_campo.'_Y']  = $array[0];
  }  else {
      $p_cur_rec[$p_nome_campo.'_D']  = "";
      $p_cur_rec[$p_nome_campo.'_M']  = "";
      $p_cur_rec[$p_nome_campo.'_Y']  = "";
  }
  
  return $p_cur_rec;

}

function colonna_numerica($tipo) {
       if (db_StartsWith($tipo,'int')) return true;
       if (db_StartsWith($tipo,'decimal')) return true;
       if (db_StartsWith($tipo,'float')) return true;
       if (db_StartsWith($tipo,'number')) return true;
       return false; 
}

function db_convnum($valore) {
   if ($valore != null && $valore != " " && $valore != "") {
       $valore = str_replace('.','',$valore);
       $valore = str_replace(',','.',$valore);
       return $valore;
   }
   return null;
}

function db_isnumeric($valore,$solopositivo = false,$zeroammesso = true){
   if ($valore != null && $valore != " " && $valore != "") {
      if ($solopositivo) {
         if ($zeroammesso) return is_numeric(db_convnum($valore)) && (db_convnum($valore) >= 0) ;
         else return is_numeric(db_convnum($valore)) && (db_convnum($valore) > 0) ;
      }
      return is_numeric(db_convnum($valore));                                 
   }
   return true;  
}

function db_ispercentuale($valore,$solopositivo = false,$zeroammesso = true){

   if (!db_isnumeric($valore,$solopositivo,$zeroammesso)) {
       return false;
   } 
   if (db_convnum($valore) > 100) return false;
   if (!$solopositivo && db_convnum($valore) < -100) return false;  
   return true;  
}

function db_is_int($var,$solopos = false)
{
    //This function removes all characters other than numbers
    //Esta fun��o limpa a url e conserva apenas os numeros
    
    if ($solopos) $var1 = preg_replace("/[^0-9]/", "", $var);
    else $var1 = preg_replace("/-[^0-9]/", "", $var);
    
    if ($var1 == $var) return true;
    return false;
}

function db_visinteger($valore) {
   return db_visnumeric($valore,0);
}
function db_visimporti($valore) {
   return db_visnumeric($valore,2);
}
function db_visnumeric($valore,$decimali){
   
   if (is_numeric($valore))
   return number_format($valore, $decimali, ',', '.');
   else return $valore;  
}

function db_get_esercizi() { 
    
    $ris = db_query_generale('esercizi',"1=1",'DATA_INIZIO desc');
    $oggi = mktime(0,0,0,date ("m"),date ("d"),date ("Y"));
    $inizio = "";
    $fine = "";
    $corrente = false;
    $esercizi = array();
    while ($cur_rec = mysql_fetch_assoc($ris)) {
        
        // se il giro prima ho trovato il corrente allora adesso
        // ho il precendente ed esco
        if ($corrente){ 
           $esercizi[] = $cur_rec;
           return $esercizi; 
        }
        
        $inizio = "";
        $fine = "";        
        if ( $cur_rec['DATA_INIZIO'] != null && $cur_rec['DATA_INIZIO'] != '0000-00-00') {
          $array = explode("-",$cur_rec['DATA_INIZIO']);
          $inizio = mktime(0,0,0,$array[1],$array[2],$array[0]);
        }
        if ( $cur_rec['DATA_FINE'] != null && $cur_rec['DATA_FINE'] != '0000-00-00') {
          $array = explode("-",$cur_rec['DATA_FINE']);
          $fine = mktime(0,0,0,$array[1],$array[2],$array[0]);
        }

        if ($fine != "" && $inizio != "" && $oggi >= $inizio && $oggi <= $fine) {
           $esercizi[] = $cur_rec;
           $corrente = true;
        }
        
    }

    return $esercizi;
}

function db_get_progressivo ($nome,$esercizio=null) {

    $where = "codice = '".$nome."' and ";
    if ($esercizio != null) $where .= " esercizio = ".$esercizio;
    else $where .= " esercizio is null";
    
    $ris = db_query_generale('progressivi',$where,null);
    $cur_rec = mysql_fetch_assoc($ris);
    
    return $cur_rec;
    
}

function db_agg_progressivo ($id_progressivo,$valore_num,$valore_alfa) {

    $post['VALORE_NUMERICO'] = $valore_num;
    $post['VALORE_ALFA'] = $valore_alfa;
    $post['UTE_UPD'] = $_SESSION['ID_OPE'];
    $post['DATA_UPD'] = "";

    db_update('progressivi',$id_progressivo,$post);
    
       
}

function db_stacca_progressivo ($nome,$esercizio) {

    $rec = db_get_progressivo($nome,$esercizio);
    if ($rec == null) return null;
    
    if ($rec['VALORE_NUMERICO'] == null) $rec['VALORE_NUMERICO'] = 0;
    
    $rec['VALORE_NUMERICO'] = ($rec['VALORE_NUMERICO'] + 1);
    db_agg_progressivo($rec['ID'],$rec['VALORE_NUMERICO'],$rec['VALORE_ALFA']);
    
    
    return $rec;
}

function get_categorie($parent, $level,$array_categorie = null,$sito=false,$motore=false) { 

    //  sito = true filtra  pubblica, scadenza
    //  motore = true toglie gli html.  

    // $parent is the parent of the children we want to see 
   // $level is increased when we go deeper into the tree, 
   //        used to display a nice indented tree 
    
    if ($array_categorie != null) $new_array = $array_categorie;
    else $new_array = array();

    $scadenza = date("Y-m-d");
    $where  = " idparent=".((int)$parent)." ";
    if ($motore) {
       $where  .= " and ( GesHtml is null or GesHtml != 1) ";
    }
    if ($sito) {
       $where  .= " and pubblica = 1 and  ( Scadenza is null or Scadenza = '0000-00-00' or Scadenza > '".$scadenza."' )";
    }
        
    // retrieve all children of $parent 
    $result = mysql_query('SELECT * FROM categorie WHERE  '.$where.' order by ordine;'); 
  
 
    // display each child 
    while ($row = mysql_fetch_array($result)) { 
        // indent and display the title of this child
        $row['Level'] = $level;
        $new_array[] = $row;
 
        // call this function again to display this 
        // child's children 
        $new_array = get_categorie($row['ID'], $level+1,$new_array,$sito,$motore); 
    } 
    
    return $new_array;
} 

function cat_padre($id) {
    $n_cat = db_query_count('categorie','idparent = '.$id,null);
    return ( $n_cat > 0);
}

function cat_con_figli($id) {
    $n_des = db_query_count('destinazioni','idcategoria = '.$id,null);
    return ( $n_des > 0);
}

function get_strutture_hp() {
   
   $ris = motore(null,null,null,null,true);
   return $ris;
}

function get_strutture_box() {
   $ris = motore(null,null,null,null,true);
   return $ris;
}

function get_dettagli_struttura ($struttura,$dadata = null, $adata = null, $puntuale = null) {

   // legge prezzo, immagine stelle, descrizione operatore....

   if (db_is_null($puntuale))  $puntuale = $_SESSION['motore_Puntuale'];

   //echo "strutura ".$cur_rec['Titolo'];  

   
   //ff crociere
   if ($struttura['IDNave'] > 0 ) {   


$select = "select min(importoscontato), min(prezzo), count(*), min(IDDestinazione) from prezzi_crociere where iddestinazione = ".$struttura['ID'];
   if ($dadata != null && $dadata != "Data Partenza:") {
     // converto data per fare query
     $dadata = converti_data_per_db($dadata);  
    //costruisco formato per sommare giorni a data 
    list($anno, $mese, $giorno) = explode("-", $dadata);
    $data1 = date("Y-m-d",mktime(0,0,0,$mese ,$giorno+7 ,$anno));
    if ($puntuale == "I") $dadata = $data1;      
        $select .= ' and data >= "'.$dadata.'" ';
     }  
 
   if ($adata != null && $adata != "Data Arrivo:") {
      $adata = converti_data_per_db($adata);
      list($anno, $mese, $giorno) = explode("-", $adata);
      $data2 = date("Y-m-d",mktime(0,0,0,$mese ,$giorno+7 ,$anno));

      if ($puntuale == "I") $adata = $data2;          
      $select .= ' and data >= "'.$adata.'" ';
   }   

   
   $result = mysql_query($select);
   if (!$result ) return false;
   $dett_viaggi = mysql_fetch_assoc($result);
   if ($dett_viaggi['count(*)'] == 0) return false;



   }
 
   else  {
// ff fine crociere
   
                                                                                         
   $select = "select min(importoscontato), min(prezzo), count(*), min(IDDestinazione) from viaggi where iddestinazione = ".$struttura['ID'];
   if ($dadata != null && $dadata != "Data Partenza:") {
     // converto data per fare query
     $dadata = converti_data_per_db($dadata);  
    //costruisco formato per sommare giorni a data 
    list($anno, $mese, $giorno) = explode("-", $dadata);
    $data1 = date("Y-m-d",mktime(0,0,0,$mese ,$giorno+7 ,$anno));
    if ($puntuale == "I") $dadata = $data1;      
        $select .= ' and datafrom >= "'.$dadata.'" ';
     }  
 
   if ($adata != null && $adata != "Data Arrivo:") {
      $adata = converti_data_per_db($adata);
      list($anno, $mese, $giorno) = explode("-", $adata);
      $data2 = date("Y-m-d",mktime(0,0,0,$mese ,$giorno+7 ,$anno));

      if ($puntuale == "I") $adata = $data2;          
      $select .= ' and datato <= "'.$adata.'" ';
   }   

   
   $result = mysql_query($select);
   if (!$result ) return false;
   $dett_viaggi = mysql_fetch_assoc($result);
   if ($dett_viaggi['count(*)'] == 0) return false;


// ff crociere
}
// ff fine crociere



   $select = "select * from stelle where id = ".$struttura['IDStelle'];
   $result = mysql_query($select);
   if (!$result ) return false;
   $dett_stelle = mysql_fetch_assoc($result);

//ff cond
  // echo "condiz".$struttura['IDCondizioni'];
   if (isset($struttura['IDCondizioni'])) {
   $select = "select * from condizioni_speciali where id = ".$struttura['IDCondizioni'];
   $result = mysql_query($select);
   if (!$result ) return false;
   $dett_condizioni = mysql_fetch_assoc($result);
   //echo "nome".$dett_condizioni['Nome'];
   }
// ff cond fine



   
     //ff crociere
   if ($struttura['IDNave'] > 0 ) { 
    
    $select = "select min(importoscontato), min(prezzo), count(*) from prezzi_crociere where iddestinazione = ".$struttura['ID'] ;

   //  $select .= ' and ID = "'.$dett_viaggi['min(ID)'].'" ';
     $data_sistema= date ("d/m/Y");
     $data_filtro = converti_data_per_db($data_sistema);
     $select .= ' and data >= "'.$data_filtro.'" ';
     $result = mysql_query($select);
     if (!$result ) return false;   
     $dett_validi = mysql_fetch_assoc($result);



}
   else {
   // fine crociere

   
   //ff  filtro prezzi validi con data sistema
     $select = "select min(importoscontato), min(prezzo), count(*) from viaggi where iddestinazione = ".$struttura['ID'] ;

   //  $select .= ' and ID = "'.$dett_viaggi['min(ID)'].'" ';
     $data_sistema= date ("d/m/Y");
     $data_filtro = converti_data_per_db($data_sistema);
     $select .= ' and datato >= "'.$data_filtro.'" ';
     $result = mysql_query($select);
     if (!$result ) return false;   
     $dett_validi = mysql_fetch_assoc($result);

   //ff fine

// ff crociere
}
// fine crociere

   $ris['Prezzo'] = $dett_viaggi['min(importoscontato)'];
   $ris['Catalogo'] = $dett_viaggi['min(prezzo)'];
   $ris['Stelle'] = $dett_stelle['Img'];  
//ffcond   
   if (isset($dett_condizioni)) {
   $ris['Condizioni'] = $dett_condizioni['Img'];  
  // echo "ris=".$ris['Condizioni'];
   }
   else $ris['Condizioni'] = '0'; 
   
 
   //ff
   if ($dett_validi['min(prezzo)'] == '0' || $dett_validi['min(prezzo)'] == "")  {
      $ris['Prezzo'] = "Su richiesta";
      $ris['Catalogo'] = "Su richiesta";
  }

   
// echo "prezzo".$ris['Prezzo']; 

   return $ris;
}

function motore ($categoria = null, $dadata = null, $adata = null, $puntuale = null, $hp = false, $preferiti = false) {

   $risultato = array();
   $sql = "select * from destinazioni where pubblica = 1";
   if ($categoria != null) {
      $sql .= " and idcategoria = ".$categoria;
   }
   if ($hp) {
      $sql .= " and homepage = 1";
   }   
   
   $sql .= " order by ordine";

    if ($preferiti) {
      if (isset($_SESSION['DATI_UTENTE']['ID'])) {
      $sql =   "SELECT destinazioni . *
                  FROM destinazioni
            INNER JOIN preferiti ON destinazioni.id = preferiti.iddestinazione
                 WHERE destinazioni.pubblica =1
                   AND preferiti.idutente = ".$_SESSION['DATI_UTENTE']['ID']."
              ORDER BY destinazioni.ordine";    
      }   else {
           $sql = "select * from destinazioni where 1 = 2";
      }           
    }
    
    $result = mysql_query($sql);
    if (!$result ) null;//gesiremo errori;
    
    $dettagli = array();
    $struttura = array();
    while ($cur_rec = mysql_fetch_assoc($result) ) {
        
        $dettagli = array();
        $struttura = array();    
        //if (db_is_null($categoria)) {
        if (!controllaValiditaCategoria($cur_rec['IDCategoria'])) continue;
        //}
        $dettagli = get_dettagli_struttura($cur_rec,$dadata,$adata);
        if (!$dettagli) continue;
        
    
        $struttura = $cur_rec;
        $struttura['Prezzo'] =  $dettagli['Prezzo'];
        $struttura['Stelle'] =  $dettagli['Stelle'];
        $struttura['Catalogo'] = $dettagli['Catalogo'];
   //ffcond 
     
        $struttura['Condizioni'] = $dettagli['Condizioni'];

        $risultato[] = $struttura;     
    }
  
    return $risultato;

}

function select_categorie($selezionata,$onchange,$sito,$motore) {
    $cat = get_categorie(0,0,null,$sito,$motore);
    
    echo "\t <select name=\"categoria\" id=\"categoria\" onchange=\"".$onchange."\" >\n";
    echo "\t\t<option value=\"\">Categoria :</option>\n";
    for ($i=0;$i<count($cat);$i++) {
       if (!db_is_null($cat[$i]['Html'])) continue;
       
       $selected = "";
       if ($selezionata == $cat[$i]['ID'])   {
           $selected = " selected ";
       }
       
       $disabled="";
       if (cat_padre($cat[$i]['ID'])) $disabled="disabled=\"disabled\"";
       
 
       //$stile = "style=\"padding-left:".(10*($cat[$i]['Level']+1))."px;\"";
      $titolo = $cat[$i]['Titolo']; 	
      for ($x=0;$x<=($cat[$i]['Level']+1);$x++) {
         $titolo = '&nbsp;&nbsp;&nbsp;'.$titolo;      
      }      
 

       echo "\t\t<option value=\"".$cat[$i]['ID']."\" ".$disabled." ".$selected."> ".$titolo." </option>\n";
    }

    echo "\t </select>\n";  
}


function isStrutturaValida($id) {
    
    $sql = "select * from destinazioni where id = ".$id;
    $result = mysql_query($sql);
    if (!$result) return false;
    
    if ( mysql_num_rows($result) == 0) return false; // struttura non trovata
    $cur_rec = mysql_fetch_assoc($result);
    
    if ($cur_rec['Pubblica'] == 0) return false; // non pubblica

    $cat= controllaValiditaCategoria($cur_rec['IDCategoria']);
    if (!$cat) return false; // categoria non valida
    
    return true;
}

function controllaValiditaCategoria($id) {
    if ($id == null || $id == 0 || $id == " ") return true;
           
    $result = mysql_query('SELECT * FROM categorie WHERE  id = '.$id);
    if (!$result) return false;
    
    if ( mysql_num_rows($result) == 0) return false; // categoira non trovata
    $cur_rec = mysql_fetch_assoc($result);
    
    if ($cur_rec['Pubblica'] == 0) return false; // non pubblica
    
    if ($cur_rec['Scadenza'] != null && substr($cur_rec['Scadenza'],0,4) != "0000") { 
       if (!ControlloRangeData(date("d/m/Y"),db_converti_data($cur_rec['Scadenza']),true)) return false; // controlla che sysdate sia minore di scadenza
    }
    
    return controllaValiditaCategoria($cur_rec['IDParent']);
    
}     

// solo sito per ora $app = carica_label();
?>

