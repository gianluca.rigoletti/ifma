<?php


  function get_tipo_tratta ($p_pnr) {
      $tipo = "DO";
      
      $nazp_old = "**";
      $conp_old = "**";
      $naza_old = "**";
      $cona_old = "**";
      $nazp = "**";
      $naza = "**";
      $conp = "**";
      $cona = "**";
      
      $tratte = db_query_fk_cod('dettaglio_tratte',$p_pnr,'pnr');
      while ($cur_rec_trat = mysql_fetch_assoc($tratte)) {
        $dett_arrivo = db_query_fk_cod('aeroporti',$cur_rec_trat['CODICE_ARRIVO'],'codice');
        $arrivo = mysql_fetch_assoc($dett_arrivo);
        $naza = $arrivo['NAZI_ID'];
        
        $dett_partenza = db_query_fk_cod('aeroporti',$cur_rec_trat['CODICE_PARTENZA'],'codice');
        $partenza = mysql_fetch_assoc($dett_partenza);
        $nazp = $partenza['NAZI_ID'];
        
        $cona = get_continente($naza);
        $conp = get_continente($nazp);
        
        // tratta domestica confronto con tratta precedente
        if ($naza == $nazp ) {
          // non � la prima tratta che considero;
          if($naza_old != "**" ) {
             // controllo continenti attuali con continenti vecchi
             if ( $conp != $conp_old || $conp != $cona_old || $cona != $conp_old || $cona != $cona_old) {
                  $tipo = 'IC';
                  break; // esco tanto sono nel caso peggiore
             }
             // controllo nazioni attuali con nazioni vecchie
             if ( $nazp != $nazp_old || $nazp != $naza_old || $naza != $nazp_old || $naza != $naza_old) {
                  $tipo = 'IN';
             }
          }
        } else { // tratta non domestica verifico continente
          $tipo = 'IN';
          if ($cona != $conp) {
             $tipo = 'IC';
             break; // esco tanto sono nel caso peggiore
          } 
          if($naza_old != "**" ) {
             // controllo continenti attuali con continenti vecchi
             if ( $conp != $conp_old || $conp != $cona_old || $cona != $conp_old || $cona != $cona_old) {
                  $tipo = 'IC';
                  break; // esco tanto sono nel caso peggiore
             }
          }          
        }

        $naza_old = $naza;
        $nazp_old = $nazp;
        $cona_old = $cona;
        $conp_old = $conp;
      }
       
      return $tipo;
  
  }
  
  function get_continente ($naz_id) {
     $risultato = db_query_mod("nazioni",$naz_id);
     $cur_rec = mysql_fetch_assoc($risultato);
     return $cur_rec['CONTINENTE'];
  }
  

  function get_aereoporto ($nazi_id) {
     $p_where_trt = " nazi_id ='". $nazi_id."'";
     $risultato = db_query_generale("aeroporti",$p_where_trt,'');
   //  $cur_rec = mysql_fetch_assoc($risultato);
     return $risultato;
  }

   function get_numero_bgt ($p_pnr) {
     $p_where_trt = " pnr ='" . $p_pnr."'";
     $risultato = db_query_count("dettaglio_passeggeri",$p_where_trt,'');
     return $risultato;
}


  function get_aero_partenza ($p_pnr) {
      $p_where_trt = " pnr ='".$p_pnr."'";
      $risTrattaMin = db_query_generale('dettaglio_tratte',$p_where_trt,'data_partenza asc, ora_partenza asc');
      $cur_rec_tratta = mysql_fetch_assoc($risTrattaMin);
      return $cur_rec_tratta['CODICE_PARTENZA'];
  } 

  function get_aero_arrivo ($p_pnr) {
      $tratte = array();
      $i = 0; // numero tratte
      $p_where_trt = " pnr ='".$p_pnr."'";
      $risTratte = db_query_generale('dettaglio_tratte',$p_where_trt,'data_partenza asc, ora_partenza asc');
      while ($cur_rec_tratte = mysql_fetch_assoc($risTratte)) {
           // se l'utente ha definito una destinazione finale allora fine
           if ($cur_rec_tratte['FLAG_DESTINAZIONE'] == "S") return $cur_rec_tratte['CODICE_ARRIVO'];
           // altrimenti memorizzo tutto in un array
           $tratte[++$i] = $cur_rec_tratte;	
      }
      
      // non ho trovato nulla!!
      if ($i==0) return null;
      
      //se la partenza della prima tratta � diversa dall'arrivo dell'ultima allora ho trovato la dest.
      if ( $tratte[1]['CODICE_PARTENZA'] != $tratte[$i]['CODICE_ARRIVO'] ) return $tratte[$i]['CODICE_ARRIVO'];

     for ($x=1;$x<=$i;$x++){
         if ($x==$i) return $tratte[$x]['CODICE_PARTENZA'];
         if ( $tratte[$x]['CODICE_PARTENZA'] == $tratte[$x+1]['CODICE_ARRIVO'] ) return $tratte[$x+1]['CODICE_PARTENZA'];
     }
     // se arrivo qui vuol dire che non ho trovato nulla!
     return null;
  } 

  function get_percorso ($p_pnr) {
      $codice_arrivo = get_aero_arrivo($p_pnr);
      $percorso = "";
      $p_where_trt = " pnr ='".$p_pnr."'";
      $risTratte = db_query_generale('dettaglio_tratte',$p_where_trt,'data_partenza asc, ora_partenza asc');
      $primo = true;
      while ($cur_rec_tratte = mysql_fetch_assoc($risTratte)) {
          
          if ($primo) {
            $percorso = $cur_rec_tratte['CODICE_PARTENZA']."@#@".$cur_rec_tratte['CODICE_ARRIVO'];
            $primo = false;
          }
          else {
             $percorso .= "@#@".$cur_rec_tratte['CODICE_ARRIVO'];
          } 
           
          if ($cur_rec_tratte['CODICE_ARRIVO'] == $codice_arrivo) break;
      }
      
      return $percorso;
  } 

  function get_pnr_per_date($da_data, $a_data) {
     
     $where = " ( data_vendita >=".$da_data." or data_vendita is null)";
     $where .= " and ( data_vendita <=".$a_data." or data_vendita is null)";
     $where .= " and (validato ='S')";
     $where .= " and replace( pnr, '_', '@@' ) NOT LIKE '%@@%' ";
     $risultato = db_query_generale("testata",$where,'pnr');
     return $risultato;
  }

  function get_pnr_per_mesi($da_mese, $a_mese) {
     $da_data  = $da_mese."01";
     $a_data  = " last_day(".$a_mese."01)";
     return get_pnr_per_date($da_data,$a_data);
  }

  function get_pnr_per_mesi_pers ($da_mese, $a_mese, $and) {
     $da_data  = $da_mese."01";
     $a_data  = " last_day(".$a_mese."01)";
     return get_pnr_per_date_pers($da_data,$a_data, $and);
  }


  function get_pnr_per_date_pers($da_data, $a_data, $and) {
     
     $where = " ( data_vendita >=".$da_data." or data_vendita is null)";
     $where .= " and ( data_vendita <=".$a_data." or data_vendita is null)";
     $where .= " and (validato ='S')";
     $where .= " and replace( pnr, '_', '@@' ) NOT LIKE '%@@%' ";
     $where .= "  $and";
     $risultato = db_query_generale("testata",$where,'pnr');
     return $risultato;
  }



  function gestisci_riemissioni($record_pnr) {
     $where = " pnr like '%".$record_pnr['PNR']."%' ";
     $where .= " and pnr != '".$record_pnr['PNR']."' ";
     $where .= " and (validato ='S')";
     $risultato = db_query_generale("testata",$where,'pnr');
     while ($r = mysql_fetch_assoc($risultato)) {
        $record_pnr['IMPORTO'] = $record_pnr['IMPORTO'] + $r['IMPORTO'];
        $record_pnr['TASSE'] = $record_pnr['TASSE'] + $r['TASSE'];
        $record_pnr['TARIFFA'] = $record_pnr['TARIFFA'] + $r['TARIFFA'];  
      
     } 
     return $record_pnr;
  }

  // copiata da manuale php on line
  function array_sort($array, $on, $order=SORT_ASC)
  {
      $new_array = array();
      $sortable_array = array();
  
      if (count($array) > 0) {
          foreach ($array as $k => $v) {
              if (is_array($v)) {
                  foreach ($v as $k2 => $v2) {
                      if ($k2 == $on) {
                          $sortable_array[$k] = $v2;
                      }
                  }
              } else {
                  $sortable_array[$k] = $v;
              }
          }
  
          switch ($order) {
              case SORT_ASC:
                  asort($sortable_array);
              break;
              case SORT_DESC:
                  arsort($sortable_array);
              break;
          }
  
          foreach ($sortable_array as $k => $v) {
              $new_array[$k] = $array[$k];
          }
      }
  
      return $new_array;
  }

  function storico_estrazione($nome_file,$programma,$tipo) {
     $insert = "insert into elaborazioni (nome_file,utente,programma,tipo) ";
     $insert .= " values ('".$nome_file.".".$tipo."','".$_SESSION['CODICE_OPE']."','".$programma."','".$tipo."')";
     $result = mysql_query($insert);
     if (! $result ) die ("Error in aggiornamento:<b> $insert </b> ".mysql_error());
  
  }
  
  function apri_file ($nome_file) {
    if ($nome_file == null) $nome_file = "null"; 
    echo "    <script type=\"text/javascript\">
     function ApriFile(nomeFile,tipo)
    {
         if (nomeFile == null) nomeFile = '".$nome_file."' + '.' + tipo;
         frmRub = open(\"../../estrazioni/\"+nomeFile, \"winLOV\", \"scrollbars=yes,resizable=yes,width=660,height=480,top=100,left=200\");
     }
    </script>";  
  }
  
  function fine_elaborazione($pdf = true,$tipo = 'csv') {
    echo "
      <form id=\"Testata\" name=\"Testata\" action=\"\" method=\"post\">
      <table>
              <tr><td colspan=\"2\"  class=\"Messaggio\"> Elaborazione terminata con successo </td></tr>
              
              <tr><td colspan=\"2\" class=\"px\" height=\"30\"></td></tr>
      
                <tr>
                <td class=\"label\" width=\"25%\"></td>
                <td width=\"75%\"> 
                   <!--input type=\"submit\" name=\"Return\" value=\"Indietro\"-->
                   <input type=\"button\" name=\"Apri File Csv\" value=\"Apri File ".$tipo."\" onClick=\"ApriFile(null,'".$tipo."')\">";
           if ($pdf)   echo " <input type=\"button\" name=\"Apri File Pdf\" value=\"Apri File Pdf\" onClick=\"ApriFile(null,'pdf')\">";
     echo "
                </td>
                </tr> 
            
      </table>
      </form>";  
  }

?>
