<?php



?>

<!DOCTYPE html>
<html lang="it">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

<?php    
  if (isset($Titolo)) {
     print("<title>".$Titolo."</title>");
  }
?>    



    <!-- Bootstrap -->
    <link href="../Html/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../Html/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../Html/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="../Html/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">

    <link href="../Html/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../Html/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../Html/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../Html/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../Html/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <link href="../Html/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    
    
    
    
    <!-- jVectorMap -->
    <link href="css/maps/jquery-jvectormap-2.0.3.css" rel="stylesheet"/>

    <!-- Custom Theme Style -->
    <link href="css/custom.css?v=1.1" rel="stylesheet">
    
    <!-- jQuery -->
    <script src="../Html/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../Html/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
  
    
    <script src="../Html/vendors/fastclick/lib/fastclick.js"></script>   
    <script src="../Html/vendors/nprogress/nprogress.js"></script>
    <script src="../Html/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>

    <script src="../Html/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../Html/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../Html/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>    
    <script src="../Html/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../Html/vendors/datatables.net-buttons/js/buttons.Html5.min.js"></script>
    <script src="../Html/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../Html/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../Html/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../Html/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../Html/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../Html/vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="../Html/vendors/devbridge-autocomplete/dist/jquery.autocomplete.js"></script>
    <script src="../Html/vendors/select2/dist/js/select2.full.min.js"></script>    
    <!-- iCheck -->
    <script src="../Html/vendors/iCheck/icheck.min.js"></script>    

    <script src="../Html/vendors/validator/validator.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="js/custom.js"></script>  
                
      


    <script type="text/javascript" src="../Html/utility.js"></script>
    <script type="text/javascript" src="../Html/tooltip.js"></script>
    <script type="text/javascript" src="../Html/ajax.js"></script>
    <script type="text/javascript" src="../Html/dataTables.personal.js"></script>         
    <script src="js/moment/moment.min.js"></script>               
    <script src="js/datepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="../Html/jquery.validate.min.js"></script>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >

  </head>


   <style>
   </style>
  
  <script type="text/javascript">
      jQuery.extend(jQuery.validator.messages, {
        required: "Campo Obbligatorio",
        email: "Inserire una mail valida",
        digits: "Inserire solo numeri",
        importoeuro:"Inserire un importo corretto",
        percentuale:"Inserire una percentuale tra 1 e 100" 
    });
  $.validator.methods.importoeuro = function(value, element, param) {
       if (value == null || value == "" ) return true;
       value =  value.replace(".","");
       var myre= /^\d+([\,]{1}\d{2})?$/;
      	if (!(myre.test(value))) {
               return false;
        }
        var imp = value.replace(".","");
        imp = imp.replace(",",".");
    		var impN = parseFloat(imp);

        if (impN < 0) return false;


        return true;

  };

  $.validator.methods.percentuale = function(value, element, param) {
       if (value == null || value == "" ) return true;
       value =  value.replace(".","");
       var myre= /^\d+([\,]{1}\d{2})?$/;
      	if (!(myre.test(value))) {
               return false;
        }
        var imp = value.replace(".","");
        imp = imp.replace(",",".");
    		var impN = parseFloat(imp);

        if (impN < 0) return false;

        if (impN > 100) return false;


        return true;

  };

  Number.prototype.formatMoney = function(c, d, t){
  var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
     return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
   };

$(document).keydown(function(e) {


       var nodeName = e.target.nodeName.toLowerCase();
     if( e.which === 13)
         if ( nodeName === 'textarea' ||
              nodeName === 'button') {
            return true;
          }
         else {
          return false;
          }
     else
          return true;


});

              var validator;
               $(document).ready(function($) { 
                  
                 validator = $("#formG").validate({
                    submitHandler: function(form) {
                        form.submit();
                    }	});  
                  

               });
               
  </script>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.php" class="site_title"><i class="fa fa-slack"></i> <span>YCrm</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
           <?php
              if (is_logged() && !isset($_GET['rub']) )  {
                 echo '
            <div class="profile">
               <div class="profile_pic">
              </div> 
              <div class="profile_info">
                <span>Benvenuto,</span>
                <h2>
                          '. $_SESSION['user_nome'].'                
                </h2>
              </div>
            </div>';
            }
            ?>
            
            <div class="clearfix"></div>
            <!-- /menu profile quick info -->

            <br />


            <?php
               include "menu.php";
               crea_menu();
            ?>
            

          </div>
        </div>
        
 <!-- top navigation -->
        <div class="top_nav">

          <div class="nav_menu">
            <nav class="" role="navigation">
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              
              

              </div>

             

              <ul class="nav navbar-nav navbar-right">
           <?php
         
           
              if (is_logged() && !isset($_GET['rub']) )  {
                 echo '              
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">

                          '. $_SESSION['user_nome'].'  
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="login.php?logout=1"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                    </li>
                  </ul>
                </li> '; 
    
                } ?>

              

              </ul>
            </nav>
          </div>

        </div>
        <!-- /top navigation -->      
        
        

        <!-- page content -->
        <div class="right_col" role="main">  
            