<?php
$m="settori";

require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/files.php';
require '../Librerie/configurazione.php';     

$Titolo = "Gestione Settori";
$Tavola= "settori";

$risultato = db_query_vis($Tavola,'ID');


require '../Librerie/ges_html_top.php';
?>

      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $Titolo;?> </h2>
            <ul class="nav navbar-right panel_toolbox">
                 <button class="btn btn-round btn-primary" type="button" onclick="location.href='ges_settori.php?p_upd=0'">Nuovo</button>
            </ul>
            <div class="clearfix"></div>
          </div>

          <div class="x_content">
          <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">

           <thead>
            <tr>
            <th  width="5%"> &nbsp;</th>
            <th  width="5%"> &nbsp;</th>
            <th  width="5%"> &nbsp;</th>
            <th width="20%"> Id </th>
            <th width="20%"> Descrizione </th>
          </thead>  
          <tbody> 
            <?php
                 while ($cur_rec = mysql_fetch_assoc($risultato))

            {
                 echo "<tr >	";
                    echo " <td ><a href=\"ges_settori.php?p_upd=1&p_id=".$cur_rec['ID']."\"><i class=\"fa fa-edit\"></i></a></td>";
                    echo " <td ><a href=\"vis_attivita_aziende.php?p_settore=".$cur_rec['ID']."\"><i class=\"fa fa-cogs\"></i></a></td>";
                     echo " <td ><a href=\"Javascript:ut_delete_rec('".$cur_rec['ID']."','del_settori')\"><i class=\"fa fa-trash\"></i></a>";
                    echo " <td >".$cur_rec['ID']."   </td>
                          <td >".$cur_rec['DESCRIZIONE']."   </td>
                         </tr> ";
                 }
            ?>
              </tbody>
        </table>

        </div>
      </div>
    </div>



     <script>
      $(document).ready(function() {
        $('#datatable-responsive').DataTable({
                  "bFilter":true,
                  "iDisplayLength": 50,
                 "aaSorting": [[ 3, "asc" ]], 
                         "bStateSave":true,                 
                  "aoColumns": [
                               { "bSortable": false },
                               { "bSortable": false },
                               { "bSortable": false },
                              null,
                              null
                             ]         
        });
      });
    </script>  





<?php require '../Librerie/ges_html_bot.php'; ?>
