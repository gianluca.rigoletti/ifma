<?php
$m="aziende";

require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/files.php';
require '../Librerie/configurazione.php';

$c_files = new files(); 

$Tavola= "aziende";

$indietro = "vis_aziende.php";
if (isset($_GET['back'])) {
   $indietro= $_GET['back'].".php";
}


if ($_GET['p_upd']==1) {
   $Funzione = "Update";
   $Disabilita_chiave = "disabled";
   $Titolo = "Modifica Azienda";
} else {
   $Funzione = "Insert";
   $Disabilita_chiave = "";
   $Titolo = "Nuova Azienda";
}

// se richiamato in update allora devo popolare il form
if ($_GET['p_upd']==1) {
   $risultato = db_query_mod($Tavola,$_GET['p_id']);
   $cur_rec = mysql_fetch_assoc($risultato);
}

// confermo

if ( isset($_POST['Insert']) || isset($_POST['Update']) ) {


   $cur_rec['ID'] = $_POST['ID'];
   $cur_rec['RagioneSociale'] = $_POST['RagioneSociale'];


   $cur_rec['Citta'] = $_POST['Citta'];
   $cur_rec['Indirizzo'] = $_POST['Indirizzo'];
   $cur_rec['Localita'] = $_POST['Localita'];
   $cur_rec['CAP'] = $_POST['CAP'];
   $cur_rec['Provincia'] = $_POST['Provincia'];
   
   $cur_rec['Nazione'] = $_POST['Nazione'];
   $cur_rec['Telefono'] = $_POST['Telefono'];
   $cur_rec['Fax'] = $_POST['Fax'];
   $cur_rec['Email'] = $_POST['Email'];
   $cur_rec['Sito'] = $_POST['Sito'];
   
   $cur_rec['Descrizione'] = $_POST['Descrizione'];
   $cur_rec['Dipendenti'] = $_POST['Dipendenti'];
   $cur_rec['Fatturato'] = $_POST['Fatturato'];
   $cur_rec['Settore'] = $_POST['Settore'];
   $cur_rec['Sito'] = $_POST['Sito'];
   
   

//obbligatorietà


   
   if ( $_POST['RagioneSociale'] == null || $_POST['RagioneSociale'] == " ") {
      $c_err->add("Campo Ragione Sociale Obbligatorio","RagioneSociale");
   }   

  

   // controllo dup-Val

   if ( isset($_POST['Insert']) && db_dup_key($Tavola,$_POST) > 0 )  {
        $c_err->add("Utente Gi&agrave; Inserito","CLIENTE");
   }


   if (!$c_err->is_errore()) {
       if ( isset($_POST['Insert'])) {
	          db_insert($Tavola,$_POST);
            $idaz =  db_get_id();
       }  else {
	          db_update($Tavola,$_POST['ID'],$_POST);
            $idaz =  $_POST['ID'];
       }
       
       // gestione attività 
       
       
       $risAtt = db_query_fk_cod("attivita_per_aziende",$idaz,"IDAZIENDA");
       $delwhere = "";
       
       function arraySearch($val,$array) {
           foreach ($array as $k => $v) {
               
               if ($val == $v) return $k;
           }
           return false;
       }
        
       while ($att = mysql_fetch_assoc($risAtt))  {
              $key = arraySearch($att['IDATTIVITA'], $_POST['attivita']);
              if( $key !== false) {
                  unset($_POST['attivita'][$key]);
              } else {
                  $delwhere = "IDAZIENDA = ".$idaz." and IDATTIVITA = ".$att['IDATTIVITA'];
                  $delete = "delete from attivita_per_aziende where ".$delwhere;
                  $resdel = mysql_query($delete);
              }       
       }
      
       $sqlInsert = "select 1 from attivita_per_aziende where  IDAZIENDA = ".$idaz." and IDATTIVITA =";
       $insertAtt = array();     
       $insertAtt["IDAZIENDA"] = $idaz;   
       foreach ($_POST['attivita'] as $k => $v) {
           if (db_is_null($v)) continue;
           $resSel =   mysql_query($sqlInsert.$v);
            //   die(mysql_num_rows($resSel));
           if (mysql_num_rows($resSel) == 0) {
               
               $insertAtt["IDATTIVITA"] = $v;    
               db_insert("attivita_per_aziende",$insertAtt,false);
           }
       }
       
       
       
       header('Location: '.$indietro);
       exit;
   }
}

// torno indietro

if (isset($_POST['Return'])) {
   header("Location: ".$indietro);
   exit;
}

require '../Librerie/ges_html_top.php';

$c_err->mostra();
?>

<style type="text/css">

 ul.bar_tabs >  li.rightside { 
    background-color: #f5f7fa;
    float: right;
    line-height: 41px;
    padding-left: 5px;
  }
  
</style>

        <script>
          $.validator.setDefaults({ 
              ignore: [],
              invalidHandler: function(e, validator){   
                        if(validator.errorList.length)
                        $('#myTab a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
                    }
          });       
          
          $(document).ready(function() {
              $('#myTab a:last').tab('show');
              $('#myTab a:first').tab('show');

              $('#myTab1 a:last').tab('show');
              $('#myTab1 a:first').tab('show');
          });
      </script>

       <form id="formG" class="form-horizontal form-label-left"  action="" method="post" novalidate>

            <div class="clearfix"></div>
           <div class="" role="tabpanel" data-example-id="togglable-tabs">
           <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab_generale" id="generale-tab" role="tab" data-toggle="tab" aria-expanded="true">Generale</a>
                </li>
                <li role="presentation" class="rightside">
               <button class="cancel btn btn-primary" type="submit" name="Return" value="Return">Indietro</button>
               <button type="submit"  class="btn btn-success" name="<?php echo $Funzione ?>" value="Salva">Salva</button>
               </li>
              </ul>           
              
            <div id="myTabContent" class="tab-content">
            
               <div role="tabpanel" class="tab-pane fade active in" id="tab_generale" aria-labelledby="Generale">
                       <div class="clearfix"></div>
                      <input type="hidden" name="ID" value="<?php if (isset($cur_rec)) echo $cur_rec['ID']; ?>" >
          
                      <div class="item form-group">
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Ragione Sociale <span class="required">*</span>
                        </label>
                        <div class="col-md-11 col-sm-11 col-xs-12">
                            <input type="text" required="true" maxlenght="150" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("RagioneSociale");?>  name="RagioneSociale" id="RAGIONE_SOCIALE" value="<?php if (isset($cur_rec)) echo $cur_rec['RagioneSociale'];?>" size="55"><br />
                      </div>
                      </div>
          
                      <div class="item form-group">
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Indirizzo
                        </label>
                        <div class="col-md-11 col-sm-11 col-xs-12">
                            <input type="text" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("Indirizzo");?> name="Indirizzo" id="Indirizzo" value="<?php if (isset($cur_rec)) echo $cur_rec['Indirizzo'];?>" size="70" maxlength="100"><br />
                      </div>
                      </div>
          
                      <div class="item form-group">
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Citt&agrave; 
                        </label>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <input type="text" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("Citta");?> name="Citta" id="Citta"  value="<?php if (isset($cur_rec)) echo $cur_rec['Citta'];?>" size="30" maxlength="50"><br />
                         </div>
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Telefono
                        </label>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <input type="text"  class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("Telefono");?> name="Telefono" id="Telefono"  value="<?php if (isset($cur_rec)) echo $cur_rec['Telefono'];?>" size="30" maxlength="50"><br />
                      </div>                         
                      </div>
                      
                      <div class="item form-group">
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Locali&agrave; 
                        </label>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <input type="text"  class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("Localita");?> name="Localita" id="Localita"  value="<?php if (isset($cur_rec)) echo $cur_rec['Localita'];?>" size="30" maxlength="50"><br />
                         </div>
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Fax
                        </label>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <input type="text"  class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("Fax");?> name="Fax" id="Fax"  value="<?php if (isset($cur_rec)) echo $cur_rec['Fax'];?>" size="30" maxlength="50"><br />
                      </div>                         
                      </div>                      
                      
                      <div class="item form-group">
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">CAP 
                        </label>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <input type="text"  class="form-control col-md-12 col-xs-12" <?php $c_err->tooltip("CAP");?> name="CAP" id="CAP"  value="<?php if (isset($cur_rec)) echo $cur_rec['CAP'];?>" size="30" maxlength="12"><br />
                      </div>
                       <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Prov. 
                          </label>     
                        <div class="col-md-2 col-sm-2 col-xs-12">
                          <select id="Provincia" name="Provincia"  class="form-control col-md-7 col-xs-12"/>
                         <?php  if (isset($cur_rec['Provincia']))
                                   { db_html_select_cod('province', $cur_rec['Provincia'],'SIGLA','DESCRIZIONE',true,null);
                                }
                                else {db_html_select_cod('province', '','SIGLA','DESCRIZIONE',true,null);
                                }
                           ?>
                          </select>
                         </div>  
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Nazione 
                        </label>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <input type="text"  class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("Nazione");?> name="Nazione" id="Nazione"  value="<?php if (isset($cur_rec)) echo $cur_rec['Nazione'];?>" size="30" maxlength="20"><br />
                      </div>                                                
                      
                      </div>            

          
                      <div class="item form-group">
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Email 
                        </label>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <input type="text" email="true"  class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("Email");?> name="Email" id="Email"  value="<?php if (isset($cur_rec)) echo $cur_rec['Email'];?>" size="30" maxlength="255"><br />
                         </div>

                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Sito
                        </label>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <input type="text" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("Sito");?> name="Sito" id="Sito"  value="<?php if (isset($cur_rec)) echo $cur_rec['Sito'];?>" size="30" maxlength="255"><br />
                         </div>                         
                                             
                      </div>
                      
               <div class="" role="tabpanel" data-example-id="togglable-tabs">                         
               <ul id="myTab1" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab_info" id="info-tab" role="tab" data-toggle="tab" aria-expanded="true">Informazioni</a>
                </li>
            
                <li role="presentation" class=""><a href="#tab_settore" id="settore-tab" role="tab" data-toggle="tab" aria-expanded="false">Settore</a>
                </li>
                <li role="presentation" class=""><a href="#tab_referenti" id="referenti-tab" role="tab" data-toggle="tab" aria-expanded="false">Referenti</a>
                </li>

                <li role="presentation" class=""><a href="#tab_associazione" id="associazione-tab" role="tab" data-toggle="tab" aria-expanded="true">Associazione</a>
                </li>
                
              </ul>           
              
              <div id="myTabContent1" class="tab-content">
              
               <div role="tabpanel" class="tab-pane fade active in" id="tab_info" aria-labelledby="Informazioni">
                       <div class="clearfix"></div>



                      <div class="item form-group">
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Descrizione
                        </label>
                        <div class="col-md-11 col-sm-11 col-xs-12">
                            <textarea class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("Descrizione");?> name="Descrizione" id="Descrizione"><?php if (isset($cur_rec)) echo $cur_rec['Descrizione'];?></textarea><br />
                      </div>
                      </div>
          
                      <div class="item form-group">
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Dipendenti 
                        </label>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                          <select id="Dipendenti" name="Dipendenti"  class="form-control col-md-7 col-xs-12"/>
                         <?php  if (isset($cur_rec['Dipendenti']))
                                   { db_html_select_cod('dipendenti', $cur_rec['Dipendenti'],'ID','DESCRIZIONE',true,null);
                                }
                                else {db_html_select_cod('dipendenti', '','ID','DESCRIZIONE',true,null);
                                }
                           ?>
                          </select>
                         </div>
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Fatturato
                        </label>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                          <select id="Fatturato" name="Fatturato"  class="form-control col-md-7 col-xs-12"/>
                         <?php  if (isset($cur_rec['Fatturato']))
                                   { db_html_select_cod('fatturato', $cur_rec['Fatturato'],'ID','DESCRIZIONE',true,null);
                                }
                                else {db_html_select_cod('fatturato', '','ID','DESCRIZIONE',true,null);
                                }
                           ?>
                          </select>                        
                      </div>                         
                      </div>
                </div> <!-- primo Informazioni -->  
                
               <div role="tabpanel" class="tab-pane fade active in" id="tab_settore" aria-labelledby="Settore">
                       <div class="clearfix"></div>
                        <div class="item form-group">
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Settore
                        </label>                       
                        <div class="col-md-11 col-sm-11 col-xs-12">
                          <select id="Settore" name="Settore"  class="form-control col-md-7 col-xs-12"/>
                         <?php  if (isset($cur_rec['Settore']))
                                   { db_html_select_cod('settori', $cur_rec['Settore'],'ID','DESCRIZIONE',false,null);
                                }
                                else {db_html_select_cod('settori', '','ID','DESCRIZIONE',true,null);
                                }
                           ?>
                          </select> 
                      </div>
                      </div>

                      <div class="item form-group dettattivita">
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Attivit&agrave;
                        </label>                       
                      
                      <?php
                          
                          
                          if (isset($cur_rec['ID']) && $cur_rec['Settore'] >= 0) {
                            $whereaa =  "IDSETTORE = ".$cur_rec['Settore']; 
                            $ris = db_query_fk_cod("attivita_per_aziende",$cur_rec['ID'],"IDAZIENDA");
                            $esiste1 = false;
                            while ($aa = mysql_fetch_assoc($ris))  {
                              if ($esiste1) {
                                  echo "<div class=\"col-md-1 col-sm-1 col-xs-12\"></div>";
                              }
                              echo "<div class=\"col-md-11 col-sm-11 col-xs-12\">
                            <select  name=\"attivita[]\"  class=\"form-control col-md-7 col-xs-12 a_aziende\"/>";
                             
                             db_html_select_cod('attivita_aziende', $aa['IDATTIVITA'],'ID','DESCRIZIONE',true,$whereaa);
                                  
                             
                            echo "</select> 
                                  </div>";
                                $esiste1 = true;
                            } 
                            if (!$esiste1) {
                                echo "<div class=\"col-md-11 col-sm-11 col-xs-12\">
                                     <select  name=\"attivita[]\"  class=\"form-control col-md-7 col-xs-12 a_aziende\"/>";
                               db_html_select_cod('attivita_aziende', '','ID','DESCRIZIONE',true,$whereaa);
                             echo "</select> 
                                  </div>";                            
                            }
                          } else {
                            
                            if (isset($cur_rec['SETTORE']) && $cur_rec['Settore'] >= 0) {
                                $whereaa =  "IDSETTORE = ".$cur_rec['Settore']; 
                                echo "<div class=\"col-md-11 col-sm-11 col-xs-12\">
                                     <select  name=\"attivita[]\"  class=\"form-control col-md-7 col-xs-12 a_aziende\"/>";
                               db_html_select_cod('attivita_aziende', '','ID','DESCRIZIONE',true,$whereaa);
                             echo "</select> 
                                  </div>";
                            } else {
                              echo "<div class=\"col-md-11 col-sm-11 col-xs-12\">
                                 <select  name=\"attivita[]\"  class=\"form-control col-md-7 col-xs-12 a_aziende\"/>";
                             echo "</select> 
                                  </div>";                            
                            
                            }
                          
                          }
                          
                          
                          
                             
                      
                      ?>
                      </div>
                       <div class="item form-group">
                        <div class="col-md-1 col-sm-1 col-xs-1">
                         <button class="btn btn-round btn-primary" type="button" onclick="javascript:add();">+</button>
                        </div>
                     
                      </div>                      

                
                
                
                </div> <!-- tab Settore -->

               <!-- tab referenti --> 
               <div role="tabpanel" class="tab-pane fade active in" style="max-height: 250px;height: 250px;overflow-y:scroll;" id="tab_referenti" aria-labelledby="Referenti">
                       <div class="clearfix"></div>
                        <table class="table table-striped" cellspacing="0" width="90%">
              
                         <thead>
                          <tr>
                          <th  width="40%"> Cognome e Nome</th>
                          <th  width="40%"> Ruolo </th>
                          <th  width="15%"> Socio</th>
                        </thead>                        
                        <tbody>
                      <?php
                        if (isset($cur_rec['ID'])) {
                            $sql = "select Cognome, Nome, RuoloAziendale1,Socio
                                      from persone 
                                     where AziendaID = ".$cur_rec['ID']."
                                      order by Cognome,Nome";
                            $ris = mysql_query($sql);
                                      
                            
                            
                            while ($ass = mysql_fetch_assoc($ris)) {

                                  $sponsor = "";
                                  if ($ass['Socio'] == 1 ) $sponsor ='<i class="fa fa-check text-success"></i>';
                                  
                                  $ruolo['DESCRIZIONE'] = "";
                                  if (!db_is_null($ass['RuoloAziendale1'])) {
                                      $risr = db_query_mod("ruolo_aziendale",$ass['RuoloAziendale1']);
                                      $ruolo=mysql_fetch_assoc($risr);
                                      
                                  }

                                   echo "<tr>";
                                   echo " <td >".$ass['Cognome']." ".$ass['Nome']. "</td>
                                          <td >".$ruolo['DESCRIZIONE']."</td>
                                          <td >".$sponsor."</td>
                                     </tr> ";                            
                            
                            }

                        }
                      ?>
                      </tbody>
                      </table>                   
                </div> <!-- tab referenti -->                   
                
               <!-- tab associazioni --> 
               <div role="tabpanel" class="tab-pane fade active in" style="max-height: 250px;height: 250px;overflow-y:scroll;" id="tab_associazione" aria-labelledby="Associazione">
                       <div class="clearfix"></div>
                        <table class="table table-striped" cellspacing="0" width="90%">
              
                         <thead>
                          <tr>
                          <th  width="40%"> Nome</th>
                          <th  width="15%"> Data Inizio</th>
                          <th  width="15%"> Data Fine</th>
                          <th width="10%">  Sponsor</th>
                          <th width="10%">  Professional</th>
                          <th width="10%">  Fornitore</th>                          
                          <th width="20%"> Note </th>
                        </thead>                        
                        <tbody>
                      <?php
                        if (isset($cur_rec['ID'])) {
                            $sql = "select a.Cognome, a.Nome, b.INIZIO, b.FINE, b.SPONSOR, b.MEMO,b.PROFESSIONAL,b.FORNITORE 
                                      from persone a,
                                           soci_azienda b
                                     where b.IDAZIENDA = ".$cur_rec['ID']."
                                       and a.ID = b.IDPERSONA
                                      order by b.FINE DESC ";
                            $ris = mysql_query($sql);
                                      
                            
                            
                            while ($ass = mysql_fetch_assoc($ris)) {

                                  $sponsor = "";
                                  $professional = "";
                                  $fornitore = "";
                                  if ($ass['SPONSOR'] == 1 ) $sponsor ='<i class="fa fa-check text-success"></i>';
                                  if ($ass['PROFESSIONAL'] == 1 ) $professional ='<i class="fa fa-check text-success"></i>';
                                  if ($ass['FORNITORE'] == 1 ) $fornitore ='<i class="fa fa-check text-success"></i>';

                                   echo "<tr>";
                                   echo " <td >".$ass['Cognome']." ".$ass['Nome']. "</td>
                                          <td >".db_converti_data($ass['INIZIO'])."</td>
                                          <td >".db_converti_data($ass['FINE'])."</td>
                                          <td >".$sponsor."</td>
                                          <td >".$professional."</td>
                                          <td >".$fornitore."</td>                                          
                                          <td >".$ass['MEMO']."</td>
                                     </tr> ";                            
                            
                            }

                        }
                      ?>
                      </tbody>
                      </table>                   
                </div> <!-- tab associazioni -->                
                
                                             
              
              </div> <!-- tab content interno-->
              </div>  <!-- tab tabpanel interno-->  
                
                
                                            
                </div><!-- primo tab -->

           
           </div>  <!-- tab content -->
           </div>  <!-- tab tabpanel -->
          
          
            <div class="ln_solid"></div>

        </form>

<script>

  function add() {
     var v =  $("#Settore").val();
     if ($("#Settore").val() == "") {
          return;
      }
      
      var valori = getAttivitaSettori(v); 
      $(".dettattivita").append(
          '<div class="col-md-1 col-sm-1 col-xs-12"></div><div class="col-md-11 col-sm-11 col-xs-12"><select  name="attivita[]"  class="form-control col-md-7 col-xs-12 a_aziende">'+valori+'</select></div>'       
      );
  }

  function getAttivitaSettori(val) {
   return $.ajax({
       type: "POST",
       url: "popola_dettaglio.php",
       data: "id="+val,
       async: false
     }).responseText;
  }
  
  oldval = $( "#Settore").val();
  $( "#Settore" ).change(function() {
      var newVal = $(this).val();
      if (!confirm("Attenzione: modificando il settore verranno cancellate le attivita' attualmente associate. Continuare?")) {
          $(this).val(oldval); //set back
          return;                           //abort!
      }
      //destroy branches
      oldval = newVal;
      
      $(".a_aziende").val("");
      $(".a_aziende").html(getAttivitaSettori(newVal)); 
     
  });

</script>

<?php require '../Librerie/ges_html_bot.php';


?>
