<?php
$m="persone";
     
require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/files.php';
require '../Librerie/configurazione.php';


function db_html_select_cod_ruolo($p_sel)
{
    $esiste = false;
    $selected = '';
    if ($p_sel == null || $p_sel == "") {
      $selected = 'selected';
      $esiste = true;  
    }    
    $stringa = '<option value = "" '.$selected.'>';
    echo $stringa;
    

    $sql = "select a.ID ID, A.DESCRIZIONE DES1, B.DESCRIZIONE DES2 FROM ruolo_aziendale A , ruolo_aziendale_area B where B.ID= A.IDAREA order by A.DESCRIZIONE, B.DESCRIZIONE ";
    $risultato = mysql_query($sql);
     
    while ($cur_rec = mysql_fetch_assoc($risultato))       
    {
    
        $selected = '';
        if (isset($p_sel) && ($p_sel==$cur_rec["ID"]) ) {
          $selected = 'selected';
          $esiste = true;
        } 
    
        $stringa = '<option value = "'.$cur_rec["ID"].'" '.$selected.' >'.htmlentities($cur_rec["DES1"]. " - ".$cur_rec["DES2"]);
        echo $stringa;
    } 
    
    if (!$esiste) {
       $stringa = '<option value = "?" selected > Non Codificato';
       echo $stringa;
    }
}

$c_files = new files(); 

$Tavola= "persone";

$indietro = "vis_persone.php";
if (isset($_GET['back'])) {
   $indietro= $_GET['back'].".php";
}


if ($_GET['p_upd']==1) {
   $Funzione = "Update";
   $Disabilita_chiave = "disabled";
   $Titolo = "Modifica Persona";
} else {
   $Funzione = "Insert";
   $Disabilita_chiave = "";
   $Titolo = "Nuova Persona";
}
    
// se richiamato in update allora devo popolare il form
if ($_GET['p_upd']==1) {
   $risultato = db_query_mod($Tavola,$_GET['p_id']);
   $cur_rec = mysql_fetch_assoc($risultato);
   $cur_rec['DataNascita_vis'] = "";
   if (isset($cur_rec['DataNascita'])) $cur_rec['DataNascita_vis'] = db_converti_data($cur_rec['DataNascita']);
}


// confermo

if ( isset($_POST['Insert']) || isset($_POST['Update']) ) {


   $cur_rec['ID'] = $_POST['ID'];
   $cur_rec['ID_migrazione'] = $_POST['ID_migrazione'];

   // check 
   $_POST['Consenso675'] = !isset($_POST['Consenso675']) ? 0 : 1;
   $_POST['Consenso675esteso'] = !isset($_POST['Consenso675esteso']) ? 0 : 1;
   $_POST['Socio'] = !isset($_POST['Socio']) ? 0 : 1;
   $_POST['Sponsor'] = !isset($_POST['Sponsor']) ? 0 : 1;

   $cur_rec['Cognome'] = $_POST['Cognome'];
   $cur_rec['Nome'] = $_POST['Nome'];
   $cur_rec['Titolo'] = $_POST['Titolo'];
   $cur_rec['Sesso'] = $_POST['Sesso'];
   $cur_rec['DataNascita_vis'] = $_POST['DataNascita_vis'];
   $cur_rec['AziendaID'] = $_POST['AziendaID'];
   $cur_rec['Dipartimento'] = $_POST['Dipartimento'];
   $cur_rec['Citta'] = $_POST['Citta'];
   $cur_rec['Indirizzo'] = $_POST['Indirizzo'];
   $cur_rec['Localita'] = $_POST['Localita'];
   $cur_rec['CAP'] = $_POST['CAP'];
   $cur_rec['Provincia'] = $_POST['Provincia'];
   
   $cur_rec['Nazione'] = $_POST['Nazione'];
   $cur_rec['Telefono'] = $_POST['Telefono'];
   $cur_rec['Fax'] = $_POST['Fax'];
   $cur_rec['Cellulare'] = $_POST['Cellulare'];
   $cur_rec['EmailAziendale'] = $_POST['EmailAziendale'];
   $cur_rec['EmailPersonale'] = $_POST['EmailPersonale'];
  
   
   $cur_rec['AreaAttivita'] = $_POST['AreaAttivita'];
   $cur_rec['RuoloAziendale1'] = $_POST['RuoloAziendale1'];
   $cur_rec['RuoloAziendale2'] = $_POST['RuoloAziendale2'];
   $cur_rec['RuoloEffettivo'] = $_POST['RuoloEffettivo'];
   $cur_rec['Socio'] = $_POST['Socio'];
   $cur_rec['Sponsor'] = $_POST['Sponsor'];
   $cur_rec['Consenso675'] = $_POST['Consenso675'];
   $cur_rec['Consenso675esteso'] = $_POST['Consenso675esteso'];
   $cur_rec['AssistenteNome'] = $_POST['AssistenteNome'];
   $cur_rec['AssistenteTel'] = $_POST['AssistenteTel'];
   $cur_rec['AssistenteEmail'] = $_POST['AssistenteEmail'];
   $cur_rec['Selezione'] = $_POST['Selezione'];
   $cur_rec['DataUltimaModifica'] = $_POST['DataUltimaModifica'];
   $cur_rec['IDIfmaInternational'] = $_POST['IDIfmaInternational'];
   $cur_rec['RuoloAziendale1'] = $_POST['RuoloAziendale1'];
   $cur_rec['AziendaID'] = $_POST['Azienda'];
   $cur_rec['RuoloAziendale2'] = $_POST['RuoloAziendale2'];
   
   
   
                                  
   

//obbligatorietà


   
   if ( $_POST['Cognome'] == null || $_POST['Cognome'] == " ") {
      $c_err->add("Campo Cognome Obbligatorio","Cognome");
   }   

   if ( $_POST['Nome'] == null || $_POST['Nome'] == " ") {
      $c_err->add("Campo Nome Obbligatorio","Nome");
   }
   
   
   if ( $_POST['DataNascita_vis'] == null || $_POST['DataNascita_vis'] == " ")  {
         $_POST['DataNascita'] = null;
    }else {
       if (!ControlloData($_POST['DataNascita_vis'])) {
          $c_err->add("Campo Data Nasciata Errato","DataNascita_vis");
       } else {
           $_POST['DataNascita'] = converti_data_per_db($_POST['DataNascita_vis']);
         }
    }
   
  

   // controllo dup-Val

   if ( isset($_POST['Insert']) && db_dup_key($Tavola,$_POST) > 0 )  {
        $c_err->add("Utente Gi&agrave; Inserito","CLIENTE");
   }


   if (!$c_err->is_errore()) {
       if ( isset($_POST['Insert'])) {
	          db_insert($Tavola,$_POST);
            $idpers =  db_get_id();
       }  else {
	          db_update($Tavola,$_POST['ID'],$_POST);
            $idpers =  $_POST['ID'];
       }
       
       // gestione potenzialità 
       
       // gestione attività 
       
       $risAtt = db_query_fk_cod("potenzialita_persona",$idpers,"IDPERSONA");
       $delwhere = "";
       
       function arraySearch($val,$array) {
           foreach ($array as $k => $v) {
               
               if ($val == $v) return $k;
           }
           return false;
       }
        
       while ($att = mysql_fetch_assoc($risAtt))  {
              $key = arraySearch($att['IDPOTENZIALITA'], $_POST['potenzialita']);
              if( $key !== false) {
                  unset($_POST['potenzialita'][$key]);
              } else {
                  $delwhere = "IDPERSONA = ".$idpers." and IDPOTENZIALITA = ".$att['IDPOTENZIALITA'];
                  $delete = "delete from potenzialita_persona where ".$delwhere;
                  $resdel = mysql_query($delete);
              }       
       }
      
       $sqlInsert = "select 1 from potenzialita_persona where  IDPERSONA = ".$idpers." and IDPOTENZIALITA =";
       $insertAtt = array();     
       $insertAtt["IDPERSONA"] = $idpers;   
       foreach ($_POST['potenzialita'] as $k => $v) {
           if (db_is_null($v)) continue;
           $resSel =   mysql_query($sqlInsert.$v);
            //   die(mysql_num_rows($resSel));
           if (mysql_num_rows($resSel) == 0) {
               
               $insertAtt["IDPOTENZIALITA"] = $v;    
               db_insert("potenzialita_persona",$insertAtt,false);
           }
       }       
       

    
       
       
       header('Location: '.$indietro);
       exit;
   }
}

// torno indietro

if (isset($_POST['Return'])) {
   header("Location: ".$indietro);
   exit;
}

require '../Librerie/ges_html_top.php';

$c_err->mostra();


?>

<style type="text/css">

 ul.bar_tabs >  li.rightside { 
    background-color: #f5f7fa;
    float: right;
    line-height: 41px;
    padding-left: 5px;
  }
  
</style>

        <script>
          $.validator.setDefaults({ 
              ignore: [],
              invalidHandler: function(e, validator){   
                        if(validator.errorList.length)
                        $('#myTab a[href="#' + jQuery(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show')
                    }
          });       
          
          $(document).ready(function() {
              $('#myTab a:last').tab('show');
              $('#myTab a:first').tab('show');

              $('#myTab1 a:last').tab('show');
              $('#myTab1 a:first').tab('show');
          });
      </script>

       <form id="formG" class="form-horizontal form-label-left"  action="" method="post" novalidate>

            <div class="clearfix"></div>
           <div class="" role="tabpanel" data-example-id="togglable-tabs">
           <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active"><a href="#tab_generale" id="generale-tab" role="tab" data-toggle="tab" aria-expanded="true">Generale</a>
                </li>
                <li role="presentation" class="rightside">
               <button class="cancel btn btn-primary" type="submit" name="Return" value="Return">Indietro</button>
               <button type="submit"  class="btn btn-success" name="<?php echo $Funzione ?>" value="Salva">Salva</button>
               </li>
              </ul>           
              
            <div id="myTabContent" class="tab-content">
            
               <div role="tabpanel" class="tab-pane fade active in" id="tab_generale" aria-labelledby="Generale">
                       <div class="clearfix"></div>
                      <input type="hidden" name="ID" value="<?php if (isset($cur_rec)) echo $cur_rec['ID']; ?>" >
          
                      <div class="item form-group">
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Cognome <span class="required">*</span>
                        </label>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <input type="text"  class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("Cognome");?>  name="Cognome" id="COGNOME" value="<?php if (isset($cur_rec)) echo $cur_rec['Cognome'];?>" size="50"><br />
                      </div>
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Nome <span class="required">*</span>
                        </label>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <input type="text"  class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("Nome");?> name="Nome" id="Nome"  value="<?php if (isset($cur_rec)) echo $cur_rec['Nome'];?>" size="30" maxlength="50"><br />
                        </div> 
                      
                      </div>
          
          
                     <div class="item form-group">
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Titolo 
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                           <select id="Titolo" name="Titolo"  class="form-control col-md-7 col-xs-12"/>
                         <?php  if (isset($cur_rec['Titolo']))
                                   { db_html_select_cod('titolo', $cur_rec['Titolo'],'Abbreviazione','Titolo',true,null);
                                }
                                else {db_html_select_cod('titolo', '','Abbreviazione','Titolo',true,null);
                                }
                           ?>
                          </select>                            
                         </div>
                        
                         
                         
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Sesso
                        </label>
                        <div class="col-md-1 col-sm-1 col-xs-12">
                        <!--select id="Sesso" name="Sesso"  class="form-control col-md-7 col-xs-12" -->
                          <?php $valori = array(''=> '-- seleziona --',
                        'M' => 'Maschio',
                        'F' => 'Femmina'
                         );
                        $valore = "";
                        if (isset($cur_rec)) $valore =  $cur_rec['Sesso'];
                        lista_gen('Sesso',$valore,"form-control col-md-7 col-xs-12",null,$valori);  ?>
                          <!--/select-->  
                        </div>
                         
                        
                         
                         <?php datepicker('DataNascita_vis'); ?>
                         <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Data di Nascita
                        </label>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <input type="text"  class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("DataNascita_vis");?> name="DataNascita_vis" id="DataNascita_vis"  value="<?php if (isset($cur_rec)) echo $cur_rec['DataNascita_vis'];?>" size="30" maxlength="50"><br />
                        </div>
            
            
                        <?php
                           $c1 = "";
                           $c2 = ""; 
                           if (isset($cur_rec['Sponsor']) && $cur_rec['Sponsor'] == 1) $c1= 'checked="checked"';
                           if (isset($cur_rec['Socio']) && $cur_rec['Socio'] == 1) $c2= 'checked="checked"';
                        
                         echo '                                                        
                      <div class="checkbox  col-md-2 col-xs-12" >
                        <label>
                         <input type="checkbox" '.$c1.' class="flat" name="Sponsor" value="1" />
                          Sponsor
                      </label>        
                       
                        <label>
                         <input type="checkbox" '.$c2.'  class="flat" name="Socio" value="1" />
                          Socio
                      </label> 
                       </div>  ';
                     
                          ?>
                              
                      
                       </div>
                       
                      
                     
          
          
          
          
          
                                                                                                                        
                  
                      
               <div class="" role="tabpanel" data-example-id="togglable-tabs">                         
               <ul id="myTab1" class="nav nav-tabs bar_tabs" role="tablist">
                 <li role="presentation" class="active"><a href="#tab_azienda" id="azienda-tab" role="tab" data-toggle="tab" aria-expanded="false">Azienda</a>
                </li>
                <li role="presentation" class=""><a href="#tab_recapiti" id="recapiti-tab" role="tab" data-toggle="tab" aria-expanded="true">Recapiti</a>
                </li>
                <li role="presentation" class=""><a href="#tab_associazione" id="associazione-tab" role="tab" data-toggle="tab" aria-expanded="true">Associazione</a>
                </li>
                <li role="presentation" ><a href="#tab_potenzialita" id="potenzialita-tab" role="tab" data-toggle="tab" aria-expanded="true">Potenzilit&agrave;</a>
                </li>                
                <li role="presentation" class=""><a href="#tab_assistente" id="assistente-tab" role="tab" data-toggle="tab" aria-expanded="false">Assistente</a>
                </li>
                <li role="presentation" ><a href="#tab_azioni" id="azioni-tab" role="tab" data-toggle="tab" aria-expanded="true">Azioni</a>
                </li>                
               
              </ul>           
              
              <div id="myTabContent1" class="tab-content">
            
            
            
            
            <div role="tabpanel" class="tab-pane fade active in" id="tab_azienda" aria-labelledby="Azienda">
                       <div class="clearfix"></div>
                       
              <div class="item form-group">
             <label class="control-label col-md-2 col-sm-2 col-xs-12" for="">Azienda
                        </label>
                        <div class="col-md-10 col-sm-10 col-xs-12">
                          <select id="AziendaID" name="AziendaID"  class=" select2_single form-control col-md-7 col-xs-12"/>
                         <?php  if (isset($cur_rec['AziendaID']))
                                   { db_html_select_cod('aziende', $cur_rec['AziendaID'],'ID','RagioneSociale',true,null);
                                }
                                else {db_html_select_cod('aziende', '','ID','RagioneSociale',true,null);
                                }
                           ?>
                          </select>                        
                      </div>   
             </div>
             
             
              <div class="item form-group">
              <label class="control-label col-md-2 col-sm-2 col-xs-12" for="">Dipartimento 
                        </label>
                        
                        <div class="col-md-10 col-sm-10 col-xs-12">
                            <input type="text" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("Dipartimento");?> name="Dipartimento" id="Dipartimento" value="<?php if (isset($cur_rec)) echo $cur_rec['Dipartimento'];?>" size="70" maxlength="100"><br />
                      </div>
             </div>
             
             
             
             <div class="item form-group">
             <label class="control-label col-md-2 col-sm-2 col-xs-12" for="">Ruolo effettivo principale
                        </label>
                        <div class="col-md-10 col-sm-10 col-xs-12">
                          <select id="RuoloAziendale1" name="RuoloAziendale1"  class="select2_single form-control col-md-7 col-xs-12"/>
                         <?php  if (isset($cur_rec['RuoloAziendale1']))
                                   { db_html_select_cod_ruolo($cur_rec['RuoloAziendale1']);
                                }
                                else {db_html_select_cod_ruolo('');
                                }
                           ?>
                          </select>                        
                      </div>   
             </div>
             
             
             <div class="item form-group">
             <label class="control-label col-md-2 col-sm-2 col-xs-12" for="">Ruolo effettivo secondario
                        </label>
                        <div class="col-md-10 col-sm-10 col-xs-12">
                          <select id="RuoloAziendale2" name="RuoloAziendale2"  class="select2_single form-control col-md-7 col-xs-12"/>
                         <?php  if (isset($cur_rec['RuoloAziendale2']))
                                   { db_html_select_cod_ruolo($cur_rec['RuoloAziendale2']);
                                }
                                else {db_html_select_cod_ruolo('');
                                }
                           ?>
                          </select>                        
                      </div>  
                      
                      
                     
                       
             </div>
             
              <div class="item form-group">
             <label class="control-label col-md-2 col-sm-2 col-xs-12" for="">Da biglietto da visita
                        </label>
                          <div class="col-md-10 col-sm-10 col-xs-12">
                            <input type="text" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("RuoloEffettivo");?> name="RuoloEffettivo" id="RuoloEffettivo" value="<?php if (isset($cur_rec)) echo $cur_rec['RuoloEffettivo'];?>" size="70" maxlength="100"><br />                        
                      </div>   
             </div>
             
             
             
             
             </div>
            
            
            
            
            
            
              
            
               <div role="tabpanel" class="tab-pane fade active in" id="tab_recapiti" aria-labelledby="Recapiti">
                       <div class="clearfix"></div>


          
                      <div class="item form-group">
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Indirizzo 
                        </label>
                        
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <input type="text" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("Indirizzo");?> name="Indirizzo" id="Indirizzo" value="<?php if (isset($cur_rec)) echo $cur_rec['Indirizzo'];?>" size="70" maxlength="100"><br />
                      </div>
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Telefono Fisso
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                          <input type="text" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("Telefono");?> name="Telefono" id="Telefono" value="<?php if (isset($cur_rec)) echo $cur_rec['Telefono'];?>" size="70" maxlength="100"><br />                               
                      </div>                         
                      </div>
                      
                      
                    <div class="item form-group">
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Citt&agrave; 
                        </label>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <input type="text" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("Citta");?> name="Citta" id="Citta"  value="<?php if (isset($cur_rec)) echo $cur_rec['Citta'];?>" size="30" maxlength="50"><br />
                         </div>
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Cellulare
                        </label>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <input type="text"  class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("Cellulare");?> name="Cellulare" id="Cellulare"  value="<?php if (isset($cur_rec)) echo $cur_rec['Cellulare'];?>" size="30" maxlength="50"><br />
                        </div>                         
                      </div>  
                      
                      
                   <div class="item form-group">
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Locali&agrave; 
                        </label>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <input type="text"  class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("Localita");?> name="Localita" id="Localita"  value="<?php if (isset($cur_rec)) echo $cur_rec['Localita'];?>" size="30" maxlength="50"><br />
                         </div>
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Fax
                        </label>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <input type="text"  class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("Fax");?> name="Fax" id="Fax"  value="<?php if (isset($cur_rec)) echo $cur_rec['Fax'];?>" size="30" maxlength="50"><br />
                      </div>                         
                      </div>   
                      
                   <div class="item form-group">
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">CAP 
                        </label>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                            <input type="text"  class="form-control col-md-12 col-xs-12" <?php $c_err->tooltip("CAP");?> name="CAP" id="CAP"  value="<?php if (isset($cur_rec)) echo $cur_rec['CAP'];?>" size="30" maxlength="12"><br />
                      </div>
                       <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Prov. 
                          </label>     
                        <div class="col-md-2 col-sm-2 col-xs-12">
                          <select id="Provincia" name="Provincia"  class="form-control col-md-7 col-xs-12"/>
                         <?php  if (isset($cur_rec['Provincia']))
                                   { db_html_select_cod('province', $cur_rec['Provincia'],'SIGLA','DESCRIZIONE',true,null);
                                }
                                else {db_html_select_cod('province', '','SIGLA','DESCRIZIONE',true,null);
                                }
                           ?>
                          </select>
                         </div>  
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Email Aziendale
                        </label>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <input type="text" email="true"  class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("Email");?> name="Email" id="Email"  value="<?php if (isset($cur_rec)) echo $cur_rec['EmailAziendale'];?>" size="30" maxlength="255"><br />
                         </div>                                                
                      
                      </div>
                      
                      
                       <div class="item form-group">
                            <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Nazione 
                        </label>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <input type="text"  class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("Nazione");?> name="Nazione" id="Nazione"  value="<?php if (isset($cur_rec)) echo $cur_rec['Nazione'];?>" size="30" maxlength="20"><br />
                      </div>    
                           <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Email Personale 
                        </label>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <input type="text" email="true"  class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("EmailPers");?> name="EmailPers" id="EmailPers"  value="<?php if (isset($cur_rec)) echo $cur_rec['EmailPersonale'];?>" size="30" maxlength="255"><br />
                         </div>
                      
                         </div>
                      
                      
                      <div class="item form-group">
                          
         <?php
                           $c1 = "";
                           $c2 = ""; 
                           if (isset($cur_rec['Consenso675']) && $cur_rec['Consenso675'] == 1) $c1= 'checked="checked"';
                           if (isset($cur_rec['Consenso675esteso']) && $cur_rec['Consenso675esteso'] == 1) $c2= 'checked="checked"';
                        
                         echo '                                                        
                      <div class="checkbox  col-md-6 col-xs-12" >
                        <label>
                         <input type="checkbox" '.$c1.' class="flat" name="Consenso675" value="1" />
                          Consenso 196/03 
                      </label>        
                       
                       </div>  ';
                       
                         echo '                                                        
                      <div class="checkbox  col-md-6 col-xs-12" >
                        <label>
                         <input type="checkbox" '.$c2.' class="flat" name="Consenso675esteso" value="1" />
                          Consenso 196/03 Esteso
                      </label>        
                       
                       </div>  ';                       
                     
                          ?>                          
                      
  
                         </div>
                      
                      
                      
                </div> <!-- primo Recapiti -->  
                
               <div role="tabpanel" class="tab-pane fade active in" id="tab_assistente" aria-labelledby="Assistente">
                       <div class="clearfix"></div>
                       
                       
                        <div class="item form-group">
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Nome Assistente 
                        </label>
                        
                        <div class="col-md-11 col-sm-11 col-xs-12">
                            <input type="text" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("AssistenteNome");?> name="AssistenteNome" id="AssistenteNome" value="<?php if (isset($cur_rec)) echo $cur_rec['AssistenteNome'];?>" size="70" maxlength="100"><br />
                      </div>                                                 
                     </div>

                     <div class="item form-group">
                     <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Telefono Assistente
                        </label>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                          <input type="text" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("AssistenteTel");?> name="AssistenteTel" id="AssistenteTel" value="<?php if (isset($cur_rec)) echo $cur_rec['Telefono'];?>" size="70" maxlength="100"><br />                               
                      </div>
                     </div>

                     <div class="item form-group">
                      <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Email Assistente
                      </label>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <input type="text" email="true"  class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("AssistenteEmail");?> name="AssistenteEmail" id="AssistenteEmail"  value="<?php if (isset($cur_rec)) echo $cur_rec['AssistenteEmail'];?>" size="30" maxlength="255"><br />
                         </div>                                                

                     </div>


                </div>
               <!-- tab potenzialita --> 
               <div role="tabpanel" class="tab-pane fade active in" id="tab_potenzialita" aria-labelledby="Potenzilit&agrave;">
                       <div class="clearfix"></div>

                      <div class="item form-group dettattivita">
                        <label class="control-label col-md-1 col-sm-1 col-xs-12" for="">Potenzilit&agrave;
                        </label>                       
                      
                      <?php
                        if (isset($cur_rec['ID'])) {
                            $ris = db_query_fk_cod("potenzialita_persona",$cur_rec['ID'],"IDPERSONA");
                            $esiste1 = false;
                            while ($aa = mysql_fetch_assoc($ris))  {
                              if ($esiste1) {
                                  echo "<div class=\"col-md-1 col-sm-1 col-xs-12\"></div>";
                              }
                              echo "<div class=\"col-md-11 col-sm-11 col-xs-12\">
                            <select  name=\"potenzialita[]\"  class=\"form-control col-md-7 col-xs-12 a_aziende\"/>";
                             
                             db_html_select_cod('potenzialita', $aa['IDPOTENZIALITA'],'ID','DESCRIZIONE',true,"");
                                  
                             
                            echo "</select> 
                                  </div>";
                                $esiste1 = true;
                            } 
                            if (!$esiste1) {
                                echo "<div class=\"col-md-11 col-sm-11 col-xs-12\">
                                     <select  name=\"potenzialita[]\"  class=\"form-control col-md-7 col-xs-12 a_aziende\"/>";
                               db_html_select_cod('potenzialita', '','ID','DESCRIZIONE',true,"");
                             echo "</select> 
                                  </div>";                           
                            }
                          } else {
                            
                                echo "<div class=\"col-md-11 col-sm-11 col-xs-12\">
                                     <select  name=\"potenzialita[]\"  class=\"form-control col-md-7 col-xs-12 a_aziende\"/>";
                               db_html_select_cod('potenzialita', '','ID','DESCRIZIONE',true,"");
                             echo "</select> 
                                  </div>";
                            } 
                      ?>
                      </div>
                       <div class="item form-group">
                        <div class="col-md-1 col-sm-1 col-xs-1">
                         <button class="btn btn-round btn-primary" type="button" onclick="javascript:add();">+</button>
                        </div>
                     
                      </div>                      
                </div> <!-- tab potenzialita -->
                                

               <!-- tab azioni --> 
               <div role="tabpanel" class="tab-pane fade active in" style="max-height: 250px;height: 250px;overflow-y:scroll;" id="tab_azioni" aria-labelledby="Azioni">
                       <div class="clearfix"></div>
                        <table class="table table-striped" cellspacing="0" width="90%">
              
                         <thead>
                          <tr>
                          <th  width="40%"> Azione</th>
                          <th  width="10%"> Data</th>
                          <th width="40%"> Tipo </th>
                          <th width="10%"> Esito </th>
                        </thead>                        
                        <tbody>
                      <?php
                        if (isset($cur_rec['ID'])) {
                            $sql = "select a.Nome, a.Data, a.Tipo,b.IDTIPORISULTATO 
                                      from azioni a,
                                           persone_azione b
                                     where b.IDPERSONA = ".$cur_rec['ID']."
                                       and a.ID = b.IDAZIONE
                                      order by a.Data,a.ID";
                            $ris = mysql_query($sql);
                                      
                            $rist = db_query_vis("tipo_azione",'ID');
                            $t=array();
                            while ($tipi=mysql_fetch_assoc($rist)) {
                                $t[$tipi['ID']] = $tipi['DESCRIZIONE']; 
                            }

                            $rise = db_query_vis("tipo_risultato",'ID');
                            $e=array();
                            while ($tipi=mysql_fetch_assoc($rise)) {
                                $e[$tipi['ID']] = $tipi['DESCRIZIONE']; 
                            }
                            
                            
                            if ($ris) {
                                while ($az = mysql_fetch_assoc($ris)) {
                                       $tipo = "";
                                       $esito = "";
                                       if (!db_is_null($az['Tipo'])) {
                                            $tipo = $t[$az['Tipo']];
                                       }
                                       if (!db_is_null($az['IDTIPORISULTATO'])) {
                                            $esito = $e[$az['IDTIPORISULTATO']];
                                       }

                                       echo "<tr>";
                                       echo " <td >".$az['Nome']."   </td>
                                              <td >".db_converti_data($az['Data'])."</td>
                                              <td >".$tipo."</td>
                                              <td >".$esito."</td>
                                         </tr> ";                            
                                
                                }
                            }

                        }
                      ?>
                      </tbody>
                      </table>                   
                </div> <!-- tab Azioni -->

               <!-- tab associazioni --> 
               <div role="tabpanel" class="tab-pane fade active in" style="max-height: 250px;height: 250px;overflow-y:scroll;" id="tab_associazione" aria-labelledby="Associazione">
                       <div class="clearfix"></div>
                        <table class="table table-striped" cellspacing="0" width="90%">
              
                         <thead>
                          <tr>
                          <th  width="20%"> Azienda</th>
                          <th  width="15%"> Data Inizio</th>
                          <th  width="15%"> Data Fine</th>
                          <th width="10%">  Sponsor</th>
                          <th width="10%">  Professional</th>
                          <th width="10%">  Fornitore</th>
                          <th width="20%"> Note </th>
                        </thead>                        
                        <tbody>
                      <?php
                        if (isset($cur_rec['ID'])) {
                            $sql = "select a.RagioneSociale, b.INIZIO, b.FINE, b.SPONSOR, b.MEMO,b.PROFESSIONAL,b.FORNITORE
                                      from aziende a,
                                           soci_azienda b
                                     where b.IDPERSONA = ".$cur_rec['ID']."
                                       and a.ID = b.IDAZIENDA
                                      order by b.INIZIO";
                            $ris = mysql_query($sql);
                                      
                            
                            
                            while ($ass = mysql_fetch_assoc($ris)) {

                                  $sponsor = "";
                                  $professional = "";
                                  $fornitore = "";
                                  if ($ass['SPONSOR'] == 1 ) $sponsor ='<i class="fa fa-check text-success"></i>';
                                  if ($ass['PROFESSIONAL'] == 1 ) $professional ='<i class="fa fa-check text-success"></i>';
                                  if ($ass['FORNITORE'] == 1 ) $fornitore ='<i class="fa fa-check text-success"></i>';

                                   echo "<tr>";
                                   echo " <td >".$ass['RagioneSociale']."   </td>
                                          <td >".db_converti_data($ass['INIZIO'])."</td>
                                          <td >".db_converti_data($ass['FINE'])."</td>
                                          <td >".$sponsor."</td>
                                          <td >".$professional."</td>
                                          <td >".$fornitore."</td>
                                          <td >".$ass['MEMO']."</td>
                                     </tr> ";                            
                            
                            }

                        }
                      ?>
                      </tbody>
                      </table>                   
                </div> <!-- tab associazioni -->

                                             
              
              </div> <!-- tab content interno-->
              </div>  <!-- tab tabpanel interno-->  
                
                
                                            
             </div> <!-- primo tab-->  

           
           </div>  <!-- tab content -->
           </div>  <!-- tab tabpanel -->
          
          
            <div class="ln_solid"></div>

        </form>

<script>

  function add() {

      
      var valori = getAttivitaSettori(); 
      $(".dettattivita").append(
          '<div class="col-md-1 col-sm-1 col-xs-12"></div><div class="col-md-11 col-sm-11 col-xs-12"><select  name="potenzialita[]"  class="form-control col-md-7 col-xs-12 a_aziende">'+valori+'</select></div>'       
      );
  }

  function getAttivitaSettori() {
   return $.ajax({
       type: "POST",
       url: "popola_dettaglio_potenzialita.php",
       async: false
     }).responseText;
  }
  


</script>

<!-- Select2 -->
    <script>
      $(document).ready(function() {
        $(".select2_single").select2({
          placeholder: "",
          allowClear: true
        });
      });
    </script>
    <!-- /Select2 -->




<?php require '../Librerie/ges_html_bot.php';

?>
