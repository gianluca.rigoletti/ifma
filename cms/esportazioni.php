<?php

date_default_timezone_set('Europe/Paris');


require '../Librerie/connect.php';
require '../Librerie/html.php';

$dett_persone = false;  
if ($_GET['tipo'] == "a") {

    if (!isset($_GET['id'])) {
        header("Location: vis_azioni.php");
       exit;    
    }
  $dett_persone = true;
  $titolo = " Partecipanti ";
  $nome = "partecipanti_".date('YmdHis').".csv";
  $sql = "select b.Titolo, b.Cognome, b.Nome,
                      AziendaID Azienda,
                      b.Dipartimento,
                      b.Indirizzo, b.Citta, b.Localita,b.CAP,b.Provincia,  \"\" Regione,
                      b.Telefono,b.Cellulare, b.Fax,
                      b.EmailAziendale,
                      \"\" Ruolo1Area,
                      b.RuoloAziendale1 Ruolo1,
                      \"\" Ruolo2Area,
                      b.RuoloAziendale2 Ruolo2,
                      b.RuoloEffettivo,
                      \"\" SettoreMacro,
                      \"\" SettoreSpecifico,
                      \"\" Dipendenti,
                      \"\" Fatturato, 
                      \"\" ReferenteSocio,
                      \"\" TipoReferenteSocio                    
           from persone_azione a, persone b
          where a.IDPERSONA = b.ID
            and a.IDAZIONE =  ".($_GET['id'])."
          order by b.Cognome, b.Nome   
   ";        
}

$Titolo = "Esportazione ".$titolo;
$ok = false;

                 

$result = mysql_query($sql);

if (!$result ) die (mysql_error());

$num_fields = mysql_num_fields($result);
$headers = array();
for ($i = 0; $i < $num_fields; $i++) {
    $headers[] = strtolower(mysql_field_name($result , $i));
}

$fp = fopen("../estrazioni/".$nome, 'w');
if ($fp && $result) {
    fputcsv($fp, $headers,";");
    while ($row =  mysql_fetch_assoc($result)) {
         
        if ($dett_persone) {
            $aziendaRis = null;
            if (!db_is_null($row['Azienda'] ))  {
                $azienda = db_query_generale("aziende", "ID = ".  $row['Azienda'],"ID");
                $aziendaRis = mysql_fetch_assoc($azienda);
                $row['Azienda']  = isset($aziendaRis['RagioneSociale']) ? $aziendaRis['RagioneSociale'] : "";
            }
            
            if (!db_is_null($row['Ruolo1'] ))  {
                $r1 = db_query_generale("ruolo_aziendale", "ID = ".  $row['Ruolo1'],"ID");
                $r1Ris = mysql_fetch_assoc($r1);
                $row['Ruolo1']  =$r1Ris['DESCRIZIONE'];     
                
                $r1A = db_query_generale("ruolo_aziendale_area", "ID = ".  $r1Ris['IDAREA'],"ID");
                $r1ARis = mysql_fetch_assoc($r1A);
                $row['Ruolo1Area']  =$r1ARis['DESCRIZIONE'];       
            }
                                
            if (!db_is_null($row['Ruolo2'] ))  {
                $r1 = db_query_generale("ruolo_aziendale", "ID = ".  $row['Ruolo2'],"id");
                $r1Ris = mysql_fetch_assoc($r1);
                $row['Ruolo2']  =$r1Ris['DESCRIZIONE'];     
                
                $r1A = db_query_generale("ruolo_aziendale_area", "ID = ".  $r1Ris['IDAREA'],"ID");
                $r1ARis = mysql_fetch_assoc($r1A);
                $row['Ruolo2Area']  =$r1ARis['DESCRIZIONE'];       
            }

            if (!db_is_null($row['Provincia'])) {
                $r1 = db_query_generale("province", "SIGLA = '".  $aziendaRis['Provincia']."'","SIGLA");
                $r1Ris = mysql_fetch_assoc($r1);
                $row['Regione']  =$r1Ris['REGIONE'];                 
            }            
            
            if (isset($aziendaRis['Settore'])) {
                $r1 = db_query_generale("settori", "ID = ".  $aziendaRis['Settore'],"ID");
                $r1Ris = mysql_fetch_assoc($r1);
                $row['SettoreMacro']  =$r1Ris['DESCRIZIONE'];                 
            }
            if (isset($aziendaRis['Dipendenti'])) {
                $r1 = db_query_generale("dipendenti", "ID = ".  $aziendaRis['Dipendenti'],"ID");
                $r1Ris = mysql_fetch_assoc($r1);
                $row['Dipendenti']  =$r1Ris['DESCRIZIONE'];                 
            }
            
            if (isset($aziendaRis['Fatturato'])) {
                $r1 = db_query_generale("fatturato", "ID = ".  $aziendaRis['Fatturato'],"ID");
                $r1Ris = mysql_fetch_assoc($r1);
                $row['Fatturato']  =$r1Ris['DESCRIZIONE'];                 
            } 
            
            if (isset($aziendaRis['ID'])) {
                $r1 = db_query_generale("attivita_per_aziende", "IDAZIENDA = ".  $aziendaRis['ID'],"IDATTIVITA");
                $r1Ris = mysql_fetch_assoc($r1);    
               
                if (isset($r1Ris['IDATTIVITA'])) {
                    $r1A = db_query_generale("attivita_aziende", "ID = ".  $r1Ris['IDATTIVITA'],"ID");
                    $r1ARis = mysql_fetch_assoc($r1A);
                    $row['SettoreSpecifico']  =$r1ARis['DESCRIZIONE'];       
                }
                
                $r1 = db_query_generale("soci_azienda", "IDAZIENDA = ".  $aziendaRis['ID']."  and FINE >= '".date("Y-m-d")."'" ,"IDPERSONA");
                $r1Ris = mysql_fetch_assoc($r1);    
                if (isset($r1Ris['IDPERSONA'])) {
                
                    if ($r1Ris['PROFESSIONAL'] == 1)  $row['TipoReferenteSocio'] =  'PROFESSIONAL';
                    elseif ($r1Ris['FORNITORE'] == 1)  $row['TipoReferenteSocio'] =  'FORNITORE';
                    elseif ($r1Ris['SPONSOR'] == 1)  $row['TipoReferenteSocio'] =  'SPONSOR';
                    else  $row['TipoReferenteSocio'] =  'SOCIO';
                    
                    $r1A = db_query_generale("persone", "ID = ".  $r1Ris['IDPERSONA'],"ID");
                    $r1ARis = mysql_fetch_assoc($r1A);
                    $row['ReferenteSocio']  =$r1ARis['Cognome']." ".$r1ARis['Nome'];                      
                }
                               
                
            }                       
              
              
                
        }  
    
    
        fputcsv($fp, array_values($row),";","\"");
    }
}

fclose($fp);


                 $ok = true;                 
                 
 

require '../Librerie/ges_html_top.php';

$c_err->mostra();



    ?>


            <?php
               if ($ok) {
                  echo "<tr><td><a href=\"f_download.php?filename=".$nome."\"><i class=\"fa fa-file-excel-o\"></i></a></td>
                            <td><a href=\"f_download.php?filename=".$nome."\">
                             Download file CSV </a></td></tr>";
               }               
            ?>   
                   
            <tr><td class="px" height="20"></td></tr>
            
         
             
            <table >
            <tr><td class="px" height="100"></td></tr> 
            </table>
        </td></tr></table>

<?php require '../Librerie/ges_html_bot.php'; ?>
