<?php
ob_start();
require '../Librerie/connect.php';

// test
//$_POST['idazione'] = "5973";
//$_POST['formid'] = "nessuno";


   if (!isset($_POST['idazione']) || db_is_null($_POST['idazione'])) {
      termina(array("COD"=>"ko","MSG"=>"Manca il parametro id azione"));
   }
   $az = $_POST['idazione'];
   
   if (!isset($_POST['formid']) || db_is_null($_POST['formid'])) {
      termina(array("COD"=>"ko","MSG"=>"Manca il parametro id form"));
   }   
  
   // carica tutti
   if ($_POST['formid'] == "tutti") {
       $sql = "INSERT INTO `persone_azione`(`IDPERSONA`, `IDAZIONE`) select a.ID, $az from persone a where not exists (select 'x' from persone_azione b where b.IDPERSONA = a.ID and b.IDAZIONE = $az)";
       $ris = mysql_query($sql);
       if (! $ris ) termina(array("COD"=>"ko","MSG"=>mysql_error()));
       ok();
   }
   
   // svuota tutti
   if ($_POST['formid'] == "nessuno") {
       $sql = "delete from `persone_azione` where IDAZIONE = $az";
       $ris = mysql_query($sql);
       if (! $ris ) termina(array("COD"=>"ko","MSG"=>mysql_error()));
       ok();   
   }
   

   // form soci 
   if ($_POST['formid'] == "soci") {
   
       if (db_is_null($_POST['SOCI_TIPI'])) {
          termina(array("COD"=>"ko","MSG"=>"Valorizzare la tipologia di socio"));
       }
       
       $campo = " a.IDPERSONA ";
       $from = " soci_azienda a ";
       $where = " 1 = 1 ";
       switch ($_POST['SOCI_TIPI']) {
            case "S":
                $where .= " and a.SPONSOR = 0 and a.FORNITORE = 0 AND a.PROFESSIONAL = 0 ";
                break;
            case "SP":
                $where .= " and a.SPONSOR = 1 ";
                break;
            case "F":
                $where .= " and a.FORNITORE = 1  ";
                break;
            case "P":
                $where .= " and a.PROFESSIONAL = 1  ";
                break;                
       }
       
       if (isset($_POST['SOCI_SCADUTI']) && $_POST['SOCI_SCADUTI'] == 1 ) {
          $where .= " and a.FINE >= '".date("Y-m-d", strtotime(" -2 months"))."'";
       } else {
          $where .= " and a.FINE >= '".date("Y-m-d")."'";
       }
       
       includi_escludi_secco($campo,$from,$where,$_POST['SOCI_IE']);
       
       ok();
   }  // form soci 
   
   
   // form soci in scadenza
   if ($_POST['formid'] == "sociScandeza") {
   
       if (db_is_null($_POST['SOCI_TIPI'])) {
          termina(array("COD"=>"ko","MSG"=>"Valorizzare la tipologia di socio"));
       }
       
       if (db_is_null($_POST['SCA_INI']) || !ControlloData($_POST['SCA_INI'])) {
          termina(array("COD"=>"ko","MSG"=>"Valorizzare correttamente la scadenza iniziale"));
       }

       if (db_is_null($_POST['SCA_FIN']) || !ControlloData($_POST['SCA_FIN'])) {
          termina(array("COD"=>"ko","MSG"=>"Valorizzare correttamente la scadenza iniziale"));
       }    

       if (!ControlloRangeData($_POST['SCA_INI'],$_POST['SCA_FIN'],false)) {
          termina(array("COD"=>"ko","MSG"=>"Range di scadenze errato"));
       }            
       
        
       
       $campo = " a.IDPERSONA ";
       $from = " soci_azienda a ";
       $where = " 1 = 1 ";
       switch ($_POST['SOCI_TIPI']) {
            case "S":
                $where .= " and a.SPONSOR = 0 and a.FORNITORE = 0 AND a.PROFESSIONAL = 0 ";
                break;
            case "SP":
                $where .= " and a.SPONSOR = 1 ";
                break;
            case "F":
                $where .= " and a.FORNITORE = 1  ";
                break;
            case "P":
                $where .= " and a.PROFESSIONAL = 1  ";
                break;                
       }
       
       $where .= " and a.FINE >= '".converti_data_per_db($_POST['SCA_INI'])."'";
       $where .= " and a.FINE <= '".converti_data_per_db($_POST['SCA_FIN'])."'";
       
       
       //termina(array("COD"=>"ko","MSG"=>$where));   
       
       includi_escludi_secco($campo,$from,$where,$_POST['SCA_IE']);
       
       ok();
   } // form soci in scadenza     


   // form facility
   if ($_POST['formid'] == "facility") {
       $codice_facility = 181; 
       $campo = " a.ID ";
       $from = " persone a ";
       $where = " ( RuoloAziendale1 = $codice_facility or RuoloAziendale2 = $codice_facility) ";
       
       includi_escludi_secco($campo,$from,$where,$_POST['FACILITY_IE']);
       
       ok();
   } // facility   
   
   // form potenzialita
   if ($_POST['formid'] == "potenzialita") {
 
       $campo = " a.IDPERSONA ";
       $from = " potenzialita_persona a ";
       
       $where_e = " IDPOTENZIALITA in ( ## )";
       $where_i = " IDPOTENZIALITA in ( ## )";
       $trovato_i= array();
       $trovato_e= array();
       
       foreach ($_POST["POTENZIALITA"] as $k => $v ) {
           if ($v == "E") $trovato_e[] =$k;
           if ($v == "I") $trovato_i[] =$k; 
       }
       
       if (count($trovato_e) > 0 ) {
           $where_e = str_replace("##", implode(",", $trovato_e),$where_e);
           includi_escludi_secco($campo,$from,$where_e,"E");
       }

       if (count($trovato_i) > 0 ) {
           $where_i = str_replace("##", implode(",", $trovato_i),$where_i);
           includi_escludi_secco($campo,$from,$where_i,"I");
       }
       ok();
   } // potenzialita   
   
   // form azioni
   if ($_POST['formid'] == "azioni") {

       if (db_is_null($_POST['AZIONEID'])) {
          termina(array("COD"=>"ko","MSG"=>"Valorizzare l'azione di riferimento"));
       }
       
       if ($_POST['AZIONI_IE'] == "I") {
       $campo = " a.IDPERSONA ";
       $from = " persone_azione a ";
       $where = " a.IDAZIONE = ".$_POST['AZIONEID'];
       includi_escludi_secco($campo,$from,$where,$_POST['AZIONI_IE']);
       } else {
         // essendo sulla stessa tavola faceva casino...
         
           $sql = "DELETE a1 FROM persone_azione AS a1 INNER JOIN persone_azione AS a2
           WHERE a1.IDPERSONA=a2.IDPERSONA
             and a2.IDAZIONE = ".$_POST['AZIONEID']."
             and a1.IDAZIONE = $az";
           $ris = mysql_query($sql);
           if (! $ris ) termina(array("COD"=>"ko","MSG"=>mysql_error()));
           ok();             
       }
       

       ok();
   } // azioni       
        
   
   // form aziende socie 
   if ($_POST['formid'] == "azsoci") {
   
       if (!isset($_POST['SOCI']) && !isset($_POST['SOCIS']) &&  !isset($_POST['SOCIF'])  &&  !isset($_POST['SOCIP']) ) {
          termina(array("COD"=>"ko","MSG"=>"Effetuare almeno una selezione"));
       }
       
       $campo = " c.ID ";
       $from = " persone c ";
       $where = " c.AziendaID in ( select b.IDAZIENDA from soci_azienda b
                                    where b.FINE >= '".date("Y-m-d")."'";

       if (isset($_POST['SOCIS'])) $where .=  " AND b.SPONSOR = 1 ";
       if (isset($_POST['SOCIF'])) $where .=  " AND b.FORNITORE = 1 ";
       if (isset($_POST['SOCIP'])) $where .=  " AND b.PROFESSIONAL = 1 ";
       
       $where .= " )";
       
       includi_escludi_secco($campo,$from,$where,$_POST['AZSOCI_IE']);
       
       ok();
   }   // form aziende socie 
   
   // form EMAIL AZIENDALE 
   if ($_POST['formid'] == "emailaz") {

       $campo = " c.ID ";
       $from = " persone c ";

       $where = "c.EmailAziendale is not null";
       if ($_POST['EMAILAZ_CS'] == "S") $where = "c.EmailAziendale is null";

       includi_escludi_secco($campo,$from,$where,$_POST['EMAILAZ_IE']);
       
       ok();
   }   // form EMAIL AZIENDALE 
   
   
   // form consenso trattamento dati 
   if ($_POST['formid'] == "consenso") {
       $campo = " c.ID ";
       $from = " persone c ";
       $valore = 1;
       if ($_POST['CONSENSO_HN'] == "N") {
          $valore = 0;
       }
       if ($_POST['CONSENSO_TIPO']== "C") {
          $where = " Consenso675 = $valore";
       }
       if ($_POST['CONSENSO_TIPO']== "E") {
          $where = " Consenso675esteso = $valore";
       }
       if ($_POST['CONSENSO_TIPO']== "T") {
          $where = " Consenso675 = $valore and Consenso675esteso = $valore";
       }
       includi_escludi_secco($campo,$from,$where,$_POST['CONSENSO_IE']);
       ok();
   }   // form consenso trattamento dati       
   
   
   // form filtri
   if ($_POST['formid'] == "filtri") {
       $campo = " c.ID ";
       $from = " persone c, aziende d ";
       $where = " c.AziendaID = d.ID ";
       
       if ($_POST['FUNZ'] == "S") {
           $elenco_raa = array_filter($_POST['RUOLOAZIENDALEAREA'],'is_not_null');  
           if (count($elenco_raa)>0 ) {
               $elenco_raain = implode(",",$elenco_raa);
               //$elenco_raa = str_replace(",,",",",$elenco_raa);
               $where .= " and ( c.RuoloAziendale1 in (select ra.ID from ruolo_aziendale ra where ra.IDAREA in ($elenco_raain) ) )";
           }
       }

       if ($_POST['RUOLO'] == "S") {
           $elenco_ra = array_filter($_POST['RUOLOAZIENDALE'],'is_not_null');  
           if (count($elenco_ra)>0 ) {
               $elenco_rain = implode(",",$elenco_ra);
               //$elenco_raa = str_replace(",,",",",$elenco_raa);
               $where .= " and ( c.RuoloAziendale1 in  ($elenco_rain) ) ";
           }
       } 
       
       if ($_POST['SETT'] == "S") {
           $elenco_sett = array_filter($_POST['SETTORE'],'is_not_null'); 
          
           if (count($elenco_sett)>0 ) {
               $elenco_settin = implode(",",$elenco_sett);
               //$elenco_raa = str_replace(",,",",",$elenco_raa);
               $where .= " and ( d.Settore in  ($elenco_settin) ) ";
           }
       }
       
       if ($_POST['ATT'] == "S") {
           $elenco_aa = array_filter($_POST['ATTIVITA_AZIENDE'],'is_not_null');  
           if (count($elenco_aa)>0 ) {
               $elenco_aain = implode(",",$elenco_aa);
               //$elenco_raa = str_replace(",,",",",$elenco_raa);
               
               $where .= " and ( d.Attivita in (select aa.IDAZIENDA from attivita_per_aziende aa where aa.IDAZIENDA = d.ID and aa.IDATTIVITA in ($elenco_aain) ) )";
           }
       }                     

       if ($_POST['PROV'] == "S") {
           $elenco_pr = array_filter($_POST['PROVINCE'],'is_not_null');  
           if (count($elenco_pr)>0 ) {
               //$elenco_raa = str_replace(",,",",",$elenco_raa);
               $where .= " and ( c.Provincia in ('" . implode("', '", $elenco_pr) . "') )";
           }
       }  
       
       $aree = $_POST["AREA"];
       if (  (!isset($aree[1]) || $aree[1] != 1) || 
             (!isset($aree[2]) || $aree[2] != 1) ||
             (!isset($aree[3]) || $aree[3] != 1) ||
             (!isset($aree[4]) || $aree[4] != 1)
          ) {
             $elenco_aree = " 55 ";
             if (isset($aree[1])) $elenco_aree .= ", 1";
             if (isset($aree[2])) $elenco_aree .= ", 2";
             if (isset($aree[3])) $elenco_aree .= ", 3";
             if (isset($aree[4])) $elenco_aree .= ", 4";
              $elenco_aree .= " ";
            
             $where .= " and ( c.Provincia in (select pr.SIGLA from regioni re,province pr where pr.REGIONE = re.Regione_Nome and re.Area_ID in ($elenco_aree) ) )";
       }         
       

       if ($_POST['FATT'] == "S") {
           $elenco_fa = array_filter($_POST['FATTURATO'],'is_not_null');  
           if (count($elenco_fa)>0 ) {
               $elenco_fain = implode(",",$elenco_fa);
               //$elenco_raa = str_replace(",,",",",$elenco_raa);
               $where .= " and ( d.Fatturato in  ($elenco_fain) ) ";
           }
       }   

       if ($_POST['DIP'] == "S") {
           $elenco_da = array_filter($_POST['DIPENDENTI'],'is_not_null');  
           if (count($elenco_da)>0 ) {
               $elenco_dain = implode(",",$elenco_da);
               //$elenco_raa = str_replace(",,",",",$elenco_raa);
               $where .= " and ( d.Dipendenti in  ($elenco_dain) ) ";
           }
       }                    
       
       includi_escludi_secco($campo,$from,$where,$_POST['FILTRI_IE']);
       ok();
   }   // form filtri          
   
   
   
   
   function includi_escludi_secco($campo,$from,$where,$tipo) {
      global $az;
      
      if ($tipo == "E")  {
           $sql = "delete from `persone_azione` where IDAZIONE = $az and IDPERSONA IN ( select $campo from $from where $where )";
      }  else {
           $sql = "INSERT INTO `persone_azione`(`IDPERSONA`, `IDAZIONE`) select $campo, $az from $from where $where ON DUPLICATE KEY UPDATE FLAG = 0; ";
      }
   //    termina(array("COD"=>"ko","MSG"=>$sql));
      $ris = mysql_query($sql);
      if (! $ris ) 
         termina(array("COD"=>"ko","MSG"=>mysql_error().$sql));      
   
   }
   
   // funzioni generali 
   function termina($cosa) {
      ob_end_clean();
      die(json_encode($cosa));
      exit;
   }
   
   function ok() {
      termina(array("COD"=>"ok","MSG"=>numero()));
   }
   
   function numero() {
      global $az;
      return db_query_count("persone_azione","IDAZIONE =".$az);
   }
   
    function is_not_null($val){
        return !db_is_null($val);
    }   
       


?>