<?php
$m="azioni";

require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/files.php';
require '../Librerie/configurazione.php';     

$Titolo = "Gestione Azioni";
$Tavola= "azioni";


/*
if (!isset($_POST['azione'])) {
   if (isset($_GET['azione'])) {
       $_POST['azione'] = $_GET['azione'];
   } else if (isset($_SESSION["vis_azione"])) {
     $_POST['azione'] = $_SESSION["vis_azione"];
   }
    else
   $_POST['azione'] = "";
} else {
  $_SESSION['vis_azione'] = $_POST['azione'];
}

*/
if (!isset($_GET['id'])) {
       header("Location: vis_azioni.php");
       exit;    
}

$where = "1 = 2";
if (!db_is_null($_GET['id']) ) {
    $where = "  b.IDAZIONE = ".$_GET['id'];
}


$sql = " select a.Cognome, a.Nome,a.AziendaID,b.IDTIPORISULTATO,b.SELEZIONE,b.PAGANTE,B.RELATORE 
           FROM persone a, persone_azione b
          WHERE a.ID = b.IDPERSONA
            and ".$where."
           order by a.Cognome, a.Nome";


$risultato = mysql_query($sql);

$rise = db_query_vis("tipo_risultato",'ID');
$e=array();
while ($tipi=mysql_fetch_assoc($rise)) {
    $e[$tipi['ID']] = $tipi['DESCRIZIONE']; 
}
                            
           


require '../Librerie/ges_html_top.php';
?>


<script>
	

  function conferma(a) {
     if (a.value != null && a.value.length != 0 ) {
        a.form.submit();
     }
  }

 </script>
 
          


      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $Titolo;?> </h2>
            <ul class="nav navbar-right panel_toolbox">
                  <button class="btn btn-round btn-primary" type="button" onclick="location.href='vis_azioni.php'">Indietro</button>
                 <button class="btn btn-round btn-primary" type="button" onclick="location.href='esporta_azioni.php?id=<?php echo $_GET['id'];?>'">Esporta</button>
            </ul>
            <div class="clearfix"></div>
          </div>
          
          

           <div class="x_content">
                <div class="clearfix"></div>
              </div>           
          


          <div class="x_content">
          <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">

           <thead>
            <tr>
            <th width="30%"> Nome </th>
            <th width="30%"> Azienda </th>
            <th width="10%"> Risultato </th>
            <th width="10%"> Pagante </th>
            <th width="10%"> Relat. </th>
            <th width="10%"> Selez. </th>
          </thead>  
          <tbody> 
            <?php
                 while ($cur_rec = mysql_fetch_assoc($risultato))

            {
                 echo "<tr >	";
                   $esito = "";
                   if (!db_is_null($cur_rec['IDTIPORISULTATO'])) {
                        $esito = $e[$cur_rec['IDTIPORISULTATO']];
                   }
                   
                    $a = "";
                   if (!db_is_null($cur_rec['AziendaID'])) {
                       $risa = db_query_mod("aziende",$cur_rec['AziendaID']);
                       $az=mysql_fetch_assoc($risa);
                       $a = $az['RagioneSociale'];
                   }                       
                   
                  $p = "";
                  if ($cur_rec['PAGANTE'] == 1 ) $p ='<i class="fa fa-check text-success"></i>';                   
                  $r = "";
                  if ($cur_rec['RELATORE'] == 1 ) $r ='<i class="fa fa-check text-success"></i>';                   
                  $s = "";
                  if ($cur_rec['SELEZIONE'] == 1 ) $s ='<i class="fa fa-check text-success"></i>';                   
                  
                   
                   
                   echo " <td >".$cur_rec['Cognome']." ".$cur_rec['Nome']."   </td>
                          <td >".$a."   </td>
                          <td >".$esito."   </td>
                          <td >".$p."   </td>
                          <td >".$r."   </td>
                          <td >".$s."   </td>
                         </tr> ";
                 }
            ?>
              </tbody>
        </table>

        </div>
      </div>
    </div>



     <script>
      $(document).ready(function() {
        $('#datatable-responsive').DataTable({
                  "bFilter":true,
                  "iDisplayLength": 50,
                 "aaSorting": [[ 0, "asc" ]], 
                         "bStateSave":true,                 
                  "aoColumns": [
                              null,
                              null,
                               { "bSortable": false },
                               { "bSortable": false },
                               { "bSortable": false },
                               { "bSortable": false }                               
                             ]         
        });
      });
    </script>  





<?php require '../Librerie/ges_html_bot.php'; ?>
