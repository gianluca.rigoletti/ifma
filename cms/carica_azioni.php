<?php
$m="azioni";

require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/files.php';
require '../Librerie/configurazione.php';

if (!isset($_GET['id'])) {

       header("Location: vis_azioni.php");
       exit;    
}
   

$Titolo = "Gestione Azioni";
$Tavola= "azioni";

   $risultato = db_query_mod($Tavola,$_GET['id']);
   $cur_rec = mysql_fetch_assoc($risultato);
   $cur_rec['DATA_VIS'] = db_converti_data($cur_rec['DATA']); 

   $numero = db_query_count("persone_azione","IDAZIONE =".$_GET['id']);

require '../Librerie/ges_html_top.php';
?>
          <input type="hidden" id="azione" value="<?php echo $_GET['id'];?>" />
                              
         <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $cur_rec['Nome']." - ".$cur_rec['DATA_VIS'];?></h2>
                  <ul class="nav navbar-right panel_toolbox">
                        <button class="btn btn-round btn-primary" type="button" onclick="location.href='esportazioni.php?tipo=a&id=<?php echo $_GET['id'];?>'">Esporta</button>
                        <button class="btn btn-round btn-primary" type="button" onclick="location.href='vis_azioni.php'">Indietro</button>
                  </ul>                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     <div>
                      <ul class="nav navbar-left panel_toolbox">
                      <button id="tutti" class="btn btn-success btn-lg" type="button">Inserisci Tutti</button>
                       <button id="nessuno" class="btn btn-danger  btn-lg" type="button">Cancella Tutti</button>
                        </ul>
                      <div class="clearfix"></div>  
                     </div>   
                     
                     
                     <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
                      <div class="panel" id="panel1">
                        <a class="panel-heading collapsed" role="tab" id="headingOne1" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1" aria-expanded="false" aria-controls="collapseOne">
                          <h4 class="panel-title">Soci</h4>
                        </a>
                        <div id="collapseOne1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                          <div class="panel-body">
                          <form id="soci" name="soci" action="" >
                             <div class="row item form-group">
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <select name="SOCI_IE" class="form-control">
                                       <option value="I" selected>Includere</option>
                                       <option value="E">Escludere</option>
                                    </select>
                                </div>  
                              <div class="col-md-2 col-sm-2 col-xs-12">
                                  <select required="true" name="SOCI_TIPI" class="form-control">
                                     <option value="" selected>Tipologia Socio</option>
                                     <option value="T" >Tutti</option>
                                     <option value="S" >Semplici</option>
                                     <option value="SP" >Sponsor</option>
                                     <option value="F" >Fornitori</option>
                                     <option value="P" >Professional</option>
                                  </select>
                              </div>   

                              <div class="checkbox col-md-6 col-xs-12" >
                                <label>
                                 <input type="checkbox"  class="flat" name="SOCI_SCADUTI" value="1" />
                                  Soci Scaduti negli ultimi 2 mese
                              </label>                                                            
                              </div>                  
                           </div>
                          
                             <div class="row">
                                <div class="col-md-2 col-sm-2 col-xs-3">
                                <button class="btn btn-success  conferma col-xs-12" type="button">OK</button>
                               </div>
                               <div class="col-md-2 col-sm-2 col-xs-3">
                                 <button class="btn btn-danger   cancella col-xs-12"  type="button">Cancella</button>
                              </div>
                            </div>
                                                      
                           </form>  
                          </div>
                        </div>
                      </div>  <!-- chiuso panel1 -->
                      

                      <div class="panel" id="panel4">
                        <a class="panel-heading collapsed" role="tab" id="headingFour1" data-toggle="collapse" data-parent="#accordion1" href="#collapseFour1" aria-expanded="false" aria-controls="collapseFour">
                          <h4 class="panel-title">Soci in Scadenza</h4>
                        </a>
                        <div id="collapseFour1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour" aria-expanded="false" style="height: 0px;">
                          <div class="panel-body">
                          <form id="sociScandeza" name="sociScandeza" action="" >
                            <div class="row item form-group">
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <select name="SCA_IE" class="form-control">
                                       <option value="I" selected>Includere</option>
                                       <option value="E">Escludere</option>
                                    </select>
                                </div>  
                              <div class="col-md-2 col-sm-2 col-xs-12">
                                  <select name="SOCI_TIPI" class="form-control">
                                     <option value="" selected>Tipologia Socio</option>
                                     <option value="T" >Tutti</option>
                                     <option value="S" >Semplici</option>
                                     <option value="SP" >Sponsor</option>
                                     <option value="F" >Fornitori</option>
                                     <option value="P" >Professional</option>
                                  </select>
                              </div>   

                          <label class="control-label col-md-2 col-sm-2 col-xs-12" for="">Periodo
                          </label>                 

                          <?php datepicker('SCA_INI'); datepicker('SCA_FIN'); ?>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <input type="text"  required="true" class="form-control col-xs-6" name="SCA_INI"  id="SCA_INI" value="<?php echo date("d/m/Y");?>"  size="10" maxlength="100">
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                 <input type="text"  required="true" class="form-control col-xs-6"  name="SCA_FIN"  id="SCA_FIN"  value="<?php echo date("d/m/Y");?>"  size="10" maxlength="100">
                          </div>

                           </div>
                           
                             <div class="row">
                                <div class="col-md-2 col-sm-2 col-xs-3">
                                <button class="btn btn-success  conferma col-xs-12" type="button">OK</button>
                               </div>
                               <div class="col-md-2 col-sm-2 col-xs-3">
                                 <button class="btn btn-danger   cancella col-xs-12"  type="button">Cancella</button>
                              </div>
                            </div>
                          
                           </form>  
                          </div>
                        </div>
                      </div>  <!-- chiuso panel4 -->                      
                      
                    
                      
                      
                      <div class="panel"  id="panel2">
                        <a class="panel-heading collapsed" role="tab" id="headingTwo1" data-toggle="collapse" data-parent="#accordion1" href="#collapseTwo1" aria-expanded="false" aria-controls="collapseTwo">
                          <h4 class="panel-title">Facility Manager</h4>
                        </a>
                        <div id="collapseTwo1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">
                          <div class="panel-body">
                          <form id="facility" name="facility" action="" >
                             <div class="row item form-group">
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <select name="FACILITY_IE" class="form-control">
                                       <option value="I" selected>Includere</option>
                                       <option value="E">Escludere</option>
                                    </select>
                                </div>  
                               <label class="control-label col-md-2 col-sm-2 col-xs-12" for="">Tutti i Facility Manager
                              </label>        
                             </div>
                             <div class="row">
                                <div class="col-md-2 col-sm-2 col-xs-3">
                                <button class="btn btn-success  conferma col-xs-12" type="button">OK</button>
                               </div>
                               <div class="col-md-2 col-sm-2 col-xs-3">
                                 <button class="btn btn-danger   cancella col-xs-12"  type="button">Cancella</button>
                              </div>
                            </div>
                                                      
                           </form>                            
                          </div>
                        </div>
                      </div> <!-- chiuso panel2 -->
                      
                      
                      <div class="panel" id="panel3">
                        <a class="panel-heading collapsed" role="tab" id="headingThree1" data-toggle="collapse" data-parent="#accordion1" href="#collapseThree1" aria-expanded="false" aria-controls="collapseThree">
                          <h4 class="panel-title">Potenzialit&agrave;</h4>
                        </a>
                        <div id="collapseThree1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false" style="height: 0px;">
                          <div class="panel-body">
                           <form id="potenzialita" name="potenzialita" action="">
                               <?php
                                  $po = db_query_vis("potenzialita","DESCRIZIONE");
                                  while ($p = mysql_fetch_assoc($po)) {
                                    echo "
                                 
                                     <div class=\"row \">
                                      <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">  ".$p['DESCRIZIONE']."
                                      </label>   
                                        <div class=\"col-md-2 col-sm-2 col-xs-12\">
                                            <select name=\"POTENZIALITA[".$p['ID']."]\" class=\"form-control\">
                                               <option value=\"\" selected>Ignora</option>
                                               <option value=\"I\">Includere</option>
                                               <option value=\"E\">Escludere</option>
                                            </select>
                                        </div>  
                                      </div>   
                                      ";                                        
                                  }
                                   
                               ?> <br />
                               <div class="row item form-group">
                                  <div class="col-md-2 col-sm-2 col-xs-3">
                                  <button class="btn btn-success  conferma col-xs-12" type="button">OK</button>
                                 </div>
                                 <div class="col-md-2 col-sm-2 col-xs-3">
                                   <button class="btn btn-danger   cancella col-xs-12"  type="button">Cancella</button>
                                </div>
                              </div> 
                            </form>                              
                          </div>
                        </div>
                      </div> <!-- chiuso panel3 -->
                      

                      <div class="panel" id="panel5">
                        <a class="panel-heading collapsed" role="tab" id="headingFive1" data-toggle="collapse" data-parent="#accordion1" href="#collapseFive1" aria-expanded="false" aria-controls="collapseFive">
                          <h4 class="panel-title">Partecipanti ad Azioni</h4>
                        </a>
                        <div id="collapseFive1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive" aria-expanded="false" style="height: 0px;">
                          <div class="panel-body">
                           <form id="azioni" name="azioni" action="">
                               <div class="row item form-group">
                                  <div class="col-md-2 col-sm-2 col-xs-12">
                                    <select name="AZIONI_IE" class="form-control">
                                       <option value="I" selected>Includere</option>
                                       <option value="E">Escludere</option>
                                    </select>                                  
                                  </div>
                                  <label class="control-label col-md-2 col-sm-2 col-xs-12" for=""> I partecipanti a
                                 </label> 
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                     <select name="AZIONEID" class="form-control">
                               <?php
                                     db_html_select_cod('azioni', '','ID','Nome',true,null);
                               ?>
                                    </select>
                                  </div>
                               </div>
                               <div class="row">
                                  <div class="col-md-2 col-sm-2 col-xs-3">
                                  <button class="btn btn-success  conferma col-xs-12" type="button">OK</button>
                                 </div>
                                 <div class="col-md-2 col-sm-2 col-xs-3">
                                   <button class="btn btn-danger   cancella col-xs-12"  type="button">Cancella</button>
                                </div>
                              </div> 
                            </form>                              
                          </div>
                        </div>
                      </div> <!-- chiuso panel5 -->
                      
                      <div class="panel" id="panel6">
                        <a class="panel-heading collapsed" role="tab" id="headingSix1" data-toggle="collapse" data-parent="#accordion1" href="#collapseSix1" aria-expanded="false" aria-controls="collapseSix">
                          <h4 class="panel-title">Persone Appartenenti ad aziende Socie</h4>
                        </a>
                        <div id="collapseSix1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix" aria-expanded="false" style="height: 0px;">
                          <div class="panel-body">
                          <form id="azsoci" name="azsoci" action="" >
                             <div class="row item form-group">
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <select name="AZSOCI_IE" class="form-control">
                                       <option value="I" selected>Includere</option>
                                       <option value="E">Escludere</option>
                                    </select>
                                </div>  


                                    <div class="checkbox col-md-8 col-xs-12" >
                                      <label>
                                       <input type="checkbox" checked="checked" class="flat" name="SOCI" value="1" />
                                        Soci
                                    </label>
                                      <label>
                                       <input type="checkbox" checked="checked" class="flat" name="SOCIS" value="1" />
                                        Sponsor
                                    </label>  
                                    <label>
                                       <input type="checkbox" checked="checked" class="flat" name="SOCIF" value="1" />
                                        Fornitori
                                    </label>   
                                      <label>
                                       <input type="checkbox" checked="checked" class="flat" name="SOCIP" value="1" />
                                        Professional
                                    </label>                                                                                                        
                                    </div>   
                                                 
                                 
                             </div>
                             <div class="row">
                                <div class="col-md-2 col-sm-2 col-xs-3">
                                <button class="btn btn-success  conferma col-xs-12" type="button">OK</button>
                               </div>
                               <div class="col-md-2 col-sm-2 col-xs-3">
                                 <button class="btn btn-danger   cancella col-xs-12"  type="button">Cancella</button>
                              </div>
                            </div>
                                                      
                           </form>  
                          </div>
                        </div>
                      </div>  <!-- chiuso panel6 -->
                                            

                      <div class="panel" id="panel7">
                        <a class="panel-heading collapsed" role="tab" id="headingSeven1" data-toggle="collapse" data-parent="#accordion1" href="#collapseSeven1" aria-expanded="false" aria-controls="collapseSeven">
                          <h4 class="panel-title">Persone con/senza email aziendale</h4>
                        </a>
                        <div id="collapseSeven1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven" aria-expanded="false" style="height: 0px;">
                          <div class="panel-body">
                          <form id="emailaz" name="emailaz" action="" >
                             <div class="row item form-group">
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <select name="EMAILAZ_IE" class="form-control">
                                       <option value="I" selected>Includere</option>
                                       <option value="E">Escludere</option>
                                    </select>
                                </div>  

                                  
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <select name="EMAILAZ_CS" class="form-control">
                                       <option value="C" selected>Con Email Aziendale</option>
                                       <option value="S">Senza Email Aziendale</option>
                                    </select>
                                </div>                                                   
                                 
                             </div>
                             <div class="row">
                                <div class="col-md-2 col-sm-2 col-xs-3">
                                <button class="btn btn-success  conferma col-xs-12" type="button">OK</button>
                               </div>
                               <div class="col-md-2 col-sm-2 col-xs-3">
                                 <button class="btn btn-danger   cancella col-xs-12"  type="button">Cancella</button>
                              </div>
                            </div>
                                                      
                           </form>  
                          </div>
                        </div>
                      </div>  <!-- chiuso panel7 -->

                      <div class="panel" id="panel8">
                        <a class="panel-heading collapsed" role="tab" id="headingEight1" data-toggle="collapse" data-parent="#accordion1" href="#collapseEight1" aria-expanded="false" aria-controls="collapseEight">
                          <h4 class="panel-title">Trattamento dei Dati</h4>
                        </a>
                        <div id="collapseEight1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight" aria-expanded="false" style="height: 0px;">
                          <div class="panel-body">
                          <form id="consenso" name="consenso" action="" >
                             <div class="row item form-group">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <select name="CONSENSO_IE" class="form-control">
                                       <option value="I" selected>Includere Persone</option>
                                       <option value="E">Escludere Persone</option>
                                    </select>
                                </div>  

                                  
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <select name="CONSENSO_HN" class="form-control">
                                       <option value="H" selected>Hanno</option>
                                       <option value="N">Non Hanno</option>
                                    </select>
                                </div> 

                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <select name="CONSENSO_TIPO" class="form-control">
                                       <option value="C" selected>Consenso</option>
                                       <option value="E">Consenso Esteso</option>
                                       <option value="T">Entrambi</option>
                                    </select>
                                </div>                                                                                    
                                 
                             </div>
                             <div class="row">
                                <div class="col-md-2 col-sm-2 col-xs-3">
                                <button class="btn btn-success  conferma col-xs-12" type="button">OK</button>
                               </div>
                               <div class="col-md-2 col-sm-2 col-xs-3">
                                 <button class="btn btn-danger   cancella col-xs-12"  type="button">Cancella</button>
                              </div>
                            </div>
                                                      
                           </form>  
                          </div>
                        </div>
                      </div>  <!-- chiuso panel8 --> 
                      
                      <div class="panel" id="panel9">
                        <a class="panel-heading collapsed" role="tab" id="headingNine1" data-toggle="collapse" data-parent="#accordion1" href="#collapseNine1" aria-expanded="false" aria-controls="collapseNine">
                          <h4 class="panel-title">Area-Ruolo/Funzione Aziendale/Macro Settore/Attivit&agrave;/Provincia/Area Geografica/Fatturato/Dipendenti</h4>
                        </a>
                        <div id="collapseNine1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine" aria-expanded="false" style="height: 0px;">
                          <div class="panel-body">
                          <form id="filtri" name="filtri" action="" >
                             <div class="row item form-group">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <select name="FILTRI_IE" class="form-control">
                                       <option value="I" selected>Includere </option>
                                       <option value="E">Escludere </option>
                                    </select>
                                </div>  

                                <label class="control-label col-md-6 col-sm-6 col-xs-12" for=""> Persone con i seguenti Requisiti
                                </label> 
                             </div>   
                                
                                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                      <li role="presentation" class="active"><a href="#tab_content1" id="profile-tab1" role="tab" data-toggle="tab" aria-expanded="true">Area - Ruolo</a>
                                      </li>
                                      <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Funzione Aziendale</a>
                                      </li>
                                      <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab3" data-toggle="tab" aria-expanded="false">Macro Settore</a>
                                      </li>
                                      <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab4" data-toggle="tab" aria-expanded="false">Attivit&agrave;</a>
                                      </li>
                                      <li role="presentation" class=""><a href="#tab_content5" role="tab" id="profile-tab5" data-toggle="tab" aria-expanded="false">Provincia</a>
                                      </li>
                                      <li role="presentation" class=""><a href="#tab_content6" role="tab" id="profile-tab6" data-toggle="tab" aria-expanded="false">Area Geografica</a>
                                      </li>
                                      <li role="presentation" class=""><a href="#tab_content7" role="tab" id="profile-tab7" data-toggle="tab" aria-expanded="false">Fatturato</a>
                                      </li>
                                      <li role="presentation" class=""><a href="#tab_content8" role="tab" id="profile-tab8" data-toggle="tab" aria-expanded="false">Dipendenti</a>
                                      </li>

                                    </ul>
                                
                                <div id="myTabContent" class="tab-content">
                                      <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="profile-tab1">
                                        <div class="row item form-group">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <select name="FUNZ" class="form-control">
                                                   <option value="T" selected>Tutti </option>
                                                   <option value="S">Solo Selezionati </option>
                                                </select>
                                            </div>   
                                         </div>  
                                         <div class="row item form-group">
                                         <div class="col-md-6 col-sm-6 col-xs-12 ruoloaziendalearea"> 
                                         <select name="RUOLOAZIENDALEAREA[]" class="form-control">
                                         <?php db_html_select_cod('ruolo_aziendale_area', null,'ID','DESCRIZIONE',true,null); ?>
                                         </select>
                                         </div>
                                         </div> 
                                         <div  class="row item form-group">
                                            <div class="col-md-1 col-sm-1 col-xs-1">
                                             <button class="btn btn-round btn-primary" type="button" onclick="javascript:add('ruoloaziendalearea');">+</button>
                                            </div>
                                         </div>  
                                      
                                      </div>   <!-- chiuso tab 1 -->
                                      <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab2">

                                      <div class="row item form-group">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <select name="RUOLO" class="form-control">
                                                   <option value="T" selected>Tutti </option>
                                                   <option value="S">Solo Selezionati </option>
                                                </select>
                                            </div>   
                                         </div>  
                                         <div class="row item form-group">
                                         <div class="col-md-6 col-sm-6 col-xs-12 ruoloaziendale"> 
                                         <select name="RUOLOAZIENDALE[]" class="form-control">
                                         <?php db_html_select_cod('ruolo_aziendale', null,'ID','DESCRIZIONE',true,null); ?>
                                         </select>
                                         </div>
                                         </div> 
                                         <div  class="row item form-group">
                                            <div class="col-md-1 col-sm-1 col-xs-1">
                                             <button class="btn btn-round btn-primary" type="button" onclick="javascript:add('ruoloaziendale');">+</button>
                                            </div>
                                         </div>  
                                      
                                      </div>   <!-- chiuso tab 2 -->
                                      <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab3">
                                        <div class="row item form-group">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <select name="SETT" class="form-control">
                                                   <option value="T" selected>Tutti </option>
                                                   <option value="S">Solo Selezionati </option>
                                                </select>
                                            </div>   
                                         </div>  
                                         <div class="row item form-group">
                                         <div class="col-md-6 col-sm-6 col-xs-12 settori"> 
                                         <select name="SETTORE[]" class="form-control">
                                         <?php db_html_select_cod('settori', null,'ID','DESCRIZIONE',true,null); ?>
                                         </select>
                                         </div>
                                         </div> 
                                         <div  class="row item form-group">
                                            <div class="col-md-1 col-sm-1 col-xs-1">
                                             <button class="btn btn-round btn-primary" type="button" onclick="javascript:add('settori');">+</button>
                                            </div>
                                         </div>  

                                      
                                      </div>    <!-- chiuso tab 3 -->
                                      <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab4">
                                        <div class="row item form-group">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <select name="ATT" class="form-control">
                                                   <option value="T" selected>Tutti </option>
                                                   <option value="S">Solo Selezionati </option>
                                                </select>
                                            </div>   
                                         </div>  
                                         <div class="row item form-group">
                                         <div class="col-md-6 col-sm-6 col-xs-12 attivita_aziende"> 
                                         <select name="ATTIVITA_AZIENDE[]" class="form-control">
                                         <?php db_html_select_cod('attivita_aziende', null,'ID','DESCRIZIONE',true,null); ?>
                                         </select>
                                         </div>
                                         </div> 
                                         <div  class="row item form-group">
                                            <div class="col-md-1 col-sm-1 col-xs-1">
                                             <button class="btn btn-round btn-primary" type="button" onclick="javascript:add('attivita_aziende');">+</button>
                                            </div>
                                         </div>                                        
                                      
                                      </div>    <!-- chiuso tab 4 -->
                                      <div role="tabpanel" class="tab-pane fade" id="tab_content5" aria-labelledby="profile-tab5">
                                        <div class="row item form-group">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <select name="PROV" class="form-control">
                                                   <option value="T" selected>Tutti </option>
                                                   <option value="S">Solo Selezionati </option>
                                                </select>
                                            </div>   
                                         </div>  
                                         <div class="row item form-group">
                                         <div class="col-md-6 col-sm-6 col-xs-12 province"> 
                                         <select name="PROVINCE[]" class="form-control">
                                         <?php db_html_select_cod('province', null,'SIGLA','DESCRIZIONE',true,null); ?>
                                         </select>
                                         </div>
                                         </div> 
                                         <div  class="row item form-group">
                                            <div class="col-md-1 col-sm-1 col-xs-1">
                                             <button class="btn btn-round btn-primary" type="button" onclick="javascript:add('province');">+</button>
                                            </div>
                                         </div>                                         
                                      </div>    <!-- chiuso tab 5 --> 
                                      <div role="tabpanel" class="tab-pane fade" id="tab_content6" aria-labelledby="profile-tab6">
                                      
                                         <?php
                                            $AR = db_query_vis("aree","ID");
                                            while ($A = mysql_fetch_assoc($AR)) {
                                              echo "
                                           
                                               <div class=\" row \">
                                                <label class=\"col-md-offset-2 col-md-6\">
                                                 <input type=\"checkbox\" checked=\"checked\" class=\"flat\" name=\"AREA[".$A['ID']."]\" value=\"1\" />
                                                    ".$A['NOME']."
                                                  </label> 
                                                </div>   
                                                ";                                        
                                            }
                                             
                                         ?>                                       
                                      
                                      
                                      </div>    <!-- chiuso tab 6 -->
                                      <div role="tabpanel" class="tab-pane fade" id="tab_content7" aria-labelledby="profile-tab7">
                                        <div class="row item form-group">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <select name="FATT" class="form-control">
                                                   <option value="T" selected>Tutti </option>
                                                   <option value="S">Solo Selezionati </option>
                                                </select>
                                            </div>   
                                         </div>  
                                         <div class="row item form-group">
                                         <div class="col-md-6 col-sm-6 col-xs-12 fatturato"> 
                                         <select name="FATTURATO[]" class="form-control">
                                         <?php db_html_select_cod('fatturato', null,'ID','DESCRIZIONE',true,null); ?>
                                         </select>
                                         </div>
                                         </div> 
                                         <div  class="row item form-group">
                                            <div class="col-md-1 col-sm-1 col-xs-1">
                                             <button class="btn btn-round btn-primary" type="button" onclick="javascript:add('fatturato');">+</button>
                                            </div>
                                         </div>                                      
                                      </div>    <!-- chiuso tab 7 -->
                                      <div role="tabpanel" class="tab-pane fade" id="tab_content8" aria-labelledby="profile-tab8">
                                        <div class="row item form-group">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <select name="DIP" class="form-control">
                                                   <option value="T" selected>Tutti </option>
                                                   <option value="S">Solo Selezionati </option>
                                                </select>
                                            </div>   
                                         </div>  
                                         <div class="row item form-group">
                                         <div class="col-md-6 col-sm-6 col-xs-12 dipendenti"> 
                                         <select name="DIPENDENTI[]" class="form-control">
                                         <?php db_html_select_cod('dipendenti', null,'ID','DESCRIZIONE',true,null); ?>
                                         </select>
                                         </div>
                                         </div> 
                                         <div  class="row item form-group">
                                            <div class="col-md-1 col-sm-1 col-xs-1">
                                             <button class="btn btn-round btn-primary" type="button" onclick="javascript:add('dipendenti');">+</button>
                                            </div>
                                         </div>                                          
                                      </div>    <!-- chiuso tab 8 -->                                                                                                                                                       
                                      
                                </div> <!-- chiuso tab content -->     
                               </div> <!-- chiuso tab panel -->                                
                                                                                                                 
                                 
                             <div class="row">
                                <div class="col-md-2 col-sm-2 col-xs-3">
                                <button class="btn btn-success  conferma col-xs-12" type="button">OK</button>
                               </div>
                               <div class="col-md-2 col-sm-2 col-xs-3">
                                 <button class="btn btn-danger   cancella col-xs-12"  type="button">Cancella</button>
                              </div>
                            </div>
                                                      
                           </form>  
                          </div>
                        </div>
                      </div>  <!-- chiuso panel9 -->                                             
                      
                    </div> <!-- chiuso accordion1 -->
                    
                    
                      
                    </div> <!-- chiuso accordion1 -->
                    


                    </form>
                    

                  </div>  <!-- chiuso xcontent -->

                    
                    <div class="alert alert-info alert-dismissible fade in" role="alert">
                    <strong>Persone incluse in questa azione <strong id="numero"><?php echo $numero;?></strong>
                  </div>                  
                </div> <!-- chiuso xpanel -->

                <div id="test">
                </div>
                
    <!-- PNotify -->
    <link href="../Html/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../Html/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../Html/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">                
    <script src="../Html/vendors/pnotify/dist/pnotify.js"></script>
    <script src="../Html/vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../Html/vendors/pnotify/dist/pnotify.nonblock.js"></script>
<script>
  function add(tipo) {
    var codice = $("."+tipo).html();
    $("."+tipo).parent().append('<div class="col-md-6 col-sm-6 col-xs-12">'+codice+'</div>');
  }
        
  $(".cancella").click(function () {
      
      var form = $(this).closest("form").attr("id");
      pulisci(form);
      
  });
  
    function pulisci(form) {   
      //alert("passa");
      $("#"+form)[0].reset();

      if ($("#"+form+" input.flat")[0]) {
          $(document).ready(function () {
              $('input.flat').iCheck({
                  checkboxClass: 'icheckbox_flat-green',
                  radioClass: 'iradio_flat-green'
              });
          });
      }         
    }
  

    $("#tutti").click(function () { 
      eseguiCarico("formid=tutti",null);
    });     

    $("#nessuno").click(function () { 
      eseguiCarico("formid=nessuno",null);
    });  
    
    $(".conferma").click(function () { 
      var form = $(this).closest("form");
       eseguiCarico(form.serialize()+"&formid="+form.attr("id"),form);
    });   
    

    function eseguiCarico(dati,form) {
      var passaggio = dati+"&idazione="+$("#azione").val();
      $.ajax({
          type: "POST",
          url: "esegui_carico.php",
          data: passaggio,
          success : function(text){
             $("#test").html(text);
             var json = $.parseJSON(text);
             if (json.COD == "ok") {
                 notifica("Operazione Eseguita con Successo","success");
                 settaNumero(json.MSG);
                 
                 if (form != null ) {
                     pulisci(form.attr("id"));
                 }
                     
             }  else {
                 notifica(json.MSG,"error"); 
             } 

          },
          error: function (request, status, error) {
            notifica("Errore tecnico! operazione non eseguita","error"); 
          }          
      });
      
    }   


    function notifica(msg,type) {
    new PNotify({
          title: 'Notifica',
          text: msg,
          type: type,
          styling: 'bootstrap3',
          width: '500px',
          buttons: {
          sticker: false
          }          
      });
    }
    
    function settaNumero(n) {
      $("#numero").html(n);
    }  
                
</script>

<?php require '../Librerie/ges_html_bot.php'; ?>
