<?php
$m="azioni";

require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/files.php';
require '../Librerie/configurazione.php';

$c_files = new files(); 

$Tavola= "azioni";

$indietro = "vis_azioni.php";

if ($_GET['p_upd']==1) {
   $Funzione = "Update";
   $Disabilita_chiave = "disabled";
   $Titolo = "Modifica Azione";
} else {
   $Funzione = "Insert";
   $Disabilita_chiave = "";
   $Titolo = "Nuova Azione";
}

// se richiamato in update allora devo popolare il form
if ($_GET['p_upd']==1) {
   $risultato = db_query_mod($Tavola,$_GET['p_id']);
   $cur_rec = mysql_fetch_assoc($risultato);
   $cur_rec['DATA_VIS'] = db_converti_data($cur_rec['DATA']);   
}

// confermo

if ( isset($_POST['Insert']) || isset($_POST['Update']) ) {


   $cur_rec['ID'] = $_POST['ID'];
   $cur_rec['Nome'] = $_POST['Nome'];
   $cur_rec['DATA_VIS'] = $_POST['DATA_VIS'];





    if (!ControlloData($_POST['DATA_VIS'])) {
       $c_err->add("Campo Data Errato","DATA_VIS");
    } else {
        $_POST['DATA'] = converti_data_per_db($_POST['DATA_VIS']);
    }

   if (!$c_err->is_errore()) {
       if ( isset($_POST['Insert'])) {
	          db_insert($Tavola,$_POST,false);

       }  else {
	          db_update($Tavola,$_GET['p_id'],$_POST);
       }
       header('Location: '.$indietro);
       exit;
   }
}

// torno indietro

if (isset($_POST['Return'])) {
   header("Location: ".$indietro);
   exit;
}

require '../Librerie/ges_html_top.php';

$c_err->mostra();
?>



       <form id="formG" class="form-horizontal form-label-left"  action="" method="post" novalidate>


            <input type="hidden" name="ID" value="<?php if (isset($cur_rec)) echo $cur_rec['ID']; ?>">
          


            
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Nome <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("Nome");?> name="Nome"  id="Nome" value="<?php if (isset($cur_rec)) echo $cur_rec['Nome'];?>" size="55" maxlength="50"><br />
            </div>
            </div>
                        
            <?php datepicker('DATA_VIS'); ?>
            
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Data <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text"  required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("DATA_VIS");?> name="DATA_VIS"  id="DATA_VIS" value="<?php if (isset($cur_rec)) echo $cur_rec['DATA_VIS']; else echo date("d/m/Y"); ?>" size="55" maxlength="100"><br />
            </div>
            </div>                        



            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-md-offset-3">
               <button class="cancel btn btn-primary" type="submit" name="Return" value="Return">Indietro</button>
               <button type="submit"  class="btn btn-success" name="<?php echo $Funzione ?>" value="Salva">Salva</button>
             </div>
        </div>
        </form>



<?php require '../Librerie/ges_html_bot.php';


?>
