<?php
$m="associazioni";

require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/files.php';
require '../Librerie/configurazione.php';

$c_files = new files(); 

$Tavola= "soci_azienda";

$indietro = "vis_associazioni.php";
$Disabilita_chiave = "disabled=\"disabled\"";
$Titolo = "Split Associazione";


// se richiamato in update allora devo popolare il form

   
$id = explode(";;",$_GET['p_id']);

if (count($id) != 3) {
   header("Location: ".$indietro);
   exit;   
}

$where = "IDAZIENDA = ".$id[0]." and IDPERSONA =".$id[1]." and INIZIO = '".$id[2]."'";

$risultato = db_query_generale($Tavola,$where,"IDAZIENDA");
$cur_rec = mysql_fetch_assoc($risultato);

$cur_rec['INIZIO_VIS'] = db_converti_data($cur_rec['INIZIO']);
$cur_rec['FINE_VIS'] = db_converti_data($cur_rec['FINE']);
$cur_rec['DATA_SPLIT'] = date("d/m/Y");  



// confermo

if (isset($_POST['Salva']) ) {


    $cur_rec['DATA_SPLIT'] = $_POST['DATA_SPLIT']; 
    if (!ControlloData($_POST['DATA_SPLIT'])) {
       $c_err->add("Campo Data Split","DATA_SPLIT");
    }                  

   // die($cur_rec['INIZIO_VIS'].$_POST['DATA_SPLIT']);
   if (!ControlloRangeData($cur_rec['INIZIO_VIS'],$_POST['DATA_SPLIT'],false)) {
       $c_err->add("Campo Data Divisione uguale o minore data Inizio","DATA_SPLIT");
   }
   
   if (!ControlloRangeData($_POST['DATA_SPLIT'],$cur_rec['FINE_VIS'],false)) {
       $c_err->add("Campo Data Divisione uguale o maggionre data Fine","DATA_SPLIT");
   }

   if (!$c_err->is_errore()) {
            $save = $cur_rec;
            $agg['FINE'] = converti_data_per_db($_POST['DATA_SPLIT']);
            
            $where = "IDAZIENDA = ".$_POST['IDAZIENDA']." and IDPERSONA =".$_POST['IDPERSONA']." and INIZIO = '".$_POST['INIZIO']."'";
	          db_update_libero($Tavola,$agg,$where);

            $save['IDPERSONA'] = $_POST['NUOVA_PERSONA'];
            $save['INIZIO'] = converti_data_per_db($_POST['DATA_SPLIT']);
      
            db_insert($Tavola,$save,false);

       header('Location: '.$indietro);
       exit;
   }
}

// torno indietro

if (isset($_POST['Return'])) {
   header("Location: ".$indietro);
   exit;
}

require '../Librerie/ges_html_top.php';

$c_err->mostra();
?>



       <form id="formG" class="form-horizontal form-label-left"  action="" method="post" novalidate>

            <?php
               $c1 = "";
               $c2 = "";
               $c3 = ""; 
               if (isset($cur_rec['SPONSOR']) && $cur_rec['SPONSOR'] == 1) $c1= 'checked="checked"';
               if (isset($cur_rec['FORNITORE']) && $cur_rec['FORNITORE'] == 1) $c2= 'checked="checked"';
               if (isset($cur_rec['PROFESSIONAL']) && $cur_rec['PROFESSIONAL'] == 1) $c3= 'checked="checked"';


                  $risA = mysql_fetch_assoc(db_query_fk("aziende",$cur_rec['IDAZIENDA']));
                  $risP = mysql_fetch_assoc(db_query_fk("persone",$cur_rec['IDPERSONA']));
                  ECHO "<input type=\"hidden\" name=\"IDPERSONA\" value=\"".$cur_rec['IDPERSONA']."\">
                  <input type=\"hidden\" name=\"IDAZIENDA\"  value=\"".$cur_rec['IDAZIENDA']."\">
                  <input type=\"hidden\" name=\"INIZIO\"  value=\"".$cur_rec['INIZIO']."\">
                  <div class=\"item form-group\">
                    <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Azienda 
                    </label>
                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                        <input type=\"text\"  required=\"true\" $Disabilita_chiave class=\"form-control col-md-7 col-xs-12\" value=\"".$risA['RagioneSociale']."\" size=\"55\" ><br />
                  </div>
                  </div>
                  <div class=\"item form-group\">
                    <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Persona 
                    </label>
                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                        <input type=\"text\"  required=\"true\" $Disabilita_chiave class=\"form-control col-md-7 col-xs-12\" value=\"".$risP['Cognome']." ".$risP['Nome']."\" size=\"55\" ><br />
                  </div>
                  </div>";                             
              
                  
                  datepicker('INIZIO_VIS'); ?>
            
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Inizio 
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text"  required="true" <?php echo $Disabilita_chiave; ?> class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("INIZIO_VIS");?> name="INIZIO_VIS"  id="INIZIO_VIS" value="<?php if (isset($cur_rec)) echo $cur_rec['INIZIO_VIS'];?>" size="55" maxlength="100"><br />
            </div>
            </div>
            
            <?php datepicker('FINE_VIS'); ?>
            
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Fine <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text"  required="true"  <?php echo $Disabilita_chiave; ?> class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("FINE_VIS");?> name="FINE_VIS"  id="FINE_VIS" value="<?php if (isset($cur_rec)) echo $cur_rec['FINE_VIS'];?>" size="55" maxlength="100"><br />
            </div>
            </div>
            
                 
            <?php datepicker('DATA_SPLIT'); ?>
            
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Data Divisione <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text"  required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("DATA_SPLIT");?> name="DATA_SPLIT"  id="DATA_SPLIT" value="<?php if (isset($cur_rec)) echo $cur_rec['DATA_SPLIT'];?>" size="55" maxlength="100"><br />
            </div>
            </div>                 
        
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Nuova Persona <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                   <select id="NUOVA_PERSONA" name="NUOVA_PERSONA" required="true" class=" select2_single form-control col-md-7 col-xs-12"/>
                         <?php  
                           $p = isset($_POST['NUOVA_PERSONA']) ? $_POST['NUOVA_PERSONA'] : "";
                          db_html_select_cod('persone', $p,'ID',array('Cognome','Nome'),false,"AziendaID =".$cur_rec['IDAZIENDA']." and ID != ".$cur_rec['IDPERSONA']);
                           ?>
                          </select> 
            </div>
            </div>        


            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-md-offset-3">
               <button class="cancel btn btn-primary" type="submit" name="Return" value="Return">Indietro</button>
               <button type="submit"  class="btn btn-success" name="Salva" value="Salva">Salva</button>
             </div>
        </div>
        </form>


        
<!-- Select2 -->
    <script>
      $(document).ready(function() {
        $(".select2_single").select2({
          placeholder: "",
          allowClear: true
        });
        
        <?php
          if ($_GET['p_upd']!=1 && isset($cur_rec) ) {
             echo  "$(\"#IDPERSONA\").html(getAttivitaSettori(".$cur_rec['IDAZIENDA'].",".$cur_rec['IDPERSONA']."));";
               
          }
        
        
        ?>

        
      });

      $( "#IDAZIENDA" ).change(function() {
          $("#IDPERSONA").val("");
          var newVal = $(this).val(); 
          if (newVal == null) $("#IDPERSONA").html("");
          else $("#IDPERSONA").html(getAttivitaSettori(newVal,null)); 
      });
      
      function getAttivitaSettori(valA,valP) {
       return $.ajax({
           type: "POST",
           url: "popola_persone_azienda.php",
           data: "idA="+valA+"&idP="+valP,
           async: false
         }).responseText;
      }      

    </script>
    <!-- /Select2 -->


<?php require '../Librerie/ges_html_bot.php';


?>
