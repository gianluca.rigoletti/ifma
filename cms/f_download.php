<?php
// definisco una variabile con il percorso alla cartella
// in cui sono archiviati i file
$dir = "../estrazioni/";

// Recupero il nome del file dalla querystring
// e lo accodo al percorso della cartella del download
$file = $dir.$_GET['filename'];

// verifico che il file esista
if(!$file)
{
  // se non esiste chiudo e stampo un errore
  die("Il file non esiste!");
}else{
  // Se il file esiste...
  // Imposto gli header della pagina per forzare il download del file
  header("Cache-Control: public");
  header("Content-type: application/force-download"); 
  header("Content-Description: File Transfer");
  header("Content-Disposition: attachment; filename= " . $_GET['filename']);
  header("Content-Transfer-Encoding: binary");
  // Leggo il contenuto del file
  readfile($file);
}
?>