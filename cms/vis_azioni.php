<?php
$m="azioni";

require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/files.php';
require '../Librerie/configurazione.php';     

$Titolo = "Gestione Azioni";
$Tavola= "azioni";

$risultato = db_query_vis($Tavola,'DATA desc');


require '../Librerie/ges_html_top.php';
?>

      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $Titolo;?> </h2>
            <ul class="nav navbar-right panel_toolbox">
                 <button class="btn btn-round btn-primary" type="button" onclick="location.href='ges_azioni.php?p_upd=0'">Nuova</button>
            </ul>
            <div class="clearfix"></div>
          </div>

          <div class="x_content">
          <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">

           <thead>
            <tr>
            <th  width="5%"> &nbsp;</th>
            <th  width="5%"> &nbsp;</th>
            <th  width="5%"> &nbsp;</th>
            <th  width="5%"> &nbsp;</th>
            <th width="20%"> Nome </th>
            <th width="20%"> Data </th>
          </thead>  
          <tbody> 
            <?php
                 while ($cur_rec = mysql_fetch_assoc($risultato))

            {
                 echo "<tr >	";
                    echo " <td ><a href=\"ges_azioni.php?p_upd=1&p_id=".$cur_rec['ID']."\"><i class=\"fa fa-edit\"></i></a></td>";
                     echo " <td ><a href=\"vis_persone_azioni.php?id=".$cur_rec['ID']."\"><i class=\"fa fa-search-plus\"></i></a></td>";
                     echo " <td ><a href=\"esportazioni.php?tipo=a&id=".$cur_rec['ID']."\"><i class=\"fa fa-file-excel-o\"></i></a></td>";
                     echo " <td ><a href=\"carica_azioni.php?id=".$cur_rec['ID']."\"><i class=\"fa fa-cogs\"></i></a></td>";
                    echo " <td >".$cur_rec['Nome']."   </td>
                          <td >".db_converti_data($cur_rec['DATA'])."   </td>
                         </tr> ";
                 } 
            ?>   
              </tbody>
        </table>

        </div>
      </div>
    </div>



     <script>
      $(document).ready(function() {
        $('#datatable-responsive').DataTable({
                  "bFilter":true,
                  "iDisplayLength": 50,
                         "bStateSave":true,                 
                  "aoColumns": [
                               { "bSortable": false },
                               { "bSortable": false },
                               { "bSortable": false },
                               { "bSortable": false },
                              null,
                              { "sType":  "data-ita"}
                             ]         
        });
      });
    </script>  





<?php require '../Librerie/ges_html_bot.php'; ?>
