<?php

require '../Librerie/connect.php';

$Titolo = "Annullamento Provincia";
$Tavola= "province";

$indietro = "vis_province.php";
if (isset($_GET['id_padre']) && !empty($_GET['id_padre']))  $indietro .= "?p_cliente=".$_GET['id_padre'];

if (isset($_GET['id']) ) {
	db_delete_cod($Tavola,$_GET['id'],"SIGLA");
	header("Location: $indietro");
	exit;
}
header("Location: $indietro");
exit;

?>