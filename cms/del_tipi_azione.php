<?php

require '../Librerie/connect.php';

$Titolo = "Annullamento tipi_azione";
$Tavola= "tipo_azione";

$indietro = "vis_tipi_azione.php";
if (isset($_GET['id_padre']) && !empty($_GET['id_padre']))  $indietro .= "?p_cliente=".$_GET['id_padre'];

if (isset($_GET['id']) ) {
	db_delete($Tavola,$_GET['id']);
	header("Location: $indietro");
	exit;
}
header("Location: $indietro");
exit;

?>