<?php
$m="associazioni";

require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/files.php';
require '../Librerie/configurazione.php';     

$Titolo = "Gestione Associazioni";
$Tavola= "soci_aziende";


if (!isset($_POST['RagioneSociale'])) {
   if (isset($_GET['RagioneSociale'])) {
       $_POST['RagioneSociale'] = $_GET['RagioneSociale'];
   } else if (isset($_SESSION["visordini_RagioneSociale"])) {
     $_POST['RagioneSociale'] = $_SESSION["visordini_RagioneSociale"];
   }
    else
   $_POST['RagioneSociale'] = "";
} else {
  $_SESSION['visordini_RagioneSociale'] = $_POST['RagioneSociale'];
}


$where = " 1 = 2 ";
if (!db_is_null($_POST['RagioneSociale']) ) {
    $where = "  upper(b.RagioneSociale) like  '%".str_replace("*","",addslashes(strtoupper($_POST['RagioneSociale'])))."%' ";
}


$sql = " select b.RagioneSociale, c.Nome,C.Cognome,a.INIZIO,a.FINE,c.ID idp ,b.ID ida
           from soci_azienda a, aziende b, persone c
          where b.ID = a.IDAZIENDA
            and c.ID = a.IDPERSONA
            and ".$where;

$risultato = mysql_query($sql);

require '../Librerie/ges_html_top.php';
?>


<script>
	$(function() {
		$( "#RagioneSociale" ).autocomplete({
			serviceUrl: "autocomplete_ragionesociale.php",
      paramName :"term"
		});
	});


  function conferma(a) {
     if (a.value != null && a.value.length != 0 ) {
        a.form.submit();
     }
  }

 </script>
 
          


      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $Titolo;?> </h2>
            <ul class="nav navbar-right panel_toolbox">
                 <button class="btn btn-round btn-primary" type="button" onclick="location.href='ges_associazioni.php?p_upd=0'">Nuovo</button>
            </ul>
            <div class="clearfix"></div>
          </div>
          
          

           <div class="x_content">
                <form action="" method="post">
                    <label>Ragione Sociale</label>
                    <input type="text" <?php $c_err->tooltip("RagioneSociale");?>  name="RagioneSociale" id="RagioneSociale" size="35" value="<?php if (isset($_POST["RagioneSociale"])) echo $_POST["RagioneSociale"]?>" />
                    <ul class="nav navbar-right panel_toolbox">
                         <button class="btn btn-round btn-primary" type="submit">Cerca</button>
                    </ul>                    
                </form>
                <div class="clearfix"></div>
              </div>           
          


          <div class="x_content">
          <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">

           <thead>
            <tr>
            <th  width="5%"> Modifica</th>
            <th  width="5%"> Cambia </th>
            <th width="20%"> Ragione Sociale </th>
            <th width="20%"> Nome </th>
            <th width="20%"> Inizio </th>
            <th width="20%"> Fine </th>
          </thead>  
          <tbody> 
            <?php
                 while ($cur_rec = mysql_fetch_assoc($risultato))

            {
            
                 $id= $cur_rec['ida'].";;".$cur_rec['idp'].";;".$cur_rec['INIZIO']; 
                 echo "<tr >	";
                    echo " <td ><a href=\"ges_associazioni.php?p_upd=1&p_id=".$id."\"><i class=\"fa fa-edit\"></i></a></td>";
                    echo " <td ><a href=\"ges_dividi_associazione.php?p_id=".$id."\"><i class=\"fa fa-exchange\"></i></a></td>";
                     //echo " <td ><a href=\"Javascript:ut_delete_rec('".$cur_rec['ID']."','del_potenzialita')\"><i class=\"fa fa-trash\"></i></a>";
                    echo " <td >".$cur_rec['RagioneSociale']."   </td>
                          <td >".$cur_rec['Cognome']." ".$cur_rec['Nome']."   </td>
                          <td >".db_converti_data($cur_rec['INIZIO'])."   </td>
                          <td >".db_converti_data($cur_rec['FINE'])."   </td>
                         </tr> ";
                 }
            ?>
              </tbody>
        </table>

        </div>
      </div>
    </div>



     <script>
      $(document).ready(function() {
        $('#datatable-responsive').DataTable({
                  "bFilter":true,
                  "iDisplayLength": 50,
                 "aaSorting": [[ 5, "asc" ]], 
                         "bStateSave":true,                 
                  "aoColumns": [
                               { "bSortable": false },
                               { "bSortable": false },
                              null,
                              null,         
                              { "sType":  "data-ita"},
                                { "sType":  "data-ita"}
                              
                             ]         
        });
      });
    </script>  





<?php require '../Librerie/ges_html_bot.php'; ?>
