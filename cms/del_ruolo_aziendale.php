<?php

require '../Librerie/connect.php';

$Titolo = "Annullamento ruolo aziendale";
$Tavola= "ruolo_aziendale";

$indietro = "vis_ruolo_aziendale.php";
if (isset($_GET['id_padre']) && (!empty($_GET['id_padre']) || $_GET['id_padre'] == 0))  $indietro .= "?p_settore=".$_GET['id_padre'];

if (isset($_GET['id']) ) {
	db_delete($Tavola,$_GET['id']);
	header("Location: $indietro");
	exit;
}
header("Location: $indietro");
exit;

?>