<?php
$m = 'carica_file';

require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/files.php';
require '../Librerie/configurazione.php';

$c_files = new files();
$tavola = 'citta';
$indietro = 'vis_citta.php';

if (isset($_GET['back'])) {
  $indietro = $_GET['back'].'.php';
}

require '../Librerie/ges_html_top.php';
$c_err->mostra();
?>

<div class="col-xs-12">
  <div class="x_panel content">
    <div class="x_title">
      <h1> Gestione caricamento file </h1>
    </div>
    <div class="x_content popovers">
    </div>
    <div class="x_content">
      <form id="upload-file" enctype="multipart/form-data">
        <label for="tipo">Seleziona il tipo di file da caricare</label>
        <input type="hidden" name="dummy">
        <br>
        <label for="file">Carica il file</label>
        <input type="file" name="file" id="file">
        <br>
      </form>
      <a value="upload" id="submit-form" name="submit-form" class="btn btn-success">Carica file</a>
      <i class="fa fa-cog"></i>
    </div>
  </div>
  
</div> 

<script>
$(document).ready(function() {
  var interval;
  $('#submit-form').click(function () {
    var $btn = $(this);
    $btn.button('loading');
    $('.fa-cog').addClass('fa-spin');
    var formData = new FormData($('#upload-file')[0]);
    $.ajax({
      url: 'ges_caricamento.php',
      type: 'POST',
      data: formData,
      cache: false,
      processData: false,
      contentType: false
    }).done(function(response) {
      console.log(response);
      $('.content').prepend('\
        <div class="alert alert-success alert-dismissible" role="alert">\
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
          <strong>Warning!</strong> Caricamento completato\
        </div>\
      ')
      $('.fa-cog').removeClass('fa-spin');
      $btn.button('reset');
    })
    .fail(function (response) {
      console.log(response);
    });;
  });

  function startProgressBar() {
    $('.progress-bar').removeClass('no-transition');
    interval = setInterval(function() {
      $.getJSON('status.json', function (data) {
        console.log(data);
        if (data == 'done') {
          $('.alert').alert('close');
          $('.progress-bar').addClass('no-transition');
          $('.progress-bar').css('width', '0%');
        }
        $('.progress-bar').attr('data-transitiongoal', data);
        $('.progress-bar').css('width', data + '%');
      });
    }, 2000);
  }
});

</script>

<?php require '../Librerie/ges_html_bot.php'; ?>

