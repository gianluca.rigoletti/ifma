<?php
$m="associazioni";

require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/files.php';
require '../Librerie/configurazione.php';

$c_files = new files(); 

$Tavola= "soci_azienda";

$indietro = "vis_associazioni.php";

if ($_GET['p_upd']==1) {
   $Funzione = "Update";
   $Disabilita_chiave = "disabled=\"disabled\"";
   $Titolo = "Modifica Associazione";
} else {
   $Funzione = "Insert";
   $Disabilita_chiave = "";
   $Titolo = "Nuova Associazione";
}

// se richiamato in update allora devo popolare il form
if ($_GET['p_upd']==1) {
   
   $id = explode(";;",$_GET['p_id']);
   
   if (count($id) != 3) {
       header("Location: ".$indietro);
       exit;   
   }

   $where = "IDAZIENDA = ".$id[0]." and IDPERSONA =".$id[1]." and INIZIO = '".$id[2]."'";
   
   $risultato = db_query_generale($Tavola,$where,"IDAZIENDA");
   $cur_rec = mysql_fetch_assoc($risultato);

   $cur_rec['INIZIO_VIS'] = db_converti_data($cur_rec['INIZIO']);
   $cur_rec['FINE_VIS'] = db_converti_data($cur_rec['FINE']); 

}

// confermo

if ( isset($_POST['Insert']) || isset($_POST['Update']) ) {



   
    $_POST['SPONSOR'] = !isset($_POST['SPONSOR']) ? 0 : 1;
   $_POST['FORNITORE'] = !isset($_POST['FORNITORE']) ? 0 : 1;
   $_POST['PROFESSIONAL'] = !isset($_POST['PROFESSIONAL']) ? 0 : 1;
   
    $cur_rec['MEMO'] = $_POST['MEMO'];
    $cur_rec['SPONSOR'] = $_POST['SPONSOR'];
    $cur_rec['FORNITORE'] = $_POST['FORNITORE'];
    $cur_rec['PROFESSIONAL'] = $_POST['PROFESSIONAL'];

    $cur_rec['FINE_VIS'] = $_POST['FINE_VIS'];
    $cur_rec['IDPERSONA'] = $_POST['IDPERSONA'];
    $cur_rec['IDAZIENDA'] = $_POST['IDAZIENDA'];
    
    if (isset($_POST['INIZIO_VIS'])) {
       $cur_rec['INIZIO_VIS'] = $_POST['INIZIO_VIS'];
       if ( !ControlloData($_POST['INIZIO_VIS'])) {
           $c_err->add("Campo Data Inizio Errato","INIZIO_VIS");
        } else {
            $_POST['INIZIO'] = converti_data_per_db($_POST['INIZIO_VIS']);
        }    
    } else {
      $_POST['INIZIO_VIS'] = db_converti_data($cur_rec['INIZIO']);
    }
                
    if (!ControlloData($_POST['FINE_VIS'])) {
       $c_err->add("Campo Data Fine  Errato","FINE_VIS");
    } else {
        $_POST['FINE'] = converti_data_per_db($_POST['FINE_VIS']);
    }                   

   if (!ControlloRangeData($_POST['INIZIO_VIS'],$_POST['FINE_VIS'],false)) {
       $c_err->add("Campo Data Fine Monore di data Inizio","FINE_VIS");
   }

   if (!$c_err->is_errore()) {
       if ( isset($_POST['Insert'])) {
	          db_insert($Tavola,$_POST,false);

       }  else {
            $where = "IDAZIENDA = ".$_POST['IDAZIENDA']." and IDPERSONA =".$_POST['IDPERSONA']." and INIZIO = '".$_POST['INIZIO']."'";
	          db_update_libero($Tavola,$_POST,$where);
       }
       header('Location: '.$indietro);
       exit;
   }
}

// torno indietro

if (isset($_POST['Return'])) {
   header("Location: ".$indietro);
   exit;
}

require '../Librerie/ges_html_top.php';

$c_err->mostra();
?>



       <form id="formG" class="form-horizontal form-label-left"  action="" method="post" novalidate>

            <?php
               $c1 = "";
               $c2 = "";
               $c3 = ""; 
               if (isset($cur_rec['SPONSOR']) && $cur_rec['SPONSOR'] == 1) $c1= 'checked="checked"';
               if (isset($cur_rec['FORNITORE']) && $cur_rec['FORNITORE'] == 1) $c2= 'checked="checked"';
               if (isset($cur_rec['PROFESSIONAL']) && $cur_rec['PROFESSIONAL'] == 1) $c3= 'checked="checked"';

            

               if ($_GET['p_upd']==1) {
                  $risA = mysql_fetch_assoc(db_query_fk("aziende",$cur_rec['IDAZIENDA']));
                  $risP = mysql_fetch_assoc(db_query_fk("persone",$cur_rec['IDPERSONA']));
                  ECHO "<input type=\"hidden\" name=\"IDPERSONA\" value=\"".$cur_rec['IDPERSONA']."\">
                  <input type=\"hidden\" name=\"IDAZIENDA\"  value=\"".$cur_rec['IDAZIENDA']."\">
                  <input type=\"hidden\" name=\"INIZIO\"  value=\"".$cur_rec['INIZIO']."\">
                  <div class=\"item form-group\">
                    <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Azienda <span class=\"required\">*</span>
                    </label>
                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                        <input type=\"text\"  required=\"true\" $Disabilita_chiave class=\"form-control col-md-7 col-xs-12\" value=\"".$risA['RagioneSociale']."\" size=\"55\" ><br />
                  </div>
                  </div>
                  <div class=\"item form-group\">
                    <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" >Persona <span class=\"required\">*</span>
                    </label>
                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                        <input type=\"text\"  required=\"true\" $Disabilita_chiave class=\"form-control col-md-7 col-xs-12\" value=\"".$risP['Cognome']." ".$risP['Nome']."\" size=\"55\" ><br />
                  </div>
                  </div>";                             
               }  else {
                   // creazione
                
                  
            ?>
          
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Azienda <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                   <select id="IDAZIENDA" name="IDAZIENDA" required="true" class=" select2_single form-control col-md-7 col-xs-12"/>
                         <?php  if (isset($cur_rec['IDAZIENDA']))
                                   { db_html_select_cod('aziende', $cur_rec['IDAZIENDA'],'ID','RagioneSociale',true,null);
                                }
                                else {db_html_select_cod('aziende', '','ID','RagioneSociale',true,null);
                                }
                           ?>
                          </select> 
            </div>
            </div>
            

            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Persona <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                   <select id="IDPERSONA" name="IDPERSONA" required="true" class="form-control col-md-7 col-xs-12"/>
                   </select> 
            </div>
            </div>            
            
              
             
          
          
               <?php 
                  } /// chiuso else creazione
                  
                  datepicker('INIZIO_VIS'); ?>
            
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Inizio <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text"  required="true" <?php echo $Disabilita_chiave; ?> class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("INIZIO_VIS");?> name="INIZIO_VIS"  id="INIZIO_VIS" value="<?php if (isset($cur_rec)) echo $cur_rec['INIZIO_VIS'];?>" size="55" maxlength="100"><br />
            </div>
            </div>
            
            <?php datepicker('FINE_VIS'); ?>
            
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Fine <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text"  required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("FINE_VIS");?> name="FINE_VIS"  id="FINE_VIS" value="<?php if (isset($cur_rec)) echo $cur_rec['FINE_VIS'];?>" size="55" maxlength="100"><br />
            </div>
            </div>
            
            <div class="item form-group">
              <div class="checkbox col-md-offset-3  col-md-7 col-xs-12" >
                <label>
                 <input type="checkbox" <?php echo $c1; ?> class="flat" name="SPONSOR" value="1" />
                  Sponsor
              </label>        
               
               </div>  
            </div>

            <div class="item form-group">
              <div class="checkbox col-md-offset-3  col-md-7 col-xs-12" >
                <label>
                 <input type="checkbox" <?php echo $c2; ?> class="flat" name="FORNITORE" value="1" />
                  Fornitore
              </label>        
               
               </div>  
            </div>
            
            <div class="item form-group">
              <div class="checkbox col-md-offset-3  col-md-7 col-xs-12" >
                <label>
                 <input type="checkbox" <?php echo $c3; ?> class="flat" name="PROFESSIONAL" value="1" />
                  Professional
              </label>        
               
               </div>  
            </div>                        
                       
            
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Memo <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text"  class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("MEMO");?> name="MEMO"  id="MEMO" value="<?php if (isset($cur_rec)) echo $cur_rec['MEMO'];?>" size="55" maxlength="100"><br />
            </div>
            </div>
                        
        


            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-md-offset-3">
               <button class="cancel btn btn-primary" type="submit" name="Return" value="Return">Indietro</button>
               <button type="submit"  class="btn btn-success" name="<?php echo $Funzione ?>" value="Salva">Salva</button>
             </div>
        </div>
        </form>


        
<!-- Select2 -->
    <script>
      $(document).ready(function() {
        $(".select2_single").select2({
          placeholder: "",
          allowClear: true
        });
        
        <?php
          if ($_GET['p_upd']!=1 && isset($cur_rec) ) {
             echo  "$(\"#IDPERSONA\").html(getAttivitaSettori(".$cur_rec['IDAZIENDA'].",".$cur_rec['IDPERSONA']."));";
               
          }
        
        
        ?>

        
      });

      $( "#IDAZIENDA" ).change(function() {
          $("#IDPERSONA").val("");
          var newVal = $(this).val(); 
          if (newVal == null) $("#IDPERSONA").html("");
          else $("#IDPERSONA").html(getAttivitaSettori(newVal,null)); 
      });
      
      function getAttivitaSettori(valA,valP) {
       return $.ajax({
           type: "POST",
           url: "popola_persone_azienda.php",
           data: "idA="+valA+"&idP="+valP,
           async: false
         }).responseText;
      }      

    </script>
    <!-- /Select2 -->


<?php require '../Librerie/ges_html_bot.php';


?>
