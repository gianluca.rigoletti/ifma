<?php

require '../Librerie/connect.php';

$Titolo = "Annullamento Attivita' aziende";
$Tavola= "attivita_aziende";

$indietro = "vis_attivita_aziende.php";
if (isset($_GET['id_padre']) && (!empty($_GET['id_padre']) || $_GET['id_padre'] == 0))  $indietro .= "?p_settore=".$_GET['id_padre'];

if (isset($_GET['id']) ) {
	db_delete($Tavola,$_GET['id']);
	header("Location: $indietro");
	exit;
}
header("Location: $indietro");
exit;

?>