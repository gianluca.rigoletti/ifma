<?php
$m="aziende";

require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/files.php';
require '../Librerie/configurazione.php';     

$Titolo = "Gestione Anagrafica";
$Tavola= "persone";


if (!isset($_POST['Cognome'])) {
   if (isset($_GET['Cognome'])) {
       $_POST['Cognome'] = $_GET['Cognome'];
   } else if (isset($_SESSION["visordini_Cognome"])) {
     $_POST['Cognome'] = $_SESSION["visordini_Cognome"];
   }
    else
   $_POST['Cognome'] = "";
} else {
  $_SESSION['visordini_Cognome'] = $_POST['Cognome'];
}




$where = "1 = 2";
if (!db_is_null($_POST['Cognome']) ) {
    $where = "  upper(Cognome) like  '%".str_replace("*","",addslashes(strtoupper($_POST['Cognome'])))."%' ";
}


$risultato = db_query_generale($Tavola,$where,'Cognome');


require '../Librerie/ges_html_top.php';
?>


<script>
	$(function() {
		$( "#Cognome" ).autocomplete({
			serviceUrl: "autocomplete_cognome.php",
      paramName :"term"
		});
	});


  function conferma(a) {
     if (a.value != null && a.value.length != 0 ) {
        a.form.submit();
     }
  }

 </script>
 
          


      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $Titolo;?> </h2>
            <ul class="nav navbar-right panel_toolbox">
                 <button class="btn btn-round btn-primary" type="button" onclick="location.href='ges_persone.php?p_upd=0'">Nuovo</button>
            </ul>
            <div class="clearfix"></div>
          </div>
          
          
       <div class="x_content">
                <form action="" method="post">
                    <label>Cognome</label>
                    <input type="text" <?php $c_err->tooltip("Cognome");?>  name="Cognome" id="Cognome" size="35" value="<?php if (isset($_POST["Cognome"])) echo $_POST["Cognome"]?>" />
                    <ul class="nav navbar-right panel_toolbox">
                         <button class="btn btn-round btn-primary" type="submit">Cerca</button>
                    </ul>                    
                </form>
                <div class="clearfix"></div>
              </div>           
          
    
          
          
          
          

          <div class="x_content">
          <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">

           <thead>
            <tr>
            <th  width="5%"> &nbsp;</th>
            <!--th  width="5%"> &nbsp;</th-->
            <th width="20%"> Cognome </th>
            <th width="20%"> Nome </th>
            <th width="20%"> Mail </th>
          </thead>  
          <tbody> 
            <?php
                 while ($cur_rec = mysql_fetch_assoc($risultato))

            {
                 echo "<tr >	";
                    echo " <td ><a href=\"ges_persone.php?p_upd=1&p_id=".$cur_rec['ID']."\"><i class=\"fa fa-edit\"></i></a></td>";
                     //echo " <td ><a href=\"Javascript:ut_delete_rec('".$cur_rec['ID']."','del_potenzialita')\"><i class=\"fa fa-trash\"></i></a>";
                    echo " <td >".$cur_rec['Cognome']."   </td>
                          <td >".$cur_rec['Nome']."   </td>
                          <td >".$cur_rec['EmailAziendale']."   </td>
                         </tr> ";
                 }
            ?>
              </tbody>
        </table>

        </div>
      </div>
    </div>



     <script>
      $(document).ready(function() {
        $('#datatable-responsive').DataTable({
                  "bFilter":true,
                  "iDisplayLength": 50,
                 "aaSorting": [[ 1, "asc" ]], 
                         "bStateSave":true,                 
                  "aoColumns": [
                               { "bSortable": false },
                              null,
                              null,
                              null
                             ]         
        });
      });
    </script>  





<?php require '../Librerie/ges_html_bot.php'; ?>
