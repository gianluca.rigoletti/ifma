<?php

require '../Librerie/connect.php';

header('content-type:application/json');


$messages = array();
upload_file();
echo json_encode('done');

/**
 * Scrive in status.json il valore 
 * 
 * @param integer $value percentuale di completamento
 */
function set_status_of_request($value)
{
    if ($value === 100) {
        $value = 'done';
    }
    $request_progress = fopen('status.json', 'w');
    fwrite($request_progress, json_encode($value));
    fclose($request_progress);
}


/**
 * Inserisce un messaggio nell'array di messaggi.
 * L'array viene modificato epr referenza.
 * 
 * @param string $type     il tipo di messaggio: error, success, info
 * @param string $content  il contenuto del messaggio
 *
 * @return json oggetto json di risposta
 */
function set_message($type, $content)
{
    global $messages;
    $push = array(
        'type' => $type, 
        'content' => $content
    );
    array_push($messages, $push);
}

/**
 * Scrive una string di log in log.txt
 * @param string $text stringa
 * @param string $file nome del file su cui scrivere
 */
function set_log_message($text, $file='log.txt', $dev=false)
{
    if ($dev) {
        $handle = fopen($file, 'a');
        fwrite($handle, $text.PHP_EOL);
        fclose($handle);
    }
}


/**
 * Prende la riga del file csv, ne fa il parsing
 * e valuta se inserire o aggiornare il db
 * @param  array $row campi della riga del file csv
 */
function update_database($row)
{
    /*
    0  Last Modified Date,
    1  Title,
    2  First Name,
    3  Last Name,
    4  Company,
    5  Email Address,
    6  Source ID,
    7  Area,
    8  Ruolo,
    9  Designation,
    10 Date of Birth,
    11 Gender,
    12 Confirmed Opted-In,
    13 Opted-Out,
    14 Work Address 1,
    15 Work City,
    16 Work State Code,
    17 Work State,
    18 Work ZIP/Postal Code,
    19 Work Country Code,
    20 Work Country,
    21 Work Phone,
    22 Work Fax,
    23 Mobile Phone,
    24 Tipologia azienda,
    25 Settore,
    26 Settore azienda,
    27 Fatturato azienda,
    28 Anno di rilevazione del fatturato,
    29 Numero dipendenti,
    30 Anno di rilevazione dei dipendenti
     */
    // Apro il file per la scrittura dei nuovi record inseriti
    $handle = fopen('record_persone_inserite.csv', 'a');
    // Controllo se il campo email è già presente:
    $sql = "SELECT * from persone WHERE EmailAziendale='".trim($row[5])."' LIMIT 1";
    $query = mysql_query($sql);

    // Debug
    // set_log_message("Query: $sql");

    // Applico le '' a tutti i campi della riga csv per inserirle senza 
    // problemi nella query sql
    $values = array_map(function($value) {
        return "'$value'";
    }, $row);
    
    if (mysql_num_rows($query) > 0) {
        $res = mysql_fetch_assoc($query);
        // Se csv ha ultima modifica > database allora aggiorno
        $csv_date = new DateTime($row[0]);
        $db_date = new DateTime($res['DataUltimaModifica']);

        // Debug
        // set_log_message("Values: ".print_r($values, true));

        if ($csv_date > $db_date) {
            $sql = "UPDATE persone
            SET Cognome            = {$values[3]},
                Nome               = {$values[2]},
                Titolo             = {$values[1]},
                Sesso              = {$values[11]},
                DataNascita        = {$values[10]},
                Indirizzo          = {$values[14]},
                Citta              = {$values[15]},
                CAP                = {$values[18]},
                Nazione            = {$values[20]},
                Telefono           = {$values[21]},
                Cellulare          = {$values[23]},
                DataUltimaModifica = '{$csv_date->format('Y-m-d')}',
                Fax                = {$values[22]}
            WHERE EmailAziendale   = ".trim($values[5]);

            // Debug
            // set_log_message("Csv >  DB query: $sql");

            mysql_query($sql);
        }
    } else {
        // Se il campo mail non è presente inserisco un nuovo record nel db
        // salvando in un file csv a parte e controllando l'integrità 
        // referenziale dell'azienda
        
        // Debug
        // set_log_message('Row: '.print_r($row, true));

        fwrite($handle, implode(';', $row).PHP_EOL);
        // Cerco l'id dell'azienda da inserire
        $sql_azienda = "SELECT ID from aziende
            WHERE upper(REPLACE(RagioneSociale, ' ', '')) 
            LIKE upper(REPLACE('%{$row[4]}%', ' ', '')) LIMIT 1";
        $query_azienda = mysql_query($sql_azienda);

        // Debug
        // set_log_message("Query azienda: $sql_azienda");

        $res_azienda = mysql_fetch_assoc($query_azienda);
        $id_azienda = ($res_azienda) ? $res_azienda['ID'] : null;
        // Parte di inserimento del nuovo record in persone    
        // Se l'azienda è presente la inserisco in AziendaID, altrimenti
        // metto il campo del csv sotto il campo Dipartimento
        if ($id_azienda !== null) {
            $sql = "INSERT INTO persone
            (ID, Cognome, Nome, Titolo, Sesso, DataNascita, AziendaID, Indirizzo,
            Citta, CAP, Nazione, Telefono, Cellulare, 
            Fax, EmailAziendale, RuoloEffettivo)
            VALUES
            (null, {$values[3]}, {$values[2]}, {$values[1]}, {$values[11]}, 
            {$values[10]}, $id_azienda, {$values[14]}, {$values[15]}, {$values[18]}, 
            {$values[20]}, {$values[21]}, {$values[23]}, 
            {$values[22]}, {$values[5]}, {$values[9]})";

            // Debug
            // set_log_message("Query azienda not null: $sql");

            mysql_query($sql);
        } else {
            $sql = "INSERT INTO persone
            (ID, Cognome, Nome, Titolo, Sesso, DataNascita, Dipartimento, Indirizzo,
            Citta, CAP, Nazione, Telefono, Cellulare,
            Fax, EmailAziendale, RuoloEffettivo)
            VALUES
            (null, {$values[3]}, {$values[2]}, {$values[1]}, {$values[11]}, 
            {$values[10]}, {$values[4]}, {$values[14]}, {$values[15]}, {$values[18]}, {$values[20]}, 
            {$values[21]}, {$values[23]}, {$values[22]}, {$values[5]},
            {$values[9]})";

            // Debug
            // set_log_message("Query azienda null: $sql");

            mysql_query($sql);
        }       
    }
    fclose($handle);
}

/**
 * Inserisce o aggiorna i file nel database
 * @param  string $type citta, servizi, tariffe
 * @param  file $file file di lettura da caricare
 * @return boolean se tutto è andato a buon fine
 */
function read_csv($filename)
{
    $header  = array();
    $data    = array();
    $success = false;
    $i       = 0;

   $file = fopen($filename, 'r');
    // Lettura dei file con intestazione
    while(!feof($file)) {
        if ($i === 0) {
            $header = fgetcsv($file, 1024, ';');

            // Debug
            // set_log_message("Header: ${var_export($header, true)}");

        }
        // Debug
        // set_log_message("index: $i");
        $data = fgetcsv($file, 1024, ';');
        if ($data) {
            update_database($data);
        }
        $i++;
    }
    fclose($file);
}

/**
 * Carica il file e restituisce un messaggio di ritorno
 * 
 */
function upload_file()
{
    global $messages;
    $response = null;
    if (isset($_POST) && !db_is_null($_POST)) {
        $file_name = basename($_FILES['file']['name']);
        $file_extension = pathinfo($file_name, PATHINFO_EXTENSION);
        //Validazione del tipo di estensione
        if ($file_extension != 'txt' && $file_extension != 'csv') {
            set_message('error', 'Non e\' stato selezionato alcun file o il file caricato non ha un\' estensione valida');
        }

        if ($_FILES['file']['error'] == UPLOAD_ERR_OK && is_uploaded_file($_FILES['file']['tmp_name'])) {
            $filename = $_FILES['file']['tmp_name'];
            // backup dei dati in una nuova tabella mysql
            $today = date("YmdHis");
            $sql_backup = "CREATE TABLE persone_$today AS SELECT * FROM persone";
            mysql_query($sql_backup);
            // Lettura file csv
            read_csv($filename);
        }
    }
}
