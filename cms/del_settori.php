<?php

require '../Librerie/connect.php';

$Titolo = "Annullamento settore";
$Tavola= "settori";

$indietro = "vis_settori.php";
if (isset($_GET['id_padre']) && !empty($_GET['id_padre']))  $indietro .= "?p_cliente=".$_GET['id_padre'];

if (isset($_GET['id']) ) {
	db_delete_cod("attivita_aziende",$_GET['id'],"IDSETTORE");
  db_delete($Tavola,$_GET['id']);
	header("Location: $indietro");
	exit;
}
header("Location: $indietro");
exit;

?>