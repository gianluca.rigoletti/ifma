var http = createObject();
var risposta ="";
var ajax_codice="";
var ajax_descrizione="";
var ajax_errore="";
var ajax_tipo_g="";

function createObject() {
var tipo_richiesta;
var browser = navigator.appName;
if(browser == "Microsoft Internet Explorer"){
tipo_richiesta = new ActiveXObject("Microsoft.XMLHTTP");
}else{
tipo_richiesta = new XMLHttpRequest();
}
return tipo_richiesta;
}

function verifica_cod_des(tavola,valore,where) {
   var campi = new Array();
   campi[0] = "DESCRIZIONE";
  verifica_cod_despers(tavola,valore,where,campi);
}

function verifica_cod_despers(tavola,valore,where,campides) {
var w = where;
if (where == null) w= '1 = 1';

var c = "";
for (i=0; i<campides.length; i++) {
  if (c != "") c +="**"; 
  c += campides[i];
}
http.open('get', '../../Librerie/verifica.php?tavola='+tavola+'&valore='+valore+'&where='+w+'&campi='+c,true);
http.send(null);
http.onreadystatechange = handleResponse;
}

function handleResponse() {
if(http.readyState == 4){
   if(http.status==200)
   { 
     var cod=document.getElementsByName(ajax_codice);
     var des=document.getElementsByName(ajax_descrizione);
     risposta = http.responseText;
     var stringa = risposta.split('*#*');
     if  (stringa[1] != "N") {
          alert(ajax_errore);
          cod[0].value = "";
          des[0].value = "";
          cod[0].focus();
      } else {
          cod[0].value = stringa[2];
          des[0].value = stringa[3];              
      }          
   }  else
   {
      alert("Error loading Ajax ! Status: "+ http.status +" - "+ http.statusText + http.responseText);
   }
}
}

function update_destinazione(id,campo,check) {

var val = 0;
if (check.checked) val = 1; 
http.open('get', 'update_destinazione.php?id='+id+'&campo='+campo+'&valore='+val,true);
http.send(null);
http.onreadystatechange = handleResponseUpdateDestinazione;
}

function handleResponseUpdateDestinazione() {
if(http.readyState == 4){
   if(http.status==200)
   { 
     risposta = http.responseText;
     var stringa = risposta.split('*#*');
     if (stringa[1]=="S") { 
     } else {
       alert("Attenzione errore in aggiornamento "+risposta);
     }
   } else
   {
      alert("Error loading Ajax ! Status: "+ http.status +" - "+ http.statusText + http.responseText);
   }

}
}

function update_utente(id,check) {

var val = 0;
if (check.checked) val = 1; 
http.open('get', 'update_utente.php?id='+id+'&valore='+val,true);
http.send(null);
http.onreadystatechange = handleResponseUpdateUtente;
}

function handleResponseUpdateUtente() {
if(http.readyState == 4){
   if(http.status==200)
   { 
     risposta = http.responseText;
     var stringa = risposta.split('*#*');
     if (stringa[1]=="S") { 
     } else {
       alert("Attenzione errore in aggiornamento "+risposta);
     }
   } else
   {
      alert("Error loading Ajax ! Status: "+ http.status +" - "+ http.statusText + http.responseText);
   }

}
}

function delete_img_gallery(tavola,id_tavola,id,tipo) {
ajax_tipo_g = tipo;
http.open('get', 'gallery_delete.php?tavola='+tavola+'&idtav='+id_tavola+'&id='+id+'&tipo='+tipo,true);
http.send(null);
http.onreadystatechange = handleResponseDeleteImgGallery;
}

function handleResponseDeleteImgGallery() {
if(http.readyState == 4){
   if(http.status==200)
   { 
     risposta = http.responseText;
     var stringa = risposta.split('*#*');
     
     var id = "";
     if (ajax_tipo_g=="I") var id = 'gallery';
     if (ajax_tipo_g=="F") var id = 'allegati';
     if (stringa[1]=="S") {
        document.getElementById(id).innerHTML= stringa[2];
     } else {
       alert("Attenzione errore in aggiornamento "+risposta);
     }
   } else
   {
      alert("Error loading Ajax ! Status: "+ http.status +" - "+ http.statusText + http.responseText);
   }

}
}

function reload_img_gallery(tavola,id_tavola,tipo) {
ajax_tipo_g = tipo;
http.open('get', 'gallery_reload.php?tavola='+tavola+'&idtav='+id_tavola+'&tipo='+tipo,true);
http.send(null);
http.onreadystatechange = handleResponseReloadImgGallery;
}

function handleResponseReloadImgGallery() {
if(http.readyState == 4){
   if(http.status==200)
   { 
     risposta = http.responseText;
     var stringa = risposta.split('*#*');
     var id = "";
     if (ajax_tipo_g=="I") var id = 'gallery';
     if (ajax_tipo_g=="F") var id = 'allegati';
     if (stringa[1]=="S") {
        window.opener.document.getElementById(id).innerHTML= stringa[2];
        window.close();
     } else {
       alert("Attenzione errore in aggiornamento "+risposta);
     }
   } else
   {
      alert("Error loading Ajax ! Status: "+ http.status +" - "+ http.statusText + http.responseText);
   }

}
}