			$(document).ready(function() {
        
        $.fn.dataTable.ext.oSort['data-ita-asc']  = function(a,b) {
            var ukDatea = a.split('/');
            var ukDateb = b.split('/');
             
            var x = parseInt(ukDatea[2] + ukDatea[1] + ukDatea[0]);
            var y = parseInt(ukDateb[2] + ukDateb[1] + ukDateb[0]);
             
            return ((x < y) ? -1 : ((x > y) ?  1 : 0));
        };
         
        $.fn.dataTable.ext.oSort['data-ita-desc'] = function(a,b) {
            var ukDatea = a.split('/');
            var ukDateb = b.split('/');
             
            var x = parseInt(ukDatea[2] + ukDatea[1] + ukDatea[0]);
            var y = parseInt(ukDateb[2] + ukDateb[1] + ukDateb[0]);
             
            return ((x < y) ? 1 : ((x > y) ?  -1 : 0));
        };      
  			
        $.fn.dataTable.ext.oSort['numeric-ita-asc']  = function(a,b) {
  				var x = (a == "-") ? 0 : a.replace(".","").replace( /,/, "." );
  				var y = (b == "-") ? 0 : b.replace(".","").replace( /,/, "." );
  				x = parseFloat( x );
  				y = parseFloat( y );
  				return ((x < y) ? -1 : ((x > y) ?  1 : 0));
  			};
  			
  			$.fn.dataTable.ext.oSort['numeric-ita-desc'] = function(a,b) {
  				var x = (a == "-") ? 0 : a.replace(".","").replace( /,/, "." );
  				var y = (b == "-") ? 0 : b.replace(".","").replace( /,/, "." );
  				x = parseFloat( x );
  				y = parseFloat( y );
  				return ((x < y) ?  1 : ((x > y) ? -1 : 0));
  			};
        
        	} );